<?php
return [
	'Whether the rules in the module' => 'Будет ли роутинг во фронте',
	'Module ID' => 'ID модуля',
	'Has menu admin items?' => 'Будут ли пункты меню в админке',
	'Module Name' => 'Название модуля',
	'Has upload dir?' => 'Создавать папку в uploads для модуля',
	'If model has text field with images upload' => 'Если у модели будут текстовые поля, с загрузкой картинок',
	'Has images?' => 'Будут ли картинки?',
	'Main model module' => 'Модель модуля по умолчанию',
	'This generator helps you to generate the skeleton code needed by a Yii clean module.' => 'Этот генератор поможет вам сгенирировать каркас модуля для Yii CleanCMS',
	'The module has been generated successfully.' => 'Модуль успешно сгенерирован',
	'Add SEO' => 'Добавить SEO',
	'Add Search Model' => 'Добавить модель для поиска',
	'Add Lang Model' => 'Мультиязычная модель',
	'Has url?' => 'Будет ссылка на запись?',
];