<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\helpers\AdminHelper;
use app\modules\admin\models\AdminImagesSizes;

class ImagesController extends Controller
{
    public function actionIndex($id)
    {
		$model = AdminImagesSizes::findOne($id);
		$changed=AdminHelper::regenerateImage(
			Yii::getAlias('@app').'/web/images/'.Yii::$app->getModule($model->modules)->path_images,
			$model
		);

        echo 'success changed = '. $changed . "\n";
    }
}
