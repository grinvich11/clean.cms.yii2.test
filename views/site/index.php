<?php
$this->title = 'My Yii Application';
?>
<? echo app\modules\slider\widgets\sliderWidget::widget(); ?>
<? echo app\modules\subjects\widgets\subjectsWidget::widget(); ?>
<? echo app\modules\areas\widgets\areasWidget::widget(); ?>
<? echo app\modules\teachers\widgets\teachersWidget::widget(); ?>
<? echo app\modules\photos\widgets\photosWidget::widget(); ?>
<? echo app\modules\reasons\widgets\reasonsWidget::widget(); ?>

<section class="om-press">
    <div class="container">
        <div class="row">
            <div class="col-xs-36 text-center">

                <h2 class="om-press__title">Убедили?</h2>
                <p class="om-press__click">Жмите на кнопку!</p>
                <a class="om-btn om-btn--cta" href="#cta-form">Оставить заявку</a>

            </div>
        </div>
    </div>
</section>

<? echo app\modules\responses\widgets\responsesWidget::widget(); ?>

<section class="om-form">
    <div class="container">
        <div class="row">
            <div class="col-xs-36">
                <h2 class=om-form__title>Вступительные экзамены</h2>

                <? echo app\modules\feedback\widgets\feedbackWidget::widget(['type'=>2]); ?>
            </div>
        </div>
    </div>
</section>

<? echo app\modules\contacts\widgets\contactsWidget::widget(); ?>