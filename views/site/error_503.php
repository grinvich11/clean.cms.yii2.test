<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

?>
<div class="site-error error-template">
    <h1><?= Html::encode($title) ?></h1>
    <h2><?= $exception->statusCode ?></h2>

    <div class="error-details">
        <?= nl2br(Html::encode($message)) ?>
    </div>
</div>