<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\helpers\MenuHelper;
use yii\widgets\Menu;
use app\helpers\LangHelper;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="no-js">
<head>
	<title><?=Html::encode($this->title) ?></title>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
	<meta http-equiv="content-language" content="<?= Yii::$app->language ?>">

	<!-- Set external libraries -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/own.css" />

    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
	<?php $this->beginBody() ?>
	<header>
		<?
		NavBar::begin([
			'brandLabel' => Yii::$app->params['siteName'],
			'brandUrl' => Url::to(['/']),
		]);
			echo Nav::widget([
				'items'=>MenuHelper::getMenu('header'),
				'options' => ['class' => 'navbar-nav']
			]);

			echo Nav::widget([
				'items'=>LangHelper::getItemsForMenu(),
				'options' => ['class' => 'navbar-nav pull-right']
			]);
		NavBar::end();
		?>
	</header>

    <main>
		<div class="container">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= $content ?>
		</div>
    </main>

    <footer>
		<div class="container">
			<p>
				admin@clevercode.ru
				 ·
				<?=Menu::widget(['items'=>MenuHelper::getMenu('footer'), 'options'=>['tag'=>false], 'itemOptions'=>['tag'=>false]]);?>
			</p>
		</div>
	</footer>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>