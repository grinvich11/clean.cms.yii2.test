<?php
namespace app\helpers;

use Yii;
use app\modules\menu\models\Menu;

class MenuHelper{
	public static $cachingTime=3600;

	/**
	 *
	 * @param type $alias
	 * @return items for widget Menu
	 */
	public static function getMenu($alias){//@TODO: попрофилировать
		if(Yii::$app->cache)
			$menu = Yii::$app->cache->get('MenuHelper::getMenuQuery_'.Yii::$app->language.$alias);
		if(!$menu){
			$menu = Menu::findOne(['alias' => $alias, 'language_id' => substr(Yii::$app->language, 0, 2), 'active'=>1, 'disabled'=>0, 'visible'=>1]);
			if(Yii::$app->cache)
				Yii::$app->cache->set('MenuHelper::getMenuQuery_'.Yii::$app->language.$alias, $menu, self::$cachingTime);
		}

		if($menu->need_highlight_items){
			if($menu){
				$items=Menu::createTree($menu->children()->andWhere(['active'=>1, 'disabled'=>0, 'visible'=>1])->all(), $menu->need_highlight_items);
			}else{
				$items=[];
			}
		}else{
			if(Yii::$app->cache)
				$items = Yii::$app->cache->get('MenuHelper::getMenu_'.Yii::$app->language.$alias);
			if(!$items) {
				if($menu){
					$items=Menu::createTree($menu->children()->andWhere(['active'=>1, 'disabled'=>0, 'visible'=>1])->all(), $menu->need_highlight_items);
				}else{
					$items=[];
				}

				if(Yii::$app->cache)
					Yii::$app->cache->set('MenuHelper::getMenu_'.Yii::$app->language.$alias, $items, self::$cachingTime);
			}
		}

		return $items ;
	}
}