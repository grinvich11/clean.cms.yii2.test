<?php
namespace app\helpers;

class ImageHelper
{
	public static function HexToRGB($hex) {
		$hex = ereg_replace("#", "", $hex);
		$color = array();

		if(strlen($hex) == 3) {
		$color['r'] = hexdec(substr($hex, 0, 1) . $r);
		$color['g'] = hexdec(substr($hex, 1, 1) . $g);
		$color['b'] = hexdec(substr($hex, 2, 1) . $b);
		}
		else if(strlen($hex) == 6) {
		$color['r'] = hexdec(substr($hex, 0, 2));
		$color['g'] = hexdec(substr($hex, 2, 2));
		$color['b'] = hexdec(substr($hex, 4, 2));
		}

		return $color;
	}

	public static function functByExt($ext,$image,$action, $sOrigImg='') {
		$ext=strtolower($ext);
		switch ($ext) {
			case 'png':
				switch ($action) {
					case 'create':
						$im=imagecreatefrompng($image);
						imagealphablending($im, false);
						imagesavealpha($im, true);

						return $im;
						break;
					case 'show':
						return imagepng($image);
						break;
					case 'save':
						imagepng($image, $sOrigImg);
						break;
				}
				break;
		}
	}

	public static function getCoord($font_size,$font_angle,$font,$text) {
		return imagettfbbox(
			$font_size,  // размер шрифта
			$font_angle,          // угол наклона шрифта (0 = не наклоняем)
			$font,  // имя шрифта, а если точнее, ttf-файла
			$text       // собственно, текст
		);
	}

	public static function applyText($sOrigImg, $text, $angle=0, $color, $opacity, $font_size, $font, $X=0, $Y=0, $watermark=NULL){
		$ext=end(explode('.', $sOrigImg));


		$aImgInfo = getimagesize($sOrigImg);
		if (is_array($aImgInfo) && count($aImgInfo)) {
			$image = self::functByExt($ext,$sOrigImg,'create');

			$iSrcWidth = $aImgInfo[0];
			$iSrcHeight = $aImgInfo[1];

			//$coord = self::getCoord($font_size, $angle, $font, $text);

			/*$X = 0;
			$Y = $iSrcHeight;*/

			imagettftext(
				$image,      // как всегда, идентификатор ресурса
				$font_size,   // размер шрифта
				$angle,           // угол наклона шрифта
				$X, $Y,      // координаты (x,y), соответствующие левому нижнему
							// углу первого символа
				$color,    // цвет шрифта
				$font,   // имя ttf-файла
				$text
			);

			self::functByExt($ext,$image,'save', $sOrigImg);

			if($watermark){
				$im = new \Imagick;
				$im->readImage($sOrigImg);
				$offset=self::getImageOffset($watermark->alignment_1.' '.$watermark->alignment_2);
				$im->setImageProperty('x', $offset[0]);
				$im->setImageProperty('y', $offset[1]);
				file_put_contents($sOrigImg, $im);
			}

			imagedestroy($image);
		}
	}

	public static function getImageOffset($type){
		switch ($type) {
			case 'center center':
				return ['NULL', 'NULL'];
				break;
			case 'right center':
				return ['TRUE', 'NULL'];
				break;
			case 'center bottom':
				return ['NULL', 'TRUE'];
				break;
			case 'right bottom':
				return ['TRUE', 'TRUE'];
				break;
			case 'left top':
				return ['0', '0'];
				break;
			case 'right top':
				return ['TRUE', '0'];
				break;
			case 'left bottom':
				return ['0', 'TRUE'];
				break;
			case 'center top':
				return ['NULL', '0'];
				break;
			case 'left center':
				return ['0', 'NULL'];
				break;
			default:
				return [$type[0], $type[1]];
				break;
		}
	}

	public static $watermark_position=[
		'left top',
		'center top',
		'right top',

		'left center',
		'center center',
		'right center',

		'left bottom',
		'center bottom',
		'right bottom',
	];

	public static function setPosition($type, $image, $watermark){
		switch ($type) {
			case 'center center':
				$image->watermark($watermark, NULL, NULL, 100);
				break;
			case 'right center':
				$image->watermark($watermark, TRUE, NULL, 100);
				break;
			case 'center bottom':
				$image->watermark($watermark, NULL, TRUE, 100);
				break;
			case 'right bottom':
				$image->watermark($watermark, TRUE, TRUE, 100);
				break;
			case 'left top':
				$image->watermark($watermark, 0, 0, 100);
				break;
			case 'right top':
				$image->watermark($watermark, TRUE, 0, 100);
				break;
			case 'left bottom':
				$image->watermark($watermark, 0, TRUE, 100);
				break;
			case 'center top':
				$image->watermark($watermark, NULL, 0, 100);
				break;
			case 'left center':
				$image->watermark($watermark, 0, NULL, 100);
				break;
			default:
				$image->watermark($watermark, $type[0], $type[1], 100);
				break;
		}
		return $image;
	}

	public static function autoRotateImage($file) {
		$image = new \Imagick;
		$image->readimage($file);
		$orientation = $image->getImageOrientation();

		switch($orientation) {
			case \Imagick::ORIENTATION_BOTTOMRIGHT:
				$image->rotateimage("#000", 180); // rotate 180 degrees
				break;

			case \Imagick::ORIENTATION_RIGHTTOP:
				$image->rotateimage("#000", 90); // rotate 90 degrees CW
				break;

			case \Imagick::ORIENTATION_LEFTBOTTOM:
				$image->rotateimage("#000", -90); // rotate 90 degrees CCW
				break;
		}

		// Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
		$image->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
		file_put_contents($file, $image);
	}
}