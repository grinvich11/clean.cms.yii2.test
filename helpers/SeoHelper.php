<?php
namespace app\helpers;
use Yii;

class SeoHelper
{
	public static function registerSeoPage($alias){
		if(Yii::$app->cache){
			$model = Yii::$app->cache->get('seo_'.$alias.'_'.Yii::$app->language);
		}

		if(!$model){
			$model=\app\modules\seo\models\SeoPages::find()->where(['alias'=>$alias])->one();
			if(!$model) return false;
			if(Yii::$app->cache)
				Yii::$app->cache->set('seo_'.$alias.'_'.Yii::$app->language, $model, Yii::$app->params['cache_time']);
		}

		Yii::$app->metaTags->register($model);
	}
}