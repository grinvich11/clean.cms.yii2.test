<?php
namespace app\helpers;

use Yii;
use app\modules\language\models\Language;
use yii\helpers\ArrayHelper;

class LangHelper{
	public static $cachingTime=3600;

	public static function getLang($slash=true){
		$finds=Language::find()->select(['locale'])->orderBy(['position'=>SORT_ASC])->all();
		if($finds){
			foreach ($finds as $value) {
				$lang[$value->locale]=($slash ? '/' : '').$value->locale;
			}
		}

		return $lang;
	}

	public static function getLangModels(){
		if(Yii::$app->cache)
			$items = Yii::$app->cache->get('LangHelper::getLangModels');
		if(!$items) {
			$items=Language::find()->orderBy(['position'=>SORT_ASC])->all();
			if(Yii::$app->cache)
				Yii::$app->cache->set('LangHelper::getLangModels', $items, self::$cachingTime);
		}
		return $items;
	}

	public static function getCurrentLang(){
		if(Yii::$app->cache)
			$model = Yii::$app->cache->get('LangHelper::getCurrentLang_'.Yii::$app->language);
		if(!$model) {
			$model=Language::find()->where(['locale_full'=>Yii::$app->language])->one();
			if(Yii::$app->cache)
				Yii::$app->cache->set('LangHelper::getCurrentLang_'.Yii::$app->language, $model, self::$cachingTime);
		}
		return $model;
	}

	public static function getItemsForMenu(){
		$langs=self::getLangModels();
		if($langs){
			$langs_tmp_arr=ArrayHelper::map($langs, 'locale_full', 'name');
			foreach ($langs_tmp_arr as $key => $value) {
				$langs_arr[$key]=$value;
			}


			$route = Yii::$app->controller->route;
			$appLanguage = Yii::$app->language;
			$params = $_GET;

			array_unshift($params, '/' . $route);

			foreach (Yii::$app->urlManager->languages as $language) {
				$isWildcard = substr($language, -2) === '-*';
				/*if (
					$language === $appLanguage ||
					// Also check for wildcard language
					$isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)
				) {
					continue;   // Exclude the current language
				}*/
				if ($isWildcard) {
					$language = substr($language, 0, 2);
				}
				$params['language'] = $language;
				$lit_arr[] = [
					'label' => $langs_arr[$language],
					'url' => $params,
					'active'=>$language==Yii::$app->language
				];
			}

			$currentLang=LangHelper::getCurrentLang();
			$items['items']=[
				'label' => $currentLang->name,
				'locale' => $currentLang->locale,
				'items'=>$lit_arr
			];

			return $items;
		}
	}
}
