<?php
namespace app\helpers;

use Yii;
use yii\image\drivers\Image;
use app\modules\admin\models\AdminModules;
use app\modules\admin\models\AdminImagesSizes;

class AdminHelper {
	public static function read_files_dir($path,$mask) {
		$files = glob($path.'/'.$mask, GLOB_NOSORT);
		if (is_array($files))
		foreach ($files as &$value) {
			 $value = str_replace(dirname($_SERVER['SCRIPT_FILENAME']), '', $value);
		}

		return $files;
	}

	public static function returnMenuArray(){
		$dirs = scandir(dirname(__FILE__).'/../modules');
		$menu=[];
		$keys=[];

		foreach ($dirs as $name){
			if ($name[0] != '.' && $name!='admin'){
				$find=AdminModules::findOne(['module'=>$name,'state'=>1]);
				if($find){
					$tmp=Yii::$app->getModule($name)->getMenuItems();
					if($tmp){
						$menu=array_merge($menu,$tmp);
						for ($index = 0; $index < count($tmp); $index++) {
							$keys[]=$find->position;
						}
					}
				}
			}
		}

		array_multisort($keys, $menu);
		return $menu;
	}
//AdminHelper::executeSql(dirname($_SERVER['SCRIPT_FILENAME']).'/protected/modules/'.$this->module.'/install/dump.sql');
	public static function executeSql($sqlDumpPath){
		if(is_file($sqlDumpPath)){
			$sqlRows=preg_split("/--\s*?--.*?\s*--\s*/", file_get_contents($sqlDumpPath));

			/*$connection=new CDbConnection($this->getDsn(), $this->dbUserName, $this->dbPassword);
			$connection->charset='utf8';
			$connection->active=true;*/
			$connection=Yii::$app->db;

			$connection->createCommand("SET NAMES 'utf8';");

			foreach($sqlRows as $q)
			{
				$q=trim($q);
				if(!empty($q))
				{
					if(strpos($q, 'DROP TABLE IF EXISTS')===false)
						$connection->createCommand($q)->execute();
					else
					{
						$lines=preg_split("/(\r?\n)+/", $q);
						$dropQuery=$lines[0];
						array_shift($lines);
						$query=implode('', $lines);

						$connection->createCommand($dropQuery)->execute();
						if($query)
							$connection->createCommand($query)->execute();
					}
				}
			}
		}
	}

	public static function removeDir($path){
		if(is_dir($path)){
			$files = glob($path.'/*'); // get all file names
			if($files)
			foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
			}
			rmdir($path);
		}
	}

	public static function regenerateImage($path, $size){
		$files = glob($path.'/'.$size->field.'/*'); // get all file names

		$path_size=$path.'/'.$size->field.'/'.$size->size;
		$path_main=$path.'/'.$size->field;

		if(!is_dir($path_main)){
			mkdir($path_main);
		}
		if(!is_dir($path_size)){
			mkdir($path_size);
		}

		$ch = 0;

		if($files)
		foreach($files as $file){ // iterate files
			if(is_file($file)){
				$file=end(explode('/', $file));
				$image = Yii::$app->image->load($path_main.'/'.$file);
				switch ($size->method) {
					case 'crop':
						$method = Image::CROP;
						break;
					case 'heigth':
						$method = Image::HEIGHT;
						break;
					case 'width':
						$method = Image::WIDTH;
						break;
					case 'auto':
						$method = Image::AUTO;
						break;
					case 'precise':
						$method = Image::PRECISE;
						break;
					case 'adapt':
						$method = Image::ADAPT;
						break;
				}

				if($size->method!=='adapt'){
					$image->resize($size->width, $size->heigth, $method);
				}else{
					$image->resize($size->width, $size->heigth, $method)->background('#fff');
				}

				$image->save($path_size.'/'.$file);
				if(Yii::$app->image->driver=='Imagick'){
					ImageHelper::autoRotateImage($path_size.'/'.$file);
				}
				$ch++;
			}
		}

		copy(Yii::getAlias('@app').'/modules/admin/assets/no_image.png', $path_size.'/no_image.png');

		$image = Yii::$app->image->load($path_size.'/no_image.png');
		$image->resize($size->width, $size->heigth, Image::CROP);
		$image->save($path_size.'/no_image.png');
		return $ch;
	}

	public static function generateImage($path,$file,$size){
		if(is_file($path.$size->field.'/'.$file)){
			if(!is_dir($path.$size->field)){
				mkdir($path.$size->field);
			}
			if(!is_dir($path.$size->field.'/'.$size->size)){
				mkdir($path.$size->field.'/'.$size->size);
			}
			copy($path.$size->field.'/'.$file,$path.$size->field.'/'.$size->size.'/'.$file);
			$image = Yii::$app->image->load($path.$size->field.'/'.$file);
			if($size->method=='crop'){
				$image->resize($size->width, $size->heigth, Image::CROP);
			}elseif($size->method=='heigth'){
				$image->resize($size->width, $size->heigth, Image::HEIGHT);
			}elseif($size->method=='width'){
				$image->resize($size->width, $size->heigth, Image::WIDTH);
			}elseif($size->method=='auto'){
				$image->resize($size->width, $size->heigth, Image::AUTO);
			}elseif($size->method=='adapt'){
				$image->resize($size->width, $size->heigth, Image::ADAPT)->background('#fff');
			}
			$image->save($path.$size->field.'/'.$size->size.'/'.$file);
			if(\Yii::$app->image->driver=='Imagick'){
				ImageHelper::autoRotateImage($path.$size->field.'/'.$size->size.'/'.$file);
			}
		}
	}

	/*
	 * recursively remove a directory
	 */
	public static function rrmdir($dir) {
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
				self::rrmdir($file);
			else
				unlink($file);
		}
		if(is_dir($dir))
			rmdir($dir);
	}

	public static function scanModules(){
		$dirs = scandir(dirname(__FILE__).'/../modules');
		foreach ($dirs as $name){
			if ($name[0] != '.'){
				$module=Yii::$app->getModule($name);
				if(!$module->not_menu){
					$find=AdminModules::findOne(['module'=>$module->id]);
					if(!$find){
						Yii::$app->db->createCommand()->insert('admin_modules', [
							'module' => $module->id,
							'name' => $module->name,
							'version' => $module->version,
							'delete' => $module->delete ? 1 : 0,
						])->execute();
					}
				}
			}
		}
		//Удалить тех, которые нет в папке модули
		$find_all=AdminModules::find()->all();
		if($find_all){
			foreach ($find_all as $value) {
				$module=Yii::$app->getModule($value->getPrimaryKey());
				if(!$module){
					$value->delete();
				}
			}
		}
	}

	public static function returnModules(){
		$dirs = scandir(dirname(__FILE__).'/../modules');
		foreach ($dirs as $name){
			if ($name[0] != '.'){
				$modules[$name]=Yii::$app->getModule($name);
			}
		}
		return $modules;
	}

	public static function full_pathinfo($path_file){
        $path_file = strtr($path_file, ['\\'=>'/']);

        preg_match("~[^/]+$~",$path_file,$file);
        preg_match("~([^/]+)[.$]+(.*)~",$path_file,$file_ext);
        preg_match("~(.*)[/$]+~",$path_file,$dirname);

        return [
			'dirname' => $dirname[1],
			'basename' => $file[0],
			'extension' => (isset($file_ext[2]))?$file_ext[2]:false,
			'filename' => (isset($file_ext[1]))?$file_ext[1]:$file[0]
		];
    }

	public static function deleteImages($file, $field, $module)
	{
		$imagePath = $_SERVER['DOCUMENT_ROOT'] . '/images/'.$module.'/'.$field.'/';
		$images_size=AdminImagesSizes::find()->where(['modules'=>$module, 'field'=>$field])->all();
		if($images_size){
			foreach ($images_size as $size) {
				if(is_file($imagePath.$size->size.'/'.$file))
					unlink($imagePath.$size->size.'/'.$file);
			}

			if(is_file($imagePath.$file))
				unlink($imagePath.$file);
		}
	}
}
