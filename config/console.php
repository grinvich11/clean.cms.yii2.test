<?php
$dirs = scandir(dirname(__FILE__).'/../modules');

// строим массив
$modules = array();
foreach ($dirs as $name){
    if ($name[0] != '.')
        $modules[$name] =[
			'class' => 'app\modules\\'.$name.'\\Module',
		];
}

// строка вида 'news|page|user|...|socials'
// пригодится для подстановки в регулярные выражения общих правил маршрутизации
define('MODULES_MATCHES', implode('|', array_keys($modules)));

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
	'controllerMap' => [
		'migrate' => [
			'class' => 'app\modules\admin\components\MigrateController',
			'templateFile' => '@app/modules/admin/templates/migrations/migration.php',
			'generatorTemplateFiles' => [
				'create_table' => '@app/modules/admin/templates/migrations/createTableMigration.php',
				'drop_table' => '@app/modules/admin/templates/migrations/dropTableMigration.php',
				'add_column' => '@app/modules/admin/templates/migrations/addColumnMigration.php',
				'drop_column' => '@app/modules/admin/templates/migrations/dropColumnMigration.php',
				'create_junction' => '@app/modules/admin/templates/migrations/createJunctionMigration.php',
				'add_param' => '@app/modules/admin/templates/migrations/createConfigMigration.php',
				'create_lang' => '@app/modules/admin/templates/migrations/createTableLangMigration.php',
				'add_seopage' => '@app/modules/admin/templates/migrations/addSeoPageMigration.php',
			]
		]
    ],
    'modules' => array_replace($modules, [
        'gii' => 'yii\gii\Module',
    ]),
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
		'image' => [
			'class' => 'yii\image\ImageDriver',
			'driver' => 'GD'  //GD or Imagick
		],
    ],
    'params' => $params,
];
