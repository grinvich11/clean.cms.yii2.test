<?php
$dirs = scandir(dirname(__FILE__).'/../modules');

// строим массив
$modules = array();
foreach ($dirs as $name){
    if ($name[0] != '.')
        $modules[$name] =[
			'class' => 'app\modules\\'.$name.'\\Module',
		];
}

// строка вида 'news|page|user|...|socials'
// пригодится для подстановки в регулярные выражения общих правил маршрутизации
define('MODULES_MATCHES', implode('|', array_keys($modules)));



$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
	'sourceLanguage' => '00',
	'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'config'],//
	'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
		'config'=>array(
			'class'=>'\app\components\MyConfig',
		),
		'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm'   => '@vendor/npm-asset',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'enableCsrfValidation'=>!(substr_count($_SERVER['REQUEST_URI'], 'treemanager')>0),//@TODO: найти как отключить CRSF или добавить, для нормального функционирования treemanager
            'cookieValidationKey' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\admin_users\models\AdminUsers',
            'enableAutoLogin' => true,
			'loginUrl' => ['admin/login'],
			'on afterLogin' => function($event){
                Yii::$app->user->identity->afterLogin($event);
            }
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
		'urlManager' => [
			'class'=>'\app\components\MUrlManager',
            'enablePrettyUrl' => true,
			//'enableStrictParsing' => true,
            'showScriptName' => false,
			'rules'=>array(
				'admin/<controller:(modules|database|config|login|seopages)>'=>'admin/<controller>/index',
				'admin/<controller:(modules|database|config|login|seopages)>/<action:\w+>/<id:\w+>'=>'admin/<controller>/<action>',
				'admin/<controller:(modules|database|config|login|seopages|main)>/<action:\w+>'=>'admin/<controller>/<action>',

				'admin/<controller:\w+>'=>'<controller>/backend/main/index',
				'admin/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/backend/main/<action>',
				'admin/<controller:\w+>/<action:\w+>'=>'<controller>/backend/main/<action>',
			),
			'languages' => is_file('../runtime/lang_locales.txt') ? unserialize(file_get_contents('../runtime/lang_locales.txt')) : ['ru'=>'ru-RU'],
			'enableDefaultLanguageUrlCode' => false,
			'enableLanguagePersistence' => false,
			'enableLocaleUrls' => true,
			'enableLanguageDetection' => false,
			//'languageParam' => 'lang',
        ],
		'view' => [
			'theme' => [
				'pathMap' => [
				   '@app/views' => '@vendor/almasaeed2010/adminlte'
				],
			],
	   ],
	   'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
					'js'=>[]
				],
				'yii\bootstrap\BootstrapAsset' => [
					'css' => [],
				],
            ],
        ],
		'image' => [
			'class' => 'yii\image\ImageDriver',
			'driver' => 'GD'  //GD or Imagick
		],
		'metaTags' => [
			'class' => 'app\modules\seo\components\MetaTagsComponent',
			'generateCsrf' => false,
			'generateOg' => false,
		],
		'i18n' => [
			'translations' => [
				'gii' => [
					'class' => 'yii\i18n\PhpMessageSource',
					//'basePath' => '@app/messages',
					//'sourceLanguage' => 'en-US',
					'fileMap' => [
						'gii'       => 'gii.php',
					],
				],
				'app' => [
					'class' => 'yii\i18n\DbMessageSource',
					//'basePath' => '@app/messages',
					//'sourceLanguage' => 'en-US',
					//'forceTranslation'=>true,
					'enableCaching'=>true,
					'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
				],
				'*' => [
					'class' => 'yii\i18n\DbMessageSource',
					//'basePath' => '@app/messages',
					//'sourceLanguage' => 'en-US',
					//'forceTranslation'=>true,
					'enableCaching'=>true,
					'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
				],
			],
		],
    ],
	'modules' => array_replace($modules, [
		'treemanager' =>  [
			'class' => '\kartik\tree\Module',
			// other module settings, refer detailed documentation
			'treeViewSettings' => [
				'nodeView' => '@app/modules/menu/views/backend/main/_form',
				'nodeAddlViews' => [
					2 => '@app/modules/menu/views/backend/main/_settings',
				]
			]
		]
	]),
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '*.*.*.*'] // регулируйте в соответствии со своими нуждами
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*'] // регулируйте в соответствии со своими нуждами
    ];;

    $config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '*.*.*.*'],
		'generators' => [
			// generator name
			'crud' => [
				//generator class
				'class'     => 'app\modules\admin\templates\gii\crud\Generator',
			],
			'model' => [
				//generator class
				'class'     => 'app\modules\admin\templates\gii\model\Generator',
				//setting for out templates
				/*'templates' => [
					// template name => path to template
					'mymodel' => '@app/modules/admin/templates/gii/model/default',
				]*/
			],
			'module' => [
				//generator class
				'class'     => 'app\modules\admin\templates\gii\module\Generator',
				//setting for out templates
				/*'templates' => [
					// template name => path to template
					'mymodel' => '@app/modules/admin/templates/gii/module/default',
				]*/
			]
		],
    ];
}

return $config;
