<?php
namespace app\components;

use Yii;

class Module extends \yii\base\Module
{
	public $defaultRoute = 'main';
	public $not_menu=false;
	public $delete=true;
	public $path_images;

	public function init() {
		parent::init();

		if(!$this->path_images)
			$this->path_images=$this->id;

		/*$this->setImport(array(
			'application.modules.'.$this->getName() . '.models.*',
			'application.modules.'.$this->getName() . '.components.*',
			'application.modules.admin.models.*',
			'application.modules.admin.components.*',
			'application.modules.admin.components.widgets.*',
		));

		$this->setViewPath(Yii::app()->getBasePath() . '/modules/' . $this->getName(). '/views');*/
		//$this->layoutPath = Yii::getAlias('@app/modules/'. Yii::$app->controller->module->id.'/views/layouts');
		//$this->setViewPath(Yii::$app->getBasePath() . '/modules/' . $this->id. '/views');
		$this->layoutPath = Yii::getAlias('@app/modules/admin/views/layouts');
	}

	public function getMenuItems(){

	}

	public static function rules()
    {
        return false;
	}
}
