<?php
namespace app\components;

use Yii;
/**
 * Load Module Routes
 */
class MUrlManager extends \codemix\localeurls\UrlManager
{
	public function init()
	{
		$this->loadModulesUrl();
		parent::init();
	}

	/**
	 * Scan each module dir and include routes.php
	 * Add module urls at the beginning of $config['urlManager']['rules']
	 * @access protected
	 */
	protected function loadModulesUrl()
	{
		$cacheId = 'modules_urls';
		$cache=Yii::$app->cache;
		if($cache!==null){
			$rules    = $cache->get($cacheId);
		}

		if(!$rules)
		{
			$rules       = [];
			$dirs = scandir(dirname(__FILE__).'/../modules');

			$modules = array();
			foreach ($dirs as $name){
				if ($name[0] != '.' && $name<>'admin' && $name<>'seo')
					$modules[$name] = 'app\modules\\'.$name.'\\Module';
			}

			foreach($modules as $name=>$module)
			{
                //if(method_exists($module, 'rules')){
					$arr_rules=call_user_func($module .'::rules');
					if($arr_rules)
						$rules = array_merge($arr_rules, $rules);
                //}
			}

			if($cache!==null){
				$cache->set($cacheId, $rules, 3600);
			}
		}

		$this->rules = array_merge($rules, $this->rules);
	}

}
