<?php

namespace app\components;

use Yii;
use yii\widgets\ListView as ParentListView;

class ListView extends ParentListView
{
	public $layout='{items} {pager}';
	public $emptyText='Нет ни одной записи';
}