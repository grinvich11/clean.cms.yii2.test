<?php
namespace app\components;

use yii\web\Controller as ParentController;
use Yii;

class Controller extends ParentController{
	public $headerTitle;
	public $breadcrumbs;

	public function init(){
		if(!is_file('../runtime/installed')){
			Yii::$app->user->logout();
			header("Location: /install");
			die();
			/*Yii::$app->user->logout();
			return $this->redirect(['/install']);*/
		}

		parent::init();
	}
}
