<?php
namespace app\components;

use Yii;

class MyConfig extends \yii\base\Component
{
    public $cachingTime;
	public static $cacheName = 'app_params';

	public function init(){
		$this->cachingTime = 3600;
		try{
			if(is_file('../runtime/installed'))
				$this->loadConfig();
		} catch (Exception $e) {

		}
	}

	private function loadConfig() {
		if(Yii::$app->cache)
			$model = Yii::$app->cache->get(self::$cacheName);
		if(!$model) {
			$model=Yii::$app->db->createCommand('SELECT param,value FROM {{config}}')->queryAll();
			if(Yii::$app->cache)
				Yii::$app->cache->set(self::$cacheName, $model, $this->cachingTime);
		}
		foreach ($model as $key) {
			Yii::$app->params[$key['param']] = $key['value'];
		}
	}

	public static function clearCache(){
		if(Yii::$app->cache)
			Yii::$app->cache->delete(self::$cacheName);
	}
}