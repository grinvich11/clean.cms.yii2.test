<?php
namespace app\components;

use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use app\modules\admin\models\AdminModules;

class FrontController extends Controller
{
	public function init()
	{
		parent::init();

		if(!$this->module->not_menu){
			$find=  AdminModules::find()->where(['module'=>$this->module->id,'state'=>1])->count();
			if(!$find){
				throw new NotFoundHttpException('The requested page does not exist.');
			}
		}

		if(Yii::$app->params['siteIsConstruction'] && Yii::$app->user->isGuest){
			throw new HttpException('503', Yii::t('main.error', 'Сайт на обслуживании'));
		}
	}

	public function getViewPath($checkTheme=false){
		return Yii::getAlias('@app').'/modules/'.$this->module->id.'/views/'.$this->id;
	}
}