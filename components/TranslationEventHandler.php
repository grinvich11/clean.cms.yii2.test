<?php
namespace app\components;

use Yii;
use yii\i18n\MissingTranslationEvent;
use app\modules\language\models\SourceMessage;
use app\modules\language\models\Message;
use app\modules\language\models\Language;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        //$event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";

		$source = SourceMessage::find()->where(['message'=>$event->message, 'category'=>$event->category])->one();

		// If we didn't find one then add it
		if( !$source )
		{
			// Add it
			$source = new SourceMessage;

			$source->category = $event->category;
			$source->message = $event->message;
			$source->save();
		}

		if( $event->language != Yii::$app->sourceLanguage )
		{
			$languages=Language::find()->select(['locale'])->all();
			if($languages){
				foreach ($languages as $language) {
					// Do the same thing with the messages
					$translation = Message::find()->where(['id'=>$source->id, 'language'=>$language->locale])->count();

					// If we didn't find one then add it
					if( !$translation )
					{
						// Add it
						$model = new Message;

						$model->id = $source->id;
						$model->language = $language->locale;
						$model->translation = $event->message;
						$model->save();
					}
				}
			}
		}
    }
}