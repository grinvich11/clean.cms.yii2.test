<?php
namespace app\modules\slider;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Слайдер';
	public $model='Slider';

	public $install=array(
		'images'=>array(
			'image'=>array(
				'admin'=>array(
					'width'=>281,
					'heigth'=>100,
					'method'=>'auto'
				),
				'big'=>array(
					'width'=>1349,
					'heigth'=>480,
					'method'=>'auto'
				)
			)
		)
	);

	public function getMenuItems(){
		if(\Yii::$app->user->identity->getIsAdmin())
			return array(
				array('label' => 'Слайдер', 'icon' => 'fa fa-picture-o', 'url' => '/admin/slider', 'active' => \Yii::$app->controller->module->id=='slider'),
			);
	}
}
