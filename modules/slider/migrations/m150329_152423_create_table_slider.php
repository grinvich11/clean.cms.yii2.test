<?php

class m150329_152423_create_table_slider extends yii\db\Migration
{
	public $tableName='{{slider}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'image' => 'VARCHAR(255) NOT NULL COMMENT "Изображение"',
				'title_form' => 'VARCHAR(255) NULL COMMENT "Заголовок формы"',
				'title_color' => 'VARCHAR(30) NOT NULL DEFAULT "#000000" COMMENT "Цвет заголовка"',
				'position' => 'INT UNSIGNED NOT NULL DEFAULT "1" COMMENT "Позиция"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}