<?php

class m150329_152446_create_table_config_params extends yii\db\Migration
{
	public $tableName='{{config}}';
	public $moduleName='slider';

	public function safeUp()
	{
		/*$this->insert(
			$this->tableName,
			array(
				'module' => $this->moduleName,
				'param' => $this->moduleName.'_slider_autoplay',
				'value' => 2000,
				'default' => 5000,
				'label' => 'Скорость смены слайдов',
				'type' => 'int'
			)
		);*/

		$this->insert(
			$this->tableName,
			array(
				'module' => $this->moduleName,
				'param' => $this->moduleName.'_footer_text',
				'value' => 'Ранний старт подготовки — залог поступления в хороший вуз!',
				'default' => 'Ранний старт подготовки — залог поступления в хороший вуз!',
				'label' => 'Скорость смены слайдов',
				'type' => 'string'
			)
		);
	}

	public function safeDown()
	{
		//echo "m150327_120222_create_config_params does not support migration down.\\n";
		//return false;
		$this->delete($this->tableName, 'module=:module', array(':module'=>$this->moduleName));
	}
}