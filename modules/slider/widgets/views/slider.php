<!-- Slider main container -->
<div class="swiper-container om-slider" id="about">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
		<? foreach ($items as $key=>$value): ?>
        <div class="swiper-slide om-slider__slide om-slider__slide--<?=($key+1);?>">
			<div class="container">
                <div class="row">
                    <div class="hidden-xs col-sm-6 col-md-8">
						<img src="/images/slider/image/<?=$value['image'];?>" alt="" class="om-slider__img"/>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
						<? echo app\modules\feedback\widgets\feedbackWidget::widget(['type'=>1, 'value'=>$value]); ?>
					</div>

                </div>
            </div>
        </div>
		<? endforeach; ?>
    </div>

    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

    <div class="om-slider__footer text-center">
		<?=\Yii::$app->params['slider_footer_text'];?>
    </div>

</div>