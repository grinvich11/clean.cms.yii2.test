<?php
namespace app\modules\slider\widgets;

use yii\base\Widget;

class sliderWidget extends Widget
{
    public function run()
    {
        $sql='SELECT id,image, title_form, title_color FROM {{slider}} ORDER BY position ASC';
		$items=\Yii::$app->db->createCommand($sql)->queryAll();

		if($items)
			return $this->render('slider', ['items'=>$items]);
    }
}