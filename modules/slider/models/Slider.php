<?php

namespace app\modules\slider\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $image
 * @property string $title_form
 * @property string $title_color
 * @property integer $position
 * @property string $created
 * @property string $modified
 */
class Slider extends \app\models\MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['position'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['image', 'title_form'], 'string', 'max' => 255],
            [['title_color'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'title_form' => 'Заголовок формы',
            'title_color' => 'Цвет заголовка',
            'position' => 'Позиция',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord)
				$this->position=(int)Yii::$app->db->createCommand("SELECT MAX(`position`)+1 FROM ".self::tableName())->queryScalar();
			return true;
		}else{
			return false;
		}
	}
}
