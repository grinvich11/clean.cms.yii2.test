<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_162605_create_table_source_messages extends Migration {
	public $tableName='{{source_message}}';

    public function safeUp() {
		$this->createTable(
			$this->tableName,
			[
				'id' => 'INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
				'category' => 'VARCHAR(32) DEFAULT NULL',
				'message' => 'TEXT',
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
    }

    public function safeDown() {
		return false;
    }
}