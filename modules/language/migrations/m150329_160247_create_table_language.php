<?php

class m150329_160247_create_table_language extends yii\db\Migration
{
	public $tableName='{{language}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'TINYINT(2) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'name' => 'VARCHAR(255) NOT NULL COMMENT "Язык"',
				'alias' => 'VARCHAR(4) NOT NULL COMMENT "Алиас"',
				'locale' => 'VARCHAR(2) NOT NULL COMMENT "Локаль"',
				'locale_full' => 'VARCHAR(5) NOT NULL COMMENT "Локаль полная"',
				'published' => 'TINYINT(1) DEFAULT "0" COMMENT "Опубликован"',
				'position' => 'TINYINT(2) UNSIGNED DEFAULT "0" COMMENT "Порядок"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',

				'UNIQUE KEY `locale` (`locale`)'
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);

		$this->insert(
			$this->tableName,
			array(
				'name' => 'Русский',
				'alias' => 'ru',
				'locale' => 'ru',
				'locale_full' => 'ru-RU',
				'published' => 1
			)
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}