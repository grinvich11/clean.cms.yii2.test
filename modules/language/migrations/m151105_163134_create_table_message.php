<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_163134_create_table_message extends Migration {
	public $tableName='{{message}}';

    public function safeUp() {
		$this->createTable(
			$this->tableName,
			[
				'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'language' => 'VARCHAR(16) NOT NULL DEFAULT ""',
				'translation' => 'TEXT',
				'PRIMARY KEY (`id`,`language`)',
				'CONSTRAINT `message_source` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE'
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
    }

    public function safeDown() {
		return false;
    }
}