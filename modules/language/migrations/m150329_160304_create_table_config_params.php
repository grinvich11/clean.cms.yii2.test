<?php

class m150329_160304_create_table_config_params extends yii\db\Migration
{
	public $tableName='{{config}}';
	public $moduleName='language';

	public function safeUp()
	{
		$this->insert(
			$this->tableName,
			array(
				'module' => $this->moduleName,
				'param' => $this->moduleName.'_language',
				'value' => 'ru-RU',
				'default' => 'ru-RU',
				'label' => 'Язык приложения',
				'type' => 'string'
			)
		);
	}

	public function safeDown()
	{
		//echo "m150329_160304_create_table_config_params does not support migration down.\\n";
		//return false;
		$this->delete($this->tableName, 'module=:module', array(':module'=>$this->moduleName));
	}
}