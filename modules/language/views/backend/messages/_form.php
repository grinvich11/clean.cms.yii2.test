<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\language\models\Language;
use app\modules\language\models\SourceMessage;
?>
<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>array(
		'autocomplete'=>'off',
	),
]); ?>
	<? $this->beginBlock('base'); ?>

		<?php echo $form->errorSummary($model); ?>

		<?php
		$widgetOptions=['value'=>$model->parent->category];
		if(!$model->isNewRecord)
			$widgetOptions['disabled']='disabled';
		?>
		<? echo $form->field($model,'parent_category', ['inputOptions' => $widgetOptions])->dropDownList(ArrayHelper::map(SourceMessage::find()->select(['category'])->groupBy(['category'])->orderBy(['category'=>SORT_ASC])->all(), 'category', 'category')); ?>

		<?php echo $form->field($model,'parent_message', ['inputOptions' => ['value'=>$model->parent->message, 'disabled'=>'disabled']]); ?>

		<?php
		$widgetOptions=[];
		if(!$model->isNewRecord)
			$widgetOptions['disabled']='disabled';
		?>
		<? echo $form->field($model,'language', ['inputOptions' => $widgetOptions])->dropDownList(ArrayHelper::map(Language::find()->all(), 'locale', 'locale')); ?>

		<?php echo $form->field($model,'translation')->textArea(); ?>



		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>/index">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true]
			]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>