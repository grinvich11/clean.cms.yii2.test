<?php
use yii\widgets\Pjax;
use app\modules\language\models\SourceMessage;
use app\modules\language\models\Language;
use app\modules\admin\components\widgets\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
<?php Pjax::begin(['id'=>'pajax_grid']); ?>
<?= GridView::widget(array(
	'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	'layout'=>'<div class=box>
		<div class=box-header>
		  <h3 class=box-title></h3>
		  <div class="box-tools btn-group pull-right">
			<button class="btn btn-warning" type=button onclick="$.pjax.reload({container:\'#pajax_grid\'});"><span class="glyphicon glyphicon-refresh"></button>
		  </div>
		</div><!-- /.box-header -->
		<div class=box-body>{items}</div><!-- /.box-body -->
		<div class="box-footer clearfix">
			<div class="row">
				<div class="col-xs-6">
					<div class="dataTables_info pull-left">{summary}</div>
				</div>
				<div class="col-xs-6">
				{pager}
				</div>
			</div>
		</div>
	  </div><!-- /.box -->',
	'columns'=>array(
		array(
			'attribute'=>'parent_category',
			'value' => function ($model) {
				return $model->parent->category;
			},
			'filter'=>ArrayHelper::map(SourceMessage::find()->select(['category'])->groupBy(['category'])->orderBy('category')->asArray()->all(), 'category', 'category')
		),
		array(
			'attribute'=>'parent_message',
			'value' => function ($model) {
				return $model->parent->message;
			},
		),
		array(
			'attribute'=>'language',
			'filter'=>ArrayHelper::map(Language::find()->select(['locale'])->asArray()->all(), 'locale', 'locale')
		),
		'translation',
		[
			'class' => 'yii\grid\ActionColumn',
			'header' => Html::a('<i class="glyphicon glyphicon-remove"></i>', ['index'], ['class' => 'btn btn-mini btn-danger']),
			'template' => '{update} {delete}',
			'contentOptions' => array('style' => 'width: 90px;text-align:center;')
		],
	)
));
?>
<?php Pjax::end(); ?>