<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\modules\admin\components\widgets\ActiveForm;
use app\modules\language\models\Message;
use app\modules\language\models\Language;
?>

<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'autocomplete'=>'off',
	],
]); ?>

	<? $this->beginBlock('base'); ?>

		<?php echo $form->errorSummary($model); ?>

		<?php echo $form->field($model,'category'); ?>
		<?php echo $form->field($model,'message'); ?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>/index">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<? $this->beginBlock('messages'); ?>
		<?
		$languages=Language::find()->select(['locale'])->all();
		if($languages){
			$message_model= new Message();
			foreach ($languages as $language) {
				$find=Message::find()->where(['language'=>$language->locale, 'id'=>$model->id])->one();
				if($find)
					$value=$find->translation;
				if(substr_count($model->category, 'text')>0){
					echo $form->field($message_model,'['.$language->locale.']translation',['labelOptions'=>['label'=>$message_model->attributeLabels()['translation'].' ['.$language->locale.']'],'inputOptions'=>['value'=>$value, 'rows'=>substr_count($value, "\n")>3 ? substr_count($value, "\n")+3 : 3]])->textArea();
				}else{
					echo $form->field($message_model,'['.$language->locale.']translation',['labelOptions'=>['label'=>$message_model->attributeLabels()['translation'].' ['.$language->locale.']'],'inputOptions'=>['value'=>$value]]);
				}
			}
		}
		?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>/index">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>$model->isNewRecord],
				['label'=>'Переводы', 'content'=>$this->blocks['messages'], 'active'=>!$model->isNewRecord]
			]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>