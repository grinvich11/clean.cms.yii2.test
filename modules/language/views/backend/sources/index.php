<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'exportButton' => true,
		'importButton' => true,
        'columns' => [
			[
				'class' => 'yii\grid\CheckboxColumn',
			],
            [
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => 'id',
			],
            'category',
            'message:ntext',
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
			],
        ],
    ]);

PjaxGrid::end();
