<?php
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin(['id'=>'pajax_grid']);
	echo GridView::widget(array(
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'actionsAllow'=>false,
		'columns'=>[
			//array('name'=>'id','type'=>'raw','value'=>'$data->id','htmlOptions'=>array('style'=>'width:40px;text-align:center;')),
			'name',
			'alias',
			'locale',
			[
				'class' => '\app\modules\admin\components\widgets\ToggleColumn',
				'attribute' => 'published',
			],
			[
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
				'template' => '{move} {update} {delete}',
			],
		]
	));
PjaxGrid::end();