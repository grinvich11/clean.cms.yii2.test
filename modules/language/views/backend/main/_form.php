<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;
?>
<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>array(
		'autocomplete'=>'off',
	),
]); ?>
	<? $this->beginBlock('base'); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->field($model,'name'); ?>
	<?php echo $form->field($model,'alias'); ?>
	<?php echo $form->field($model,'locale'); ?>
	<?php echo $form->field($model,'locale_full'); ?>
	<?php echo $form->field($model,'published')->checkBox();?>

	<div class="box-footer">
		<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/admin/<?=Yii::$app->controller->module->id;?>">К списку</a>
	</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo yii\bootstrap\Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true]
			]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>