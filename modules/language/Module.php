<?php
namespace app\modules\language;

use Yii;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Языки';
	public $delete=false;

	public function getMenuItems(){
		return [
			[
				'label' => 'Языки', 'icon' => 'fa fa-tags', 'url' => '/admin/language', 'active' => Yii::$app->controller->module->id=='language',
				'items'=>[
					['label' => 'Языки', 'icon' => 'fa fa-tags', 'url' => '/admin/language', 'active' => Yii::$app->controller->module->id=='language' && Yii::$app->controller->id=='backend/main'],
					['label' => 'Оригиналы', 'icon' => 'fa fa-tags', 'url' => '/language/backend/sources', 'active' => Yii::$app->controller->module->id=='language' && Yii::$app->controller->id=='backend/sources'],
					['label' => 'Переводы', 'icon' => 'fa fa-tags', 'url' => '/language/backend/messages', 'active' => Yii::$app->controller->module->id=='language' && Yii::$app->controller->id=='backend/messages'],
				]
			],
        ];
	}
}
