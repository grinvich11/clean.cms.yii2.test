<?php
namespace app\modules\language\controllers\backend;

class MainController extends \app\modules\admin\components\CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\language\models\Language';
	}
}
