<?php
namespace app\modules\language\controllers\backend;

use Yii;

class MessagesController extends \app\modules\admin\components\CrudAdminModuleController
{
	public $model='Message';

	public function actionUpdate($id, $language = NULL)
	{
		$model=$this->findModel($id, $language);
		$module=$model->getModule();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->load(Yii::$app->request->post()))
		{
			if($model->save()){
				if(Yii::$app->getRequest()->getIsPjax()){
					header('X-PJAX-URL: /'.$this->module->id.'/backend/main/index');
					return Yii::$app->runAction($this->module->id.'/backend/main/index');
				}else{
					return $this->redirect(['index']);
				}
			}
		}

		$this->breadcrumbs=array(
			['label'=>$module->name, 'url'=>array('index')],
			isset($model->name) ? $model->name : '',
			'Редактирование',
		);

		return $this->render('_form',array(
			'model'=>$model,
		));
	}

	protected function findModel($id, $language = NULL) {
		$tmp_model=new $this->model;

		if (isset($tmp_model->behaviors()['ml'])) {
			$model = call_user_func_array([&$this->model, 'model'], [])->multilang()->findByPk((int) $id);
		} else {
			$model = call_user_func_array([&$this->model, 'find'], [])->where(['id'=>$id, 'language'=>$language])->one();
		}
		if ($model === null)
			throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
}