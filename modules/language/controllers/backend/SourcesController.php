<?php
namespace app\modules\language\controllers\backend;

use Yii;
use app\modules\language\models\Message;
use app\modules\language\models\SourceMessage;
use app\modules\admin\components\CrudAdminModuleController;

class SourcesController extends CrudAdminModuleController
{
	public $name_attribute='message';

	public function getModelClass(){
		return $this->model='app\modules\language\models\SourceMessage';
	}

	public function actionExport(){
		$sql="SELECT * FROM {{source_message}}";
		$all=Yii::$app->db->createCommand($sql)->queryAll();
		if($all){
			$cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
			\PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
			$objPHPExcel = new \PHPExcel();

			$objPHPExcel->getProperties()->setCreator("Clean.CMS")->setLastModifiedBy("Clean.CMS");

			$sheet=$objPHPExcel->getActiveSheet();
			$r=1; $i=0;

			$sql="SELECT l.locale FROM {{language}} AS l ORDER BY l.position ASC";
			$langs=Yii::$app->db->createCommand($sql)->queryAll();

			$sheet->setCellValueByColumnAndRow($i++, $r, "ID");
			$sheet->setCellValueByColumnAndRow($i++, $r, "Категория");
			$sheet->setCellValueByColumnAndRow($i++, $r, "Оригинал");
			foreach ($langs as $value) {
				$sheet->setCellValueByColumnAndRow($i++, $r, "Перевод [{$value['locale']}]");
			}

			$border_style= ['borders' => ['allborders' => ['style' => \PHPExcel_Style_Border::BORDER_THIN,'color' => ['argb' => '766f6e']]]];
			$sheet->getStyle("A1:{$this->letter_($i)}1")->applyFromArray($border_style);
			$sheet->getStyle("A1:{$this->letter_($i)}1")->getFont()->setBold(true);

			$r=2;
			foreach ($all as $item) {
				$i=0;

				$sheet->setCellValueByColumnAndRow($i++, $r, $item['id']);
				$sheet->getCell($this->letter_($i).$r)->setDataType(\PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->setCellValueByColumnAndRow($i++, $r, $item['category']);
				$sheet->getCell($this->letter_($i).$r)->setDataType(\PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->setCellValueByColumnAndRow($i++, $r, $item['message']);

				$fl=$this->getLangsModel($item['id']);
				foreach ($langs as $value) {
					$sheet->getCell($this->letter_($i).$r)->setDataType(\PHPExcel_Cell_DataType::TYPE_STRING);
					$sheet->setCellValueByColumnAndRow($i++, $r, isset($fl[$value['locale']]['translation']) ? $fl[$value['locale']]['translation'] : '');
					$sheet->getCell($this->letter_($i).$r)->setDataType(\PHPExcel_Cell_DataType::TYPE_STRING);
				}
				$r++;
			}

			$sheet->setCellValueByColumnAndRow(0, ++$r, 'Для редактирования доступны только столбцы "Перевод[]"');
			$sheet->getStyle("{$this->letter_(1)}$r:{$this->letter_(1)}$r")->getFont()->setBold(true);

			$sheet->setSelectedCells('D2');

			$sheet->getColumnDimension($this->letter_(1))->setWidth(7);
			$sheet->getColumnDimension($this->letter_(2))->setWidth(20);
			$sheet->getColumnDimension($this->letter_(3))->setWidth(35);
			$sheet->getColumnDimension($this->letter_(4))->setWidth(35);
			$sheet->getColumnDimension($this->letter_(5))->setWidth(35);
			$sheet->getColumnDimension($this->letter_(6))->setWidth(35);

			$objPHPExcel->getActiveSheet()->setTitle('Переводы');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			// Sending headers to force the user to download the file
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.Yii::$app->params['siteName'].'-langs-'.date('d-m-Y H_i_s').'.xlsx"');
			header('Cache-Control: max-age=0');
			$s=$objWriter->save('php://output');

			$newname=Yii::$app->params['siteName'].'-langs-'.date('d-m-Y H_i_s').'.xlsx';
			if(file_exists("xls/langs/export/".$newname)) {
				$newname=Yii::$app->params['siteName'].'-langs-'.date('d-m-Y H_i_s').' '.uniqid().'.xlsx';
			}

			$objWriter->save(str_replace('.php', '.xlsx', "xls/langs/export/".$newname));
		}
	}

	private function getLangsModel($id){
		$sql="SELECT m.id, m.`language`, m.translation
			FROM
			message AS m
			INNER JOIN `language` AS l ON l.locale = m.`language`"
			. "WHERE m.id = {$id} "
			. "ORDER BY l.position ASC";
		$all=\Yii::$app->db->createCommand($sql)->queryAll();
		if($all){
			foreach ($all as $value) {
				$new[$value['language']]=$value;
			}
			$all=$new;
		}

		return $all;
	}

	private function letter_($c){
		$c = intval($c);
		if ($c <= 0) return '';
		$letter = '';

		while($c != 0)
		{
			$p = ($c - 1) % 26;
			$c = intval(($c - $p) / 26);
			$letter = chr(65 + $p) . $letter;
		}

		return $letter;
	}

	public function actionImport(){
		if(is_uploaded_file($_FILES['file']["tmp_name"])){
			//copy($_FILES['file']["tmp_name"], dirname($_SERVER['SCRIPT_FILENAME']).'/xls/'.uniqid().'.xls');
			$tname='xls/langs/import/'.Yii::$app->params['siteName'].'-langs-'.date('d-m-Y H_i_s').'.xls';
			move_uploaded_file($_FILES['file']["tmp_name"], $tname);

			$cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
			\PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
			$objPHPExcel = \PHPExcel_IOFactory::load(dirname($_SERVER['SCRIPT_FILENAME']).'/'.$tname);
			$row = 2; $end=true;

			$sql="SELECT l.locale FROM `language` AS l ORDER BY l.position ASC";
			$langs=\Yii::$app->db->createCommand($sql)->queryAll();

			do{
				if ($objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->getValue()=='')
					$end=false;
				else{
					$i=0;

					$data[$row-2]=new \stdClass();
					$data[$row-2]->id=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i++, $row)->getValue();
					$data[$row-2]->category=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i++, $row)->getValue();
					$data[$row-2]->message=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i++, $row)->getValue();

					foreach ($langs as $value) {
						$data[$row-2]->$value['locale']=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($i++, $row)->getValue();
					}

					$row++;
				}
			} while($end);
			unset($objPHPExcel);
			/* PHPExcel */
			$i=2;

			foreach ($data as $value) {
				$model= SourceMessage::findOne($value->id);
				if($model){
					foreach ($langs as $lang) {
						$find_lang=Message::find()->where(['language'=>$lang['locale'], 'id'=>$model->id])->one();
						if(!$find_lang){
							$find_lang=new Message();
							$find_lang->id=$model->id;
							$find_lang->language=$lang['locale'];
						}
						$find_lang->translation=$value->$lang['locale'];
						$find_lang->save();
					}
				}

				$i++;
			}

			Yii::$app->session->setFlash('success', 'Импорт успешно завершен');
		}else{
			Yii::$app->session->setFlash('danger', 'Не выбран файл для импорта');
		}

		return $this->redirect('/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id);
	}
}