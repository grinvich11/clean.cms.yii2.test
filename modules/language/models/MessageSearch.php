<?php

namespace app\modules\language\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\language\models\Message;

/**
 * MessageSearch represents the model behind the search form about `Message`.
 */
class MessageSearch extends Message
{	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['language', 'translation', 'parent_category', 'parent_message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();
		$query->joinWith(['parent']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['class' => 'app\modules\admin\components\Pagination'],
			'sort' => [
				'attributes' => [
					'id',
					'language',
					'translation',
					'parent_category' => [
						'asc' => ['source_message.category' => SORT_ASC],
						'desc' => ['source_message.category' => SORT_DESC],
						'default' => SORT_ASC
					],
					'parent_message' => [
						'asc' => ['source_message.message' => SORT_ASC],
						'desc' => ['source_message.message' => SORT_DESC],
						'default' => SORT_ASC
					],
				],
				'defaultOrder' => ['id' => SORT_DESC]
			]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'translation', $this->translation])
            ->andFilterWhere(['like', 'source_message.message', $this->parent_message])
            ->andFilterWhere(['like', 'source_message.category', $this->parent_category]);

        return $dataProvider;
    }
}
