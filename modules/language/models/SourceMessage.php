<?php

namespace app\modules\language\models;

use Yii;
use app\models\MainModel;
use app\modules\language\models\Message;

/**
 * This is the model class for table "source_message".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'message' => 'Оригинал',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }

	public function afterSave($insert, $changedAttributes)
    {
		if(isset($_POST['Message'])){
			foreach ($_POST['Message'] as $language=>$value) {
				$find=Message::find()->where(['language'=>$language, 'id'=>$this->id])->one();
				if(!$find)
					$find= new Message();
				$find->translation=trim($value['translation']);
				$find->language=$language;
				$find->id=$this->id;
				$find->save();
			}
		}

		return parent::afterSave($insert, $changedAttributes);
	}
}
