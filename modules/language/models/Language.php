<?php

namespace app\modules\language\models;

use Yii;
use app\models\MainModel;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $locale
 * @property integer $published
 * @property integer $position
 * @property string $created
 * @property string $modified
 */
class Language extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'locale', 'locale_full'], 'required'],
            [['published', 'position'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 4],
			[['locale'], 'string', 'max' => 7],
            [['locale_full'], 'string', 'max' => 5],
            [['locale', 'locale_full'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Язык',
            'alias' => 'Алиас',
            'locale' => 'Локаль',
            'locale_full' => 'Локаль полная',
            'published' => 'Опубликован',
            'position' => 'Порядок',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

	public function afterSave($insert, $changedAttributes)
    {
		$this->updateLangCache();

		return parent::afterSave($insert, $changedAttributes);
	}

	public function afterDelete()
    {
        $this->updateLangCache();

        parent::afterDelete();
    }

	private function updateLangCache(){
		$all=Language::find()->where(['published'=>1])->orderBy(['position'=>SORT_ASC])->all();
		if($all){
			foreach ($all as $lang) {
				$str[]=$lang->alias;
				$locale[$lang->alias]=$lang->locale_full;

				if(Yii::$app->cache)
					Yii::$app->cache->delete('LangHelper::getCurrentLang_'.$lang->locale_full);
			}
			file_put_contents('../runtime/lang_locales.txt', serialize($locale));
		}

		if(Yii::$app->cache)
			Yii::$app->cache->delete('LangHelper::getLangModels');
	}
}
