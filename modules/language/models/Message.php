<?php

namespace app\modules\language\models;

use Yii;
use app\models\MainModel;
use app\modules\language\models\SourceMessage;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends MainModel
{
	public $parent_category;
	public $parent_message;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Язык',
            'translation' => 'Перевод',
            'parent_category' => 'Категория',
            'parent_message' => 'Оригинал',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }
}
