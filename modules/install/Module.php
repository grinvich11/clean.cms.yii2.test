<?php
namespace app\modules\install;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Установка';
	public $model='InstallForm';
	public $not_menu=true;
	public $delete=false;

	public static function rules()
    {
        return array(
            'install'=>'install/frontend/main/index',
        );
    }
}
