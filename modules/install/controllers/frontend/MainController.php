<?php
namespace app\modules\install\controllers\frontend;

use Yii;
use app\modules\install\models\InstallForm;
use app\helpers\AdminHelper;
use app\modules\admin\components\MainInstallHelper;
use app\modules\admin\models\AdminModules;
use app\modules\admin_users\models\AdminUsers;
use yii\web\NotFoundHttpException;

class MainController extends \yii\web\Controller
{
	public $layout='admin_column';
	public $install=false;
	public $body_class='skin-blue';
	public $breadcrumbs;

	public function init()
	{
		if(is_file('../runtime/installed')){
			Yii::$app->db;
			$this->install=true;
		}else{
			Yii::$app->user->logout();
			$this->install=false;
		}

		if($this->install)
			throw new NotFoundHttpException('The requested page does not exist.');

		return parent::init();
	}

	public function checkRequirements(){
		$error=false;
		if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN'){
			if(substr(sprintf('%o', fileperms('../runtime')), -4)!=='0777'){
				$text[]='Права на директорию runtime не равны 0777!';
				$error=true;
			}
			if(substr(sprintf('%o', fileperms('assets')), -4)!=='0777'){
				$text[]='Права на директорию runtime не равны 0777!';
				$error=true;
			}
		}

		if(!is_writable(Yii::$app->basePath . "/config/db.php")){
			$text[]='Необходимы права на файл "config/db.php" для сохранения конфига базы';
			$error=true;
		}

		if(!is_writable('uploads')){
			$text[]='Необходимы права для записи в папку uploads';
			$error=true;
		}

		if(!is_writable('images')){
			$text[]='Необходимы права для записи в папку images';
			$error=true;
		}

		if($error)
			Yii::$app->session->setFlash('error', implode('<br>', $text));

		return $error;
	}

	public function actionIndex()
	{
		$model=new InstallForm();
		$modules=AdminHelper::returnModules();
		if(isset($modules['admin'])) unset($modules['admin']);
		if(isset($modules['install'])) unset($modules['install']);

		if(isset($_POST['InstallForm'])) {
            if($model->load(Yii::$app->request->post()) && $model->validate())
            {
				try
                {
					$connectionString = "mysql:host={$model->db_host};dbname={$model->db_name};port={$model->db_port}";

					Yii::$app->db->dsn=$connectionString;
					Yii::$app->db->username=$model->db_user;
					Yii::$app->db->password=$model->db_password;
					Yii::$app->db->charset='utf8';
					Yii::$app->db->tablePrefix='';
					Yii::$app->db->emulatePrepare=true;
					Yii::$app->db->open();

                    $dbParams = [
                        'class' => 'yii\db\Connection',
                        'dsn' => $connectionString,
                        'username' => $model->db_user,
                        'password' => $model->db_password,
                        //'emulatePrepare' => true,
                        'charset' => 'utf8',
                        //'enableParamLogging' => false,
                        //'enableProfiling' => false,
                        //'schemaCachingDuration' => 3600,
                        'tablePrefix' => ''
                    ];

					$db_config = Yii::$app->basePath . "/config/db.php";

                    $fw = fopen($db_config, 'w+');

                    if (!$fw) {
                        $model->addError('', "Не могу открыть файл '{$db_config}' для записи!");
                    } else {
                        fwrite($fw, "<?php\n return " . var_export($dbParams, true) . " ;\n?>");
                        fclose($fw);

                        @chmod($db_config, 0666);

						$ih=new MainInstallHelper('admin');
						$ih->install(true);

						foreach ($modules as $name_module=>$module) {
							$installed=false;
							//if(($module->not_menu || !$module->delete) || isset($_POST['Module'][$name_module])){
							if(!$module->delete || (isset($_POST['Module'][$name_module]) && $_POST['Module'][$name_module]==1)){
								$ih=new MainInstallHelper($name_module);
								$ih->install(true);
								$installed=true;
							}

							if(!$module->not_menu){
								$find=new AdminModules();
								$find->module=$name_module;
								$find->name=$module->name;
								$find->version=$module->version;
								$find->delete=(int)$module->delete;
								$find->state=(int)$installed;
								$find->save();
							}
						}

						$user=new AdminUsers();
						$user->email=$model->admin_email;
						$user->password=$user->hashPassword($model->admin_password);
						$user->admin=1;
						if($user->save(false)){
							$login=Yii::$app->user->login($user, 3600*24*30);
							if($login){
								Yii::$app->session->set('email', $user->email);
								//Yii::$app->session->set('login', $user->login);
								Yii::$app->session->set('images', 'no_image.png');
								Yii::$app->session->set('admin', 1);
							}
						}

						file_put_contents('../runtime/installed',1);
						file_put_contents('../runtime/lang_locales.txt',  serialize(['ru'=>'ru-RU']));

                        return $this->redirect(['/admin']);
                    }

                } catch (Exception $e) {
                    $model->addError('', $e->getMessage());
                }
            }
        }

		return $this->render('index', ['model'=>$model,'modules'=>$modules, 'error'=>$this->checkRequirements()]);
	}
}