<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs']=['Установка'];
if(!$error):
?>
<div class="box box-primary">
<? $form = ActiveForm::begin([
	'id'=>'install-form',
	/*'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'type'=>'horizontal',*/
]); ?>
	<div class="box-body">

		<p class="note">Поля отмеченные <span class="required">*</span> являются обязательными.</p>

		<?php echo $form->errorSummary($model); ?>

		<fieldset>
			<legend>Настройки базы данных</legend>

			<?php echo $form->field($model,'db_host'); ?>
			<?php echo $form->field($model,'db_port'); ?>
			<?php echo $form->field($model,'db_user'); ?>
			<?php echo $form->field($model,'db_password'); ?>
			<?php echo $form->field($model,'db_name'); ?>
		</fieldset>

		<fieldset>
			<legend>Данные администратора</legend>

			<?php echo $form->field($model,'admin_email'); ?>
			<?php echo $form->field($model,'admin_password'); ?>
		</fieldset>

		<fieldset>
			<legend>Модули</legend>

			<?php
			if($modules){
				foreach ($modules as $name=>$module) {
					$disabled=$module->not_menu || !$module->delete;
					if($disabled)
						$disabled_modules.='<div class="form-group">'
						. '<label for="Module_'.$name.'" class="col-sm-3 control-label required">'.$module->name.($disabled ? ' <span class="required">*</span>' : '').'</label>'
						. '<div class="col-sm-6">'
							. Html::checkBox('Module['.$name.']', $disabled, array('id'=>'Module_'.$name, ($disabled ? 'disabled' : 'class')=>($disabled ? 'disabled' : 'class')))
						. '</div>'
					. '</div>'.'<div class="clearfix"></div>';
					else
						$not_disabled_modules.='<div class="form-group">'
						. '<label for="Module_'.$name.'" class="col-sm-3 control-label required">'.$module->name.($disabled ? ' <span class="required">*</span>' : '').'</label>'
						. '<div class="col-sm-6">'
							. Html::checkBox('Module['.$name.']', $disabled || isset($_POST['Module'][$name]), array('id'=>'Module_'.$name, ($disabled ? 'disabled' : 'class')=>($disabled ? 'disabled' : 'class')))
						. '</div>'
					. '</div>'.'<div class="clearfix"></div>';
				}
				echo $disabled_modules;
				echo $not_disabled_modules;
			}
			?>
		</fieldset>
	</div>

	<div class="box-footer">
		<button type="submit" class="btn btn-primary">Установить</button>
	</div>

<?php ActiveForm::end(); ?>
</div>
<? else: ?>
	<a class="btn btn-primary" href="">Обновить страницу</a>
<? endif; ?>