<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

if($items):
foreach ($items as $value): ?>
	<div class="row">
		<div class="col-xs-8 col-sm-9">
			<a href="#"><img src="/images/users/images/admin/<?=  app\helpers\SiteHelper::returnOneImages($value->user->images);?>" class="pull-left"/></a>
			<p class="om-graduates__review-text">
				<?=$value->created;?> <a href="#"><?=$value->user->name;?></a>
				<br>
				 <?=$value->text;?>
			</p>
		</div>
	</div>
	<hr>
<? endforeach;
endif;?>


<?
$form = ActiveForm::begin([
	'id'=>'form',
	'action'=>  \yii\helpers\Url::to(['responses/frontend/main/create']),
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'autocomplete'=>'off',
	],
]); ?>

	<label>Добавить отзыв</label>
	<?= $form->field($model, 'text', ['template'=>'{input}{error}'])->textArea(['rows' => 4, 'required']) ?>
	<?= $form->field($model, 'item_id', ['template'=>'{input}'])->hiddenInput()?>
	<?= $form->field($model, 'type', ['template'=>'{input}'])->hiddenInput()?>

	<div class="box-footer">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>

<?php ActiveForm::end(); ?>