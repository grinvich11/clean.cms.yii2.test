<?php
namespace app\modules\responses\widgets;

use yii\base\Widget;
use app\modules\responses\models\Responses;

class responsesWidget extends Widget
{
	public $item_id;
	public $type=0;

    public function run()
    {
		$items=Responses::find()->where(['item_id'=>$this->item_id, 'type'=>$this->type, 'moderated'=>1])->orderBy(['created'=>SORT_DESC])->all();
		$model=new Responses();
		$model->type=$this->type;
		$model->item_id=$this->item_id;

		return $this->render('responses', ['items'=>$items, 'model'=>$model]);
    }
}