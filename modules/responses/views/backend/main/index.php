<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();

    echo GridView::widget([
		'createButton' => false,
		'actionsAllow' => false,
		'settingsButton' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			/*[
				'class' => 'yii\grid\CheckboxColumn',
			],*/
            [
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => 'id',
			],
            'text:ntext',
			[
				'attribute' => 'user_id',
				'label'=>'Оставил пользователь',
				'format'=>'raw',
				'value'=>function($model){
					return '<a href="/users/backend/main/update?id='.$model->user_id.'" data-pjax=0>'.$model->user->name.'</a>';
				},
				'contentOptions'=>array('style'=>'width:140px;'),
				'filter'=>\yii\helpers\ArrayHelper::map(\app\modules\users\models\Users::find()->innerJoin('responses', 'responses.user_id=users.id')->groupBy(['id'])->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name'),
			],
			[
				'label'=>'Игра',
				'format'=>'raw',
				'value'=>function($model){
					$team_user=app\modules\games\models\TeamsUsers::findOne($model->item_id);
					if($team_user){
						$game=$team_user->teams->games;
						return '<a href="/games/backend/main/update?id='.$game->id.'" data-pjax=0 target=_blank>'.$game->name.'</a>';
					}
				},
			],
			[
				'label'=>'Об игроке',
				'format'=>'raw',
				'value'=>function($model){
					$team_user=app\modules\games\models\TeamsUsers::findOne($model->item_id);
					if($team_user){
						$user=$team_user->user;
						return '<a href="/users/backend/main/update?id='.$user->id.'" data-pjax=0 target=_blank>'.$user->name.'</a>';
					}
				},
			],
            //'item_id',
            // 'type',
            // 'created',
            [
				'class' => '\app\modules\admin\components\widgets\ToggleColumn',
				'attribute' => 'moderated',
			],
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
				'template' => '{delete}'
			],
        ],
    ]);

PjaxGrid::end();
