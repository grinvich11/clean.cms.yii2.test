<?php

namespace app\modules\responses\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\responses\models\Responses;

/**
 * ResponsesSearch represents the model behind the search form about `Responses`.
 */
class ResponsesSearch extends Responses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'item_id', 'type', 'moderated'], 'integer'],
            [['text', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Responses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['class' => 'app\modules\admin\components\Pagination'],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'item_id' => $this->item_id,
            'type' => $this->type,
            'moderated' => $this->moderated,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
