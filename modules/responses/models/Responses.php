<?php

namespace app\modules\responses\models;

use Yii;
use app\models\MainModel;
use app\modules\users\models\Users;

/**
 * This is the model class for table "responses".
 *
 * @property integer $id
 * @property string $text
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $type
 * @property integer $moderated
 * @property string $created
 * @property string $modified
 *
 * @property Users $user
 */
class Responses extends MainModel
{
	public $types=[
		'0'=>'На странице профиля',//Users
		'1'=>'В игре',//TeamUsers
	];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'responses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
            [['user_id', 'item_id', 'type', 'moderated'], 'integer'],
            [['created', 'modified'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Отзыв',
            'user_id' => 'Пользователь',
            'item_id' => 'Id записи',
            'type' => 'Тип записи',
            'moderated' => 'Одобрен',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

}
