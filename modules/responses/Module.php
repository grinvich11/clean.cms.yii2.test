<?php

namespace app\modules\responses;

use Yii;
use app\components\Module as ParentModule;

class Module extends ParentModule
{
	public $version='1';
	public $name='Отзывы';

    public function getMenuItems(){
		if(Yii::$app->user->identity->getIsAdmin())
        return [
			[
				'label' => 'Отзывы',
				'icon' => 'fa fa-comment',
				'url' => '/responses/backend/main',
				'active' => Yii::$app->controller->module->id=='responses',
				'count'=> Yii::$app->db->createCommand("SELECT COUNT(id) FROM {{responses}}")->queryScalar(),
				'count_new'=> Yii::$app->db->createCommand("SELECT COUNT(id) FROM {{responses}} WHERE moderated=0")->queryScalar()
			]
        ];
    }

    public static function rules(){
        return [
			'responses/frontend/main/create'=>'responses/frontend/main/create',
			'responses/frontend/main/ajax'=>'responses/frontend/main/ajax',
        ];
    }

}
