<?php

namespace app\modules\responses\controllers\frontend;

use Yii;
use app\modules\responses\models\Responses;
use app\components\FrontController;
use yii\widgets\ActiveForm;
use yii\web\Response;

class MainController extends FrontController
{
	public function actionCreate()
	{
		$model=new Responses();

		$model->text=$_POST['Responses']['text'];
		$model->item_id=$_POST['Responses']['item_id'];
		$model->type=$_POST['Responses']['type'];
		$model->user_id=Yii::$app->user->getId();

		if($model->save()){
			Yii::$app->session->setFlash('success', 'Ваш отзыв добавлен');
			return $this->redirect($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '/');
		}else{
			Yii::$app->session->setFlash('success', 'Произошла ошибка при добавлении');
			return $this->redirect($_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '/');
		}
	}

	public function actionAjax()
	{
		$model=new Responses();

		if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && isset($_POST['ajax'])) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}

		$model->text=$_POST['Responses']['text'];
		$model->item_id=$_POST['Responses']['item_id'];
		$model->type=$_POST['Responses']['type'];
		$model->user_id=Yii::$app->user->getId();

		if($model->save()){
			return Yii::t('game.response', 'Ваш отзыв будет добавлен, после модерации');
		}else{
			return 'error';
		}

	}
}
