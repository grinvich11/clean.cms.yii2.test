<?php

use yii\db\Migration;

class m150612_185306_create_table_responses extends Migration {

    public $tableName = '{{responses}}';

    public function safeUp() {
        $this->createTable(
			$this->tableName, [
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'text' => 'TEXT NOT NULL COMMENT "Отзыв"',
				'user_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Пользователь"',
				'item_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Id записи"',
				'type' => 'TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Тип записи"',

				'moderated' => 'TINYINT(1) UNSIGNED DEFAULT 0 COMMENT "Одобрен"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			], 'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
        );

		$this->createIndex('idx-responses-user_id', $this->tableName, 'user_id');
		$this->createIndex('idx-responses-moderated', $this->tableName, 'moderated');
		$this->createIndex('idx-responses-item_id', $this->tableName, 'item_id');
		$this->createIndex('idx-responses-type', $this->tableName, 'type');

		$this->addForeignKey('fk-responses-user_id', $this->tableName, 'user_id', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropTable($this->tableName);
    }

}
