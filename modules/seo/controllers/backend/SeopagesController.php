<?php
namespace app\modules\seo\controllers\backend;

use Yii;
use yii\web\Response;
use app\modules\admin\components\CrudAdminModuleController;

class SeopagesController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\seo\models\SeoPages';
	}

	public function actionAjax($id)
	{
		$model=$this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			if(!$model->validate()){
				return ActiveForm::validate($model);
			}else
				$model->save();

			$response = [
				'success'=>'true'
			];
			return $response;
		}else{
			return $this->renderAjax('_ajax', [
				'model'=>$model
			]);
		}
	}
}
