<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use app\modules\seo\widgets\MetaTags;

$form = ActiveForm::begin([
	'id'=>'form-seo-pages',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'options'=>[
		'autocomplete'=>'off',
	],
]); ?>


	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->field($model,'name')->textInput(['readonly'=>'readonly']); ?>
	<?php echo MetaTags::widget(['model' => $model,'form' => $form, 'enable_footer'=>false]); ?>

	<div class="box-footer" style="padding-left:0;">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>

<?php ActiveForm::end() ?>