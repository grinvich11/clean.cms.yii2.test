<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use app\modules\seo\widgets\MetaTags;

$form = ActiveForm::begin([
    'id'=>'form',
    'enableClientValidation'=>true,
    'enableAjaxValidation'=>false,
    'validateOnSubmit'=>true,
    'layout'=>'horizontal',
    'options'=>[
        'enctype'=>'multipart/form-data',
        'autocomplete'=>'off',
		'data-pjax'=>1
    ],
]); ?>
	<? $this->beginBlock('base'); ?>

		<?php echo $form->errorSummary($model); ?>

		<?php echo $form->field($model,'name')->textInput(['readonly'=>'readonly']); ?>
		<?php echo MetaTags::widget(['model' => $model,'form' => $form, 'enable_footer'=>false]); ?>

		<div class="box-footer">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
        <div class="nav-tabs-custom">
        <?= Tabs::widget([
            'items'=>[
                ['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true],
            ]
        ]);
        ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>