<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\AdminModules;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();

    echo GridView::widget([
		'dataProvider'=>$dataProvider,
		'filterModel' =>$searchModel,
		'createButton' =>false,
		'settingsButton' =>false,
		'actionsAllow'=>false,
		'columns'=>[
			[
				'attribute'=>'name',
				'format'=>'raw',
				'value'=>function ($model) {
					return Html::a(Html::encode($model->name), ["update", "id"=>$model->id], ['data-pjax'=>0]);
				},
				'contentOptions'=>['style'=>'width:340px;']
			],
			[
				'attribute'=>'module',
				'format'=>'raw',
				'value'=>function ($model) {
					if($model->module){
						$module=Yii::$app->getModule($model->module);

						return Html::a($module->name, "/admin/modules/update/".$model->module, ['data-pjax'=>0]);
					}
				},
				'filter'=>ArrayHelper::map(AdminModules::find()->where(['admin_modules.state'=>1])->innerJoin('seo_pages', 'seo_pages.`module`=admin_modules.module')->groupBy(['admin_modules.module'])->all(), 'module', 'name'),
			],
			[
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
				'template' => '{update}',
			],
		]
    ]);

PjaxGrid::end();