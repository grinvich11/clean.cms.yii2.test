<?php
/**
 * Created by PhpStorm.
 * User: artemshmanovsky
 * Date: 13.03.15
 * Time: 19:16
 */

namespace app\modules\seo\components;

use Yii;

class MetaTagsComponent extends \v0lume\yii2\metaTags\MetaTagsComponent {

    public function register($model=null)
    {
        if($this->generateCsrf && Yii::$app->request->enableCsrfValidation)
        {
            Yii::$app->view->registerMetaTag(['name' => 'csrf-param', 'content' => Yii::$app->request->csrfParam], 'csrf-param');
            Yii::$app->view->registerMetaTag(['name' => 'csrf-token', 'content' => Yii::$app->request->csrfToken], 'csrf-token');
        }

        if(isset($model) && $model->getBehavior(self::$behaviorName))
        {
            $meta_tag = $model->getBehavior(self::$behaviorName)->getModel(substr(Yii::$app->language, 0, 2));

			Yii::$app->view->title=$meta_tag->title;
            //Yii::$app->view->registerMetaTag(['name' => 'title', 'content' => $meta_tag->title], 'title');
			if(!empty($meta_tag->keywords))
				Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_tag->keywords], 'keywords');
			if(!empty($meta_tag->description))
				Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $meta_tag->description], 'description');

            if($this->generateOg)
            {
                Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $meta_tag->title], 'og:title');
                Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => $meta_tag->description], 'og:description');
                Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => \yii\helpers\Url::to('', true)], 'og:url');
            }
        }
    }
}
