<?php
/**
 * Created by PhpStorm.
 * User: artemshmanovsky
 * Date: 12.03.15
 * Time: 1:52
 */

namespace app\modules\seo\components;

use Yii;
use yii\db\ActiveRecord;
use app\helpers\LangHelper;
use app\modules\seo\models\MetaTag;
use v0lume\yii2\metaTags\MetaTagBehavior as ParentBehavior;

class MetaTagBehavior extends ParentBehavior
{
    private $_model = null;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }


    public function afterSave($event)
    {
        $attributes = Yii::$app->request->post('MetaTag', Yii::$app->request->get('MetaTag', null) );

        if($attributes)
        {
			$attributes=[];
			$attributes['model_id'] = $this->owner->id;
			$attributes['model']  = (new \ReflectionClass($this->owner))->getShortName();

			foreach (LangHelper::getLang(false) as $lang) {
				if (isset($_POST['MetaTag'][$lang])) {
					$model = $this->getModel($lang);

					$attributes['language_id'] = $lang;

					$model->setAttributes($_POST['MetaTag'][$lang]);

					$model->attributes = $attributes;
					$model->save();
				}
			}
        }
    }


    public function afterDelete($event)
    {
        MetaTag::deleteAll([
            'model_id' => $this->owner->id,
            'model'  => (new \ReflectionClass($this->owner))->getShortName()
        ]);
    }


    public function getModel($lang = NULL)
    {
        $model = MetaTag::findOne([
            'model_id' => $this->owner->id,
            'model'  => (new \ReflectionClass($this->owner))->getShortName(),
            'language_id'  => $lang
        ]);

        if($model == null)
            $model = new MetaTag();

        $this->_model = $model;

        return $model;
    }


    public function getTitle()
    {
        $model = $this->_model;
        if(!isset($model))
            $model = $this->getModel(Yii::$app->language);

        return isset($model) ? $model->title : '';
    }

    public function getDescription()
    {
        $model = $this->_model;
        if(!isset($model))
            $model = $this->getModel(Yii::$app->language);

        return isset($model) ? $model->description : '';
    }

    public function getKeywords()
    {
        $model = $this->_model;
        if(!isset($model))
            $model = $this->getModel(Yii::$app->language);

        return isset($model) ? $model->keywords : '';
    }
}
