<?php

namespace app\modules\seo\models;

use Yii;
use app\models\MainModel;
use yii\helpers\ArrayHelper;
use app\modules\seo\components\MetaTagBehavior;

/**
 * This is the model class for table "seo_pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $module
 * @property string $created
 * @property string $modified
 */
class SeoPages extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'module'], 'required'],
            [['created', 'modified'], 'safe'],
            [['name', 'alias', 'module'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Алиас',
            'module' => 'Модуль',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'MetaTag' => [
					'class' => MetaTagBehavior::className(),
				],
			]
		);
	}
}
