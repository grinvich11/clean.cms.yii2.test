<?php

namespace app\modules\seo\models;

use Yii;
use app\models\MainModel;
use v0lume\yii2\metaTags\MetaTags;

/**
 * This is the model class for table "meta_tags".
 *
 * @property integer $id
 * @property string $model
 * @property integer $model_id
 * @property string $language_id
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $created
 * @property string $modified
 */
class MetaTag extends MainModel
{
    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return '{{%meta_tags}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
     public function rules()
    {
        return [
            [['model', 'model_id', 'language_id'], 'required'],
            [['model_id'], 'integer'],
            [['description'], 'string'],
            [['created', 'modified'], 'safe'],
            [['model', 'title', 'keywords'], 'string', 'max' => 255],
            [['language_id'], 'string', 'max' => 7],
            [['model', 'model_id', 'language_id'], 'unique', 'targetAttribute' => ['model', 'model_id', 'language_id'], 'message' => 'The combination of Model, Model ID and Language ID has already been taken.']
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'title' => MetaTags::t('messages', 'model_title'),
            'keywords' => MetaTags::t('messages', 'model_keywords'),
            'description' => MetaTags::t('messages', 'model_description'),
        ];
    }
}
