<?php

use yii\db\Schema;
use yii\db\Migration;

class m150930_144506_create_table_meta_tags extends Migration {

    public function safeUp()
    {
        $this->createTable('{{%meta_tags}}', [
            'id' => Schema::TYPE_PK,
            'model' => Schema::TYPE_STRING . ' NOT NULL',
            'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language_id' => 'VARCHAR(7) NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'keywords' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'created' => Schema::TYPE_DATETIME,
            'modified' => Schema::TYPE_DATETIME,

			'CONSTRAINT fk_language_id_meta_tag FOREIGN KEY (language_id) REFERENCES {{language}} (locale) ON DELETE CASCADE ON UPDATE CASCADE',
        ]);

        $this->createIndex('object', '{{%meta_tags}}', ['model', 'model_id', 'language_id'], true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%meta_tags}}');
    }
}