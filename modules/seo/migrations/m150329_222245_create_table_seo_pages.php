<?php

class m150329_222245_create_table_seo_pages extends yii\db\Migration
{
	public $tableName='{{seo_pages}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'name' => 'VARCHAR(255) NOT NULL COMMENT "Название"',
				'alias' => 'VARCHAR(255) NOT NULL COMMENT "Алиас"',
				'module' => 'VARCHAR(255) NOT NULL COMMENT "Модуль"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}