<?
use app\helpers\LangHelper;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

foreach (LangHelper::getLangModels() as $key=>$lang) {
	$this->beginBlock('seo_lang_'.$lang->locale);
		echo $form->field($model, '['.$lang->locale.']title', [
			'labelOptions'=>['label'=>"Заголовок (title) [$lang->alias]"],
			'inputOptions'=>['value'=>$models[$lang->locale]->title],
		])->textInput(['maxlength' => 255]);

		echo $form->field($model, '['.$lang->locale.']keywords', [
			'labelOptions'=>['label'=>"Ключевые слова (keywords) [$lang->alias]"],
			'inputOptions'=>['value'=>$models[$lang->locale]->keywords],
		])->textInput(['maxlength' => 255]);

		$model->description=$models[$lang->locale]->description;
		echo $form->field($model, '['.$lang->locale.']description', [
			'labelOptions'=>['label'=>"Описание (description) [$lang->alias]"]
		])->textArea();
	$this->endBlock();

	$seo_lang_tab[]=['label'=>$lang->name, 'content'=>$this->blocks['seo_lang_'.$lang->locale], 'active'=>$key==0];
}

?>

<div class="box-body">
	<div class="nav-tabs-custom">
	<?
	echo Tabs::widget([
		'id'=>'seo_lang_tab',
		'items'=>$seo_lang_tab
	]);
	?>
	</div>
</div>

<? if($enable_footer): ?>
<div class="box-footer">
	<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
	<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id.'/'.Yii::$app->controller->id;?>">К списку</a>
</div>
<? endif; ?>