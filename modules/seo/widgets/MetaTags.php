<?php
/**
 * Created by PhpStorm.
 * User: artemshmanovsky
 * Date: 11.03.15
 * Time: 15:03
 */

namespace app\modules\seo\widgets;

use Yii;
use yii\base\Widget;
use yii\base\Exception;

use v0lume\yii2\metaTags\MetaTagsComponent;
use v0lume\yii2\metaTags\models\MetaTag;


class MetaTags extends Widget
{
    public $model;
    public $form;
	public $enable_footer=true;


    public function init()
    {
        parent::init();
        self::registerTranslations();

        if (!$this->model->getBehavior(MetaTagsComponent::$behaviorName))
        {
            throw new Exception(self::t('messages', 'widget_behavior_exception {behaviorName}', ['behaviorName' => MetaTagsComponent::$behaviorName]), 500);
        }
    }


    public static function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['metaTags/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'sys',
            'basePath' => '@vendor/v0lume/yii2-meta-tags/messages',
            'fileMap' => [
                'metaTags/messages' => 'messages.php',
            ],
        ];
    }


    public function run()
    {
        $model = new MetaTag;

        if(!$this->model->isNewRecord)
        {
            $meta_tags = MetaTag::find()->where([
                'model_id' => $this->model->id,
                'model'  => (new \ReflectionClass($this->model))->getShortName()
            ])->all();

			if(isset($meta_tags)){
				foreach ($meta_tags as $meta_tag) {
					$models[$meta_tag->language_id] = $meta_tag;
				}
			}
        }

        return $this->render('MetaTags', [
            'model' => $model,
            'models' => $models,
            'form' => $this->form,
			'enable_footer' => $this->enable_footer
        ]);
    }


    public static function t($category, $message, $params = [], $language = null)
    {
        self::registerTranslations();

        return Yii::t('metaTags/' . $category, $message, $params, $language);
    }
}
