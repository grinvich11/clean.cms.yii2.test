<?php

namespace app\modules\news\controllers\frontend;

use Yii;
use app\helpers\SeoHelper;
use app\modules\news\models\News;
use app\components\FrontController;
use yii\data\ActiveDataProvider;

class MainController extends FrontController
{
	public function actionIndex()
	{
		SeoHelper::registerSeoPage('news_page');

		$dataProvider = new ActiveDataProvider([
            'query' => News::find()->where(['published'=>1]),
            'pagination' => [
                'pageSize' => Yii::$app->params['news_pagination'],
				'pageParam'=>'p',
				'pageSizeParam'=>'pp'
            ],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
	}

	public function actionView()
	{
		$model=$this->findModel($_GET['id']);
		Yii::$app->metaTags->register($model);

		return $this->render('view', [
			'model'=>$model,
		]);
	}

	protected function findModel($id)
	{
		$model=News::findOne($id);
		if($model===null || $model->published==0)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
}
