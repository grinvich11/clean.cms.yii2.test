<?php

namespace app\modules\news\controllers\backend;

use app\modules\admin\components\CrudAdminModuleController;

class MainController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\news\models\News';
	}
}
