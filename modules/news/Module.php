<?php

namespace app\modules\news;

use Yii;
use app\components\Module as ParentModule;

class Module extends ParentModule
{
	public $version='1';
	public $name='Новости';
	public $model='News';

	public $install=[
		'uploads_dir'=>true,
		'images'=>[
			'images'=>[
				'admin'=>[
					'width'=>88,
					'heigth'=>88,
					'method'=>'auto'
				],
				'middle'=>[
					'width'=>290,
					'heigth'=>136,
					'method'=>'crop'
				]
			]
		]
	];

	public function getMenuItems(){
		return [
			[
				'label' => 'Новости',
                'icon' => 'fa fa-newspaper-o',
                'url' => '/news/backend/main',
                'active' => Yii::$app->controller->module->id=='news',
				'count'=> Yii::$app->db->createCommand("SELECT COUNT(id) FROM {{news}}")->queryScalar(),
				'bg'=> 'green',
			]
        ];
	}

	public static function rules()
    {
        return [
            'news'=>'news/frontend/main/index',
            '<alias:.*?>-n<id:\d+>'=>'news/frontend/main/view',
        ];
    }
}
