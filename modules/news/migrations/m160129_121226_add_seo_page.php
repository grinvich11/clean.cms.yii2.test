<?php

use yii\db\Schema;
use yii\db\Migration;

class m160129_121226_add_seo_page extends Migration {
	public $tableName='{{seo_pages}}';
	public $moduleName='news';

	public function safeUp()
	{
		$this->insert(
			$this->tableName,
			[
				'module' => $this->moduleName,
				'alias' => $this->moduleName.'_page',
				'name' => 'Новости'
			]
		);
	}

	public function safeDown()
	{
		$this->delete($this->tableName, 'module=:module', [':module'=>$this->moduleName]);
	}
}