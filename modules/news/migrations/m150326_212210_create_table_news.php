<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_212210_create_table_news extends Migration
{
	public $tableName='{{news}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			[
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'name' => 'VARCHAR(255) NOT NULL COMMENT "Заголовок"',
				'preview' => 'TEXT NOT NULL COMMENT "Превью"',
				'text' => 'TEXT NOT NULL COMMENT "Контент"',
				'posted' => 'DATETIME NOT NULL COMMENT "Дата создания"',
				'images' => 'VARCHAR(255) DEFAULT NULL COMMENT "Изображение"',

				'published' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT "Опубликована"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}