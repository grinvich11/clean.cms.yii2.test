<?php

class m150326_212312_create_config_params extends yii\db\Migration
{
	public $tableName='{{config}}';
	public $moduleName='news';

	public function safeUp()
	{
		$this->insert(
			$this->tableName,
			array(
				'module' => $this->moduleName,
				'param' => $this->moduleName.'_pagination',
				'value' => 10,
				'default' => 10,
				'label' => 'Кол-во новостей на странице',
				'type' => 'int'
			)
		);

		$this->insert(
			$this->tableName,
			array(
				'module' => $this->moduleName,
				'param' => $this->moduleName.'_pagination_widget',
				'value' => 4,
				'default' => 4,
				'label' => 'Кол-во новостей в виджете',
				'type' => 'int'
			)
		);
	}

	public function safeDown()
	{
		$this->delete($this->tableName, 'module=:module', [':module'=>$this->moduleName]);
	}
}