<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			[
				'class' => 'yii\grid\CheckboxColumn',
			],
			[
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => 'id',
			],
            'name',
            //'preview:ntext',
            // 'text:ntext',
            'posted',
            // 'created',
            [
				'class' => '\app\modules\admin\components\widgets\ImageColumn',
				'attribute' => 'images',
			],
            [
				'class' => '\app\modules\admin\components\widgets\ToggleColumn',
				'attribute' => 'published',
			],
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
			],
        ],
    ]);

PjaxGrid::end();