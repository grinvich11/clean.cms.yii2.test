<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use app\modules\seo\widgets\MetaTags;

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
		'data-pjax'=>1
	],
]); ?>
	<? $this->beginBlock('base'); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'preview')->textarea(['rows' => 3]) ?>

    <?= app\modules\admin\widgets\redactorWidget::widget(['form'=>$form, 'model'=>$model, 'attribute'=>'text']); ?>

    <?= $form->field($model, 'posted',['wrapperOptions'=>['class'=>'col-sm-3']])->widget(
		dosamigos\datetimepicker\DateTimePicker::className(), [
		   'language' => 'ru',
		   'clientOptions' => [
				'autoclose' => true,
				'format' => 'yyyy-mm-dd HH:ii',
				'todayBtn' => true,
		   ]
	]); ?>

    <?= app\modules\admin\widgets\jqUploadWidget::widget(['form'=>$form, 'model'=>$model, 'attribute'=>'images']); ?>

    <?= $form->field($model, 'published')->checkbox() ?>

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>">К списку</a>
    </div>

	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?= Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true],
				['label'=>'SEO', 'content'=>  MetaTags::widget(['model' => $model,'form' => $form])],
			]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end(); ?>