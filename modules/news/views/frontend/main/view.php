<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['breadcrumbs']=[
	['label'=>'Новости', 'url'=>['index']],
	$model->name,
];
?>
<div class="news-view">

    <h1><?= Html::encode($model->name) ?></h1>
	<img src="/images/news/images/middle/<?=  \app\helpers\SiteHelper::returnOneImages($model->images);?>" class="img-responsive">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'preview:ntext',
            'text:ntext',
            'posted',
            'created',
            'modified',
        ],
    ]) ?>

</div>
