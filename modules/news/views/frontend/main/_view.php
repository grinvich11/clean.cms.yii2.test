<?php
use yii\helpers\Html;
use app\helpers\SiteHelper;
?>

<div class="col-md-4 col-sm-6">
	<div class="panel panel-default text-center">
		<div class="panel-heading">
			<img src="/images/news/images/middle/<?=SiteHelper::returnOneImages($model->images);?>" alt="" class="img-responsive">
		</div>
		<div class="panel-body">
			<h4><?php echo Html::a(Html::encode($model->name), $model->getUrl()); ?></h4>
			<p><?=$model->preview;?></p>
			<a href="<?=$model->getUrl();?>" class="btn btn-primary">Learn More</a>
		</div>
	</div>
</div>