<?php
use yii\helpers\Html;
use app\components\ListView;
?>

<h1>Новости</h1>
<br />

<div class="row">
<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    ]);
?>
</div>