<?
use yii\helpers\Html;
?>
<h3>Последние новости</h3>
<div id="news" class="module">
	<div class="inner">
		<? foreach ($items as $model): ?>
		<table class="item">
			<tr>
				<td class="img"><span class="data"><?=Yii::$app->formatter->asDatetime($model->posted, "dd.MM.yy");?></span></td>
				<td>
					<h3><a href="<?=$model->getUrl();?>"><?=Html::encode($model->name);?></a></h3>
					<? if($model->preview): ?>
						<p><?=$model->preview;?></p>
					<? endif; ?>
				</td>
			</tr>
		</table>
		<? endforeach; ?>
	</div>
</div>