<?php
namespace app\modules\news\widgets;

use yii\base\Widget;
use app\modules\news\models\News;

class newsWidget extends Widget
{
    public function run()
    {
        $news=News::find()->where(['published'=>1])->orderBy(['posted'=>SORT_DESC])->limit(3)->all();

		if($news)
			return $this->render('news', ['items'=>$news]);
    }
}