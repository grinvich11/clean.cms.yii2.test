<?php

namespace app\modules\news\models;

use Yii;
use yii\helpers\Url;
use app\models\MainModel;
use app\helpers\SiteHelper;
use yii\helpers\ArrayHelper;
use app\modules\seo\components\MetaTagBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $name
 * @property string $preview
 * @property string $text
 * @property string $posted
 * @property string $images
 * @property integer $published
 * @property string $created
 * @property string $modified
 */
class News extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'preview', 'text', 'posted'], 'required'],
            [['preview', 'text'], 'string'],
            [['posted', 'created', 'modified'], 'safe'],
            [['published'], 'integer'],
            [['name', 'images'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок',
            'preview' => 'Превью',
            'text' => 'Контент',
            'posted' => 'Дата создания',
            'images' => 'Изображение',
            'published' => 'Опубликована',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'MetaTag' => [
					'class' => MetaTagBehavior::className(),
				],
			]
		);
	}

	public function getUrl(){
		return Url::to(['/news/frontend/main/view', 'id' => $this->id, 'alias'=>SiteHelper::str2url($this->name)]);
	}
}
