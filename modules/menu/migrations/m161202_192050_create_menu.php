<?php

use yii\db\Migration;

class m161202_192050_create_menu extends Migration
{
    public $tableName='{{menu}}';

    public function up()
    {
		$this->createTable($this->tableName, [
            'id' => $this->bigPrimaryKey(),
			'alias' => 'varchar(15) DEFAULT NULL',
			'language_id' => 'varchar(7) NOT NULL DEFAULT "ru"',
            'root' => $this->integer(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),
            'name' => $this->string(60)->notNull(),
			'url' => $this->string(255),
            'icon' => $this->string(255),
            'icon_type' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'active' => $this->boolean()->notNull()->defaultValue(true),
            'selected' => $this->boolean()->notNull()->defaultValue(false),
            'disabled' => $this->boolean()->notNull()->defaultValue(false),
            'readonly' => $this->boolean()->notNull()->defaultValue(false),
            'visible' => $this->boolean()->notNull()->defaultValue(true),
            'collapsed' => $this->boolean()->notNull()->defaultValue(false),
            'movable_u' => $this->boolean()->notNull()->defaultValue(true),
            'movable_d' => $this->boolean()->notNull()->defaultValue(true),
            'movable_l' => $this->boolean()->notNull()->defaultValue(true),
            'movable_r' => $this->boolean()->notNull()->defaultValue(true),
            'removable' => $this->boolean()->notNull()->defaultValue(true),
            'removable_all' => $this->boolean()->notNull()->defaultValue(false),
			'module' => 'varchar(255) NULL COMMENT "Модуль"',
			'sql_count' => 'varchar(50) NULL COMMENT "Процедура вызова подсчета кол-ва"',
			'sql_count_new' => 'varchar(50) NULL COMMENT "Процедура вызова подсчета кол-ва новых"',
			'activate_when_module' => 'tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Активировать пункт, когда модуль активен?"',
			'need_highlight_items' => 'tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Необходимо ли выделять активные пункты?"',
			'by_module' => 'varchar(255) NULL',
			'created' => 'DATETIME DEFAULT NULL',
            'modified' => 'DATETIME DEFAULT NULL',
			'INDEX index_language_id (language_id)',
			'INDEX index_alias (alias)',
			'CONSTRAINT menu_ibfk_1 FOREIGN KEY (language_id) REFERENCES {{language}} (locale) ON DELETE CASCADE ON UPDATE CASCADE',
        ]);
        $this->createIndex('tree_NK1', $this->tableName, 'root');
        $this->createIndex('tree_NK2', $this->tableName, 'lft');
        $this->createIndex('tree_NK3', $this->tableName, 'rgt');
        $this->createIndex('tree_NK4', $this->tableName, 'lvl');
        $this->createIndex('tree_NK5', $this->tableName, 'active');

		$this->execute("INSERT INTO `menu` (`id`, `alias`, `language_id`, `root`, `lft`, `rgt`, `lvl`, `name`, `url`, `icon`, `icon_type`, `active`, `selected`, `disabled`, `readonly`, `visible`, `collapsed`, `movable_u`, `movable_d`, `movable_l`, `movable_r`, `removable`, `removable_all`, `module`, `sql_count`, `sql_count_new`, `activate_when_module`, `need_highlight_items`, `by_module`, `created`, `modified`) VALUES ('1', 'admin', 'ru', '1', '1', '2', '0', 'Admin menu', NULL, '', '1', '1', '0', '0', '0', '1', '0', '1', '1', '1', '1', '1', '0', NULL, NULL, NULL, '0', '1', NULL, NULL, NULL);");
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
