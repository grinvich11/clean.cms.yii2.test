<?php
namespace app\modules\menu\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

class Menu extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

	/**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['url', 'module', 'by_module', 'sql_count', 'sql_count_new'], 'string'];
        $rules[] = [['language_id'], 'string', 'max' => 7];
        $rules[] = [['alias'], 'string', 'max' => 15];
		$rules[] = [['alias'], 'required', 'when' => function($model) {
			return $model->lvl === 0;
		}];
		$rules[] = [['activate_when_module', 'need_highlight_items'], 'integer'];
        return $rules;
    }

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'url' => 'Ссылка',
            'language_id' => 'Язык',
			'module' => 'Модуль',
			'sql_count' => 'Процедура вызова подсчета кол-ва',
			'sql_count_new' => 'Процедура вызова подсчета кол-ва новых',
			'activate_when_module' => 'Активировать пункт, когда модуль активен?',
			'need_highlight_items' => 'Необходимо ли выделять активные пункты?',
			'by_module' => 'Скрытое поле, для того чтобы можно было удалить пункты модуля',
        ];
    }

	/**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'url' => 'Ссылка, для перехода в меню',
            'language_id' => 'Для какого языка меню',
            'alias' => 'Метка, по которой меню можно вызвать. Пример, для сайта с несколькими языками. Делаем метку header, указываем ее для нескольких меню, с разными языками. При переключении языка, подтягивается нужное меню.',
			'activate_when_module' => 'Подсветить пункт меню, если сейчас находимся в нужном модуле',
			'need_highlight_items' => 'Если нет необходимости выделять активные пункты меню, можем кэшировать все меню, не перебирая пункты',
			'sql_count' => 'Хранимая процедура в mysql',
			'sql_count_new' => 'Хранимая процедура в mysql',
        ];
    }

	public static function createTree($category, $need_highlight=false, $left = 0, $right = null) {
		if(!$left){
			$left=1;
			$first=true;
		}
		$tree = array();

		foreach ($category as $cat => $range) {
			if ($range->lft == $left + 1 && (is_null($right) || $range->rgt < $right)) {
				$item=[
					'label'=>$range->name,
					'url'=> \yii\helpers\Url::to([$range->url]),
					'active'=>$need_highlight ? (
						$range->activate_when_module ?
						Yii::$app->controller->module->id==$range->module :
						Yii::$app->request->url===$range->url
					) : FALSE
				];
				if($range->rgt-$range->lft>1){
					$item['items'] = self::createTree($category, $need_highlight, $range->lft, $range->rgt);
				}


				if($first){
					if(isset($item['items']))
						$item['dropDownOptions']=['class'=>'dropdown-menu multi-level'];
					array_push($tree, $item);
				}else{
					if(isset($item['items']))
						$item['dropDownOptions']=['class'=>'dropdown-menu'];
					array_push($tree, $item);
				}

				$left = $range->rgt;
			}
		}
		return $tree;
	}

	public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				[
					'class' => TimestampBehavior::className(),
					'createdAtAttribute' => $this->hasAttribute('created') ? 'created' : null,
					'updatedAtAttribute' => $this->hasAttribute('modified') ? 'modified' : null,
					'value' => new Expression('NOW()'),
				],
			]
		);
	}

	public function afterSave($insert, $changedAttributes)
    {
		$this->updateCache();

		return parent::afterSave($insert, $changedAttributes);
	}

	public function afterDelete()
    {
        $this->updateCache();

        parent::afterDelete();
    }

	private function updateCache(){
		//@TODO: Пока так, потом надо переделать чтобы только меню сбрасывались
		if(Yii::$app->cache)
			Yii::$app->cache->flush();
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getRootp()
    {
        return $this->hasOne(self::className(), ['id' => 'root']);
    }
}