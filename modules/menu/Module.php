<?php

namespace app\modules\menu;

use Yii;
use app\components\Module as ParentModule;

class Module extends ParentModule
{
	public $version='1';
	public $name='Меню';
	public $delete=false;

    public function getMenuItems(){
        return [
			[
				'label' => 'Меню',
				'icon' => 'fa fa-object-group',
				'url' => '/menu/backend/main',
				'active' => Yii::$app->controller->module->id=='menu',
			]
        ];
    }

}
