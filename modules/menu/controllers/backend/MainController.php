<?php

namespace app\modules\menu\controllers\backend;

use Yii;
use app\modules\admin\components\CAdminController;

class MainController extends CAdminController
{
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->breadcrumbs=[
			'Меню',
		];

		$this->body_class='sidebar-mini skin-blue sidebar-collapse';

		return $this->render('index', []);
	}
}
