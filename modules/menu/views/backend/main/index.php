<?php
use kartik\tree\TreeView;
use app\modules\menu\models\Menu;
use app\modules\admin\components\widgets\PjaxGrid;

PjaxGrid::begin();

	echo TreeView::widget([
		// single query fetch to render the tree
		// use the Product model you have in the previous step
		'query' => Menu::find()->addOrderBy('root, lft'),
		'headingOptions' => ['label' => ''],
		'fontAwesome' => true,     // optional
		'isAdmin' => false,         // optional (toggle to enable admin mode)
		'displayValue' => 1,        // initial display value
		'softDelete' => true,       // defaults to true
		//'showCheckbox' => true,       // defaults to true
		'cacheSettings' => [
			'enableCache' => true   // defaults to true
		]
	]);

PjaxGrid::end();
