<?
use yii\helpers\ArrayHelper;
use app\modules\language\models\Language;

if($node->lvl!==0 && $_POST['parentKey']<>'root'){
	$root=$node->rootp;
	echo $form->field($node, 'url')->textInput($inputOpts);
	if($root->need_highlight_items){
		echo $form->field($node, 'module')->textInput($inputOpts);
		echo $form->field($node, 'activate_when_module')->checkbox();
	}
	if($root->alias=='admin'){
		echo $form->field($node, 'sql_count')->textInput($inputOpts);
		echo $form->field($node, 'sql_count_new')->textInput($inputOpts);
	}
}else{
	echo $form->field($node, 'language_id')->dropDownList(ArrayHelper::map(Language::find()->all(), 'locale', 'locale'));
	echo $form->field($node, 'alias')->textInput($inputOpts);
	echo $form->field($node, 'need_highlight_items')->checkbox();
}