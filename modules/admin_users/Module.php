<?php
namespace app\modules\admin_users;

use Yii;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Администраторы';
	public $model='AdminUsers';
	public $delete=false;

	public $install=[
		'images'=>[
			'images'=>[
				'admin'=>[
					'width'=>50,
					'heigth'=>50,
					'method'=>'crop'
				],
				'small'=>[
					'width'=>150,
					'heigth'=>150,
					'method'=>'auto'
				]
			]
		]
	];

	public function getMenuItems(){
		if(Yii::$app->user->identity->getIsAdmin())
			return [
				[
					'label' => 'Администраторы',
					'icon' => 'fa fa-users',
					'url' => '/admin/admin_users',
					'active' => Yii::$app->controller->module->id=='admin_users'
				]
			];
	}
}
