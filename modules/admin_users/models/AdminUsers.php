<?php
namespace app\modules\admin_users\models;

use Yii;
use app\models\MainModel;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admin_users".
 *
 * @property integer $id
 * @property string $email
 * @property string $login
 * @property string $name
 * @property string $password
 * @property string $hash
 * @property string $images
 * @property integer $admin
 * @property integer $rememberFilters
 * @property integer $admin_pagination
 * @property string $lastlogin
 * @property string $created
 * @property string $modified
 */
class AdminUsers extends MainModel implements IdentityInterface
{
    public $username;

	public $image;
	public $repeat_password;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'admin_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			//['login', 'required'],
			[['email'], 'email'],
			[['email', 'login'], 'unique'],
			[['email','password'], 'required','on'=>'login'],
			[['password', 'repeat_password'], 'required', 'on'=>'passchange','message'=>'Необходимо заполнить поле'],
			[['password', 'repeat_password'], 'string', 'min'=>5, 'on'=>'passchange', 'tooShort'=>'Минимальная длина пароля 5 символов'],
			[['email'], 'unique', 'on'=>'registration'],
			[['email', 'name'], 'string', 'max'=>255],
			[['email','password', 'repeat_password'], 'required', 'on'=>'registration'],
			[['email'], 'unique', 'on'=>'registration'],
			[['email'], 'required', 'on'=>'profile'],
			[['admin', 'rememberFilters', 'admin_pagination'], 'integer'],
			[['lastlogin','images','auth_key'], 'safe'],
		];
	}

	public function scenarios()
	{
		$scenarios = parent::scenarios();
        $scenarios['passchange'] = ['password','repeat_password'];
        $scenarios['registration'] = ['email', 'password', 'login', 'name', 'admin', 'rememberFilters', 'admin_pagination', 'images'];
        $scenarios['profile'] = ['email', 'login', 'name', 'admin', 'rememberFilters', 'admin_pagination', 'images'];
        return $scenarios;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'email' => 'Email',
			'login' => 'Логин',
			'name' => 'Имя',
			'images' => 'Фото',
			'password' => $this->getScenario()=='passchange' ? 'Текущий пароль' : 'Пароль',
			'repeat_password' => $this->getScenario()=='passchange' ? 'Новый пароль' : 'Повторите пароль',
			'lastlogin' => 'Посл. вход',
			'rememberFilters' => 'Запоминать выборки в списках?',
            'admin_pagination' => 'Кол-во записей на странице в админ панели',
			'admin' => 'Главный админ',
			'created' => 'Created',
			'modified' => 'Modified',
		];
	}

	/**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		$user=static::findOne($id);
        return $user;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new \yii\base\NotSupportedException('findIdentityByAccessToken is not implemented.');
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }*/

        return null;
    }

	/**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

	public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

	public function hashPassword($password) {
		//return md5(md5($password).'Lkd4@kd0');
		return md5($password);
	}

	public function validatePassword($password) {
		return $this->hashPassword($password)===$this->password;
	}

	/**
     * Finds user by login or email
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByLoginOrEmail($username)
    {
		return static::find()->where('email=:username OR login=:username', [':username'=>$username])->one();
    }

	/**
     * @return bool Whether the user is an admin or not.
     */
    public function getIsAdmin()
    {
        return (bool)Yii::$app->session->get('admin');
    }

	public function afterSave($insert, $changedAttributes)
    {
		if(Yii::$app->user->getid()===$this->id){
			Yii::$app->session->set('name', $this->name);
			Yii::$app->session->set('email', $this->email);
			Yii::$app->session->set('login', $this->login);
			Yii::$app->session->set('images', $this->images);
			Yii::$app->session->set('rememberFilters', $this->rememberFilters);
			Yii::$app->session->set('admin_pagination', $this->admin_pagination);
		}

		return parent::afterSave($insert, $changedAttributes);
	}

	public function afterLogin($event) {
		Yii::$app->session->set('name', $this->name);
		Yii::$app->session->set('email', $this->email);
		Yii::$app->session->set('login', $this->login);
		Yii::$app->session->set('images', $this->images);
		Yii::$app->session->set('rememberFilters', $this->rememberFilters);
		Yii::$app->session->set('admin_pagination', $this->admin_pagination);

		if($this->admin)
			Yii::$app->session->set('admin', 1);

		$this->lastlogin=new \yii\db\Expression('NOW()');
		$this->update();
    }

	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert))
		{
			if($insert){
				$this->generateAuthKey();
			}

			return true;
		}
		else
			return false;
	}
}