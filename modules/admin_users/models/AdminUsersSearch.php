<?php

namespace app\modules\admin_users\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin_users\models\AdminUsers;

/**
 * AdminUsersSearch represents the model behind the search form about `AdminUsers`.
 */
class AdminUsersSearch extends AdminUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'admin'], 'integer'],
            [['email', 'login', 'name', 'password', 'hash', 'images', 'lastlogin', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminUsers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['class' => 'app\modules\admin\components\Pagination'],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'admin' => $this->admin,
            'lastlogin' => $this->lastlogin,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'images', $this->images]);

        return $dataProvider;
    }
}
