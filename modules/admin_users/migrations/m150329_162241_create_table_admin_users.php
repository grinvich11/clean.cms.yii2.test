<?php

class m150329_162241_create_table_admin_users extends yii\db\Migration
{
	public $tableName='{{admin_users}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'email' => 'VARCHAR(255) DEFAULT NULL COMMENT "Email"',
				'login' => 'VARCHAR(255) NOT NULL COMMENT "Логин"',
				'name' => 'VARCHAR(255) NOT NULL COMMENT "Имя"',
				'auth_key' => 'VARCHAR(32) NULL',
				'password' => 'VARCHAR(32) NOT NULL COMMENT "Пароль"',
				'hash' => 'VARCHAR(32) NOT NULL',
				'images' => 'TEXT DEFAULT NULL COMMENT "Фото"',
				'admin' => 'TINYINT(1) DEFAULT "1" COMMENT "Админ"',
				'rememberFilters' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "0" COMMENT "Запоминать выборки в списках?"',
				'admin_pagination' => 'SMALLINT(5) UNSIGNED NOT NULL DEFAULT "10" COMMENT "Кол-во записей на странице в админ панели"',

				'lastlogin' => 'DATETIME DEFAULT NULL COMMENT "Посл. вход"',
				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}