<?php
namespace app\modules\admin_users\controllers\backend;

use Yii;
use yii\filters\AccessControl;
use app\modules\admin\components\CrudAdminModuleController;

class MainController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\admin_users\models\AdminUsers';
	}

	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
						'actions' => ['passchange'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
						'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->getIsAdmin();
                        }
                    ]
                ]
            ]
        ];
    }

	public function actionPasschange()
	{
		$model=$this->findModel(Yii::$app->user->getid());

		$model->scenario='passchange';
        if (isset($_POST['AdminUsers'])) {
            $model->repeat_password = $_POST['AdminUsers']['repeat_password'];
            if ($model->validatePassword($_POST['AdminUsers']['password']) && $model->validate()){
				$model->password=$model->hashPassword($_POST['AdminUsers']['repeat_password']);
				if($model->save()){
					Yii::$app->session->setFlash('success','Ваш пароль был успешно изменен');
					return $this->redirect('passchange');
				}
            }else
                $model->addError('password', 'Не правильный пароль');
        }
        $model->password='';
        $model->repeat_password='';
        return $this->render('passchange', ['model' => $model]);
	}

	public function actionCreate()
	{
		$model=new $this->model;

		if($model->load(Yii::$app->request->post()))
		{
			$model->scenario='passchange';
			$model->repeat_password = $_POST['AdminUsers']['repeat_password'];
			$model->password=$model->hashPassword($_POST['AdminUsers']['password']);
			if(!empty($_POST['AdminUsers']['password']) && $_POST['AdminUsers']['password']==$_POST['AdminUsers']['repeat_password'] && $model->validate()){
				if ($model->save()){
					//$model->saveRights();
				}
			} else
				$model->addError('password', 'Пароли не совпадают');

			if(!$model->hasErrors() && $model->save()){
				//$model->saveRights();
				if(Yii::$app->getRequest()->getIsPjax()){
					header('X-PJAX-URL: /'.$this->module->id.'/backend/main/index');
					return Yii::$app->runAction($this->module->id.'/backend/main/index');
				}else{
					return $this->redirect(['index']);
				}
			}
		}else{
			$model->password='';
		}

		$this->breadcrumbs=[
			['label'=>$this->module->name, 'url'=>['index']],
			'Добавить',
		];

		return $this->render('_form',[
			'model'=>$model,
		]);
	}

	public function actionUpdate($id)
	{
		$model=$this->findModel($id);
		//$model->scenario='update';

		if($model->load(Yii::$app->request->post()))
		{
			$password=$model->password;
			$model->password=$_POST['AdminUsers']['password'];

			if(empty($model->password)){
				$model->password=$password;
			}else{
				$model->scenario='passchange';

				$model->repeat_password = $_POST['AdminUsers']['repeat_password'];
				if($_POST['AdminUsers']['password']==$_POST['AdminUsers']['repeat_password'] && $model->validate()){
					$model->password=$model->hashPassword($_POST['AdminUsers']['repeat_password']);
					if ($model->save()){
						//$model->saveRights();
						Yii::$app->session->setFlash('success','Пароль пользователя "'.$model->email.($model->name ? ' - '.$model->name : '').'" был успешно изменен');
					}
				} else{
					$model->addError('password', 'Пароли не совпадают');
				}
			}


			if($model->validate() && $model->save()){
				//$model->saveRights();
				if(Yii::$app->getRequest()->getIsPjax()){
					header('X-PJAX-URL: /'.$this->module->id.'/backend/main/index');
					return Yii::$app->runAction($this->module->id.'/backend/main/index');
				}else{
					return $this->redirect(['index']);
				}
			}
		}else{
			$model->password='';
		}

		$this->breadcrumbs=[
			['label'=>$this->module->name, 'url'=>['index']],
			$model->name,
			'Редактирование',
		];

		return $this->render('_form',[
			'model'=>$model,
		]);
	}
}
