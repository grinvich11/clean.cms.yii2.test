<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
	],
]); ?>
	<? $this->beginBlock('base'); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->field($model,'login'); ?>
	<?php echo $form->field($model,'email'); ?>
	<?php echo $form->field($model,'name'); ?>

	<?php echo app\modules\admin\widgets\jqUploadWidget::widget(['form'=>$form, 'model'=>$model, 'attribute'=>'images', 'many'=>true, 'path'=>'images/small/']); ?>

	<div class="box-footer">
		<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/admin/<?=Yii::$app->controller->module->id;?>">К списку</a>
	</div>
	<? $this->endBlock(); ?>


	<? $this->beginBlock('rights'); ?>
		<?php echo $form->field($model, 'admin')->checkBox();?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/admin/<?=Yii::$app->controller->module->id;?>">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<? $this->beginBlock('admin'); ?>
		<?php echo $form->field($model, 'rememberFilters')->checkBox();?>
		<?php echo $form->field($model, 'admin_pagination'); ?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/admin/<?=Yii::$app->controller->module->id;?>">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<? $this->beginBlock('password'); ?>
		<?php echo $form->field($model,'password', [
			'template' => '{label} <div class="col-sm-4">{input}{error}{hint}</div>',
			'inputOptions' => ['class'=>'form-control', 'maxlength'=>40, 'autocomplete'=>'off']
		])->passwordInput(); ?>

		<?php echo $form->field($model,'repeat_password', [
			'template' => '{label} <div class="col-sm-4">{input}{error}{hint}</div>',
			'inputOptions' => ['class'=>'form-control', 'maxlength'=>40, 'autocomplete'=>'off']
		])->passwordInput(); ?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/admin/<?=Yii::$app->controller->module->id;?>">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo yii\bootstrap\Tabs::widget([
			'items'=>array(
				array('label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true),
				array('label'=>'Права', 'content'=>$this->blocks['rights']),
				array('label'=>'Настройки админки', 'content'=>$this->blocks['admin']),
				array('label'=>'Пароль', 'content'=>$this->blocks['password']),
			)
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>