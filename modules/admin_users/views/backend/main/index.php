 <?php
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\grid\ActionColumn;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;
use app\modules\admin\components\widgets\ImageColumn;

PjaxGrid::begin();

	echo GridView::widget([
		'dataProvider'=>$dataProvider,
		'filterModel' =>$searchModel,
		'actionsAllow'=>false,
		'columns'=>[
			[
				'class' => ImageColumn::className(),
				'attribute' => 'images',
			],
			'login',
			'email',
			[
				'attribute'=>'name',
				'format'=>'raw',
				'value'=>function ($model) {
					return Html::a(Html::encode($model->name), ["update", "id"=>$model->id], ['data-pjax'=>0]);
				},
				'contentOptions'=>['style'=>'width:340px;']
			],
			'lastlogin',
			[
				'class' => ActionColumn::className(),
				'header' => Html::a('<i class="glyphicon glyphicon-remove"></i>', ['index'], ['class'=>'btn btn-mini btn-danger']),
				'template' => '{update} {delete}',
				'contentOptions'=>['style'=>'width: 60px;text-align:center;']
			],
		]
	]);

PjaxGrid::end();
