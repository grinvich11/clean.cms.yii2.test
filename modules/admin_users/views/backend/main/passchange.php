<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;

Yii::$app->controller->breadcrumbs=[
	'Смена пароля'
];
?>

<div class="row">
	<!-- left column -->
	<div class="col-md-12">
	  <!-- general form elements -->
	  <div class="box box-primary">
		<div class="box-header">
		  <h3 class="box-title"></h3>
		</div><!-- /.box-header -->
		<? $form = ActiveForm::begin([
			'id'=>'user-form',
			'layout' => 'horizontal',
			'enableClientValidation'=>true,
			'validateOnSubmit'=>true,
			'options'=>[
				'autocomplete'=>'off'
			]
		])
	  /*
	  $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
			'id'=>'user-form',
			'enableClientValidation'=>true,
			'enableAjaxValidation'=>false,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
			'htmlOptions'=>array(
				'autocomplete'=>'off'
			),
			'type'=>'horizontal',
		)); */?>

<?  ?>
			<div class="box-body">
				<?php echo $form->field($model,'password', [
					'template' => '{label} <div class="col-sm-4">{input}{error}{hint}</div>',
					'inputOptions' => ['class'=>'form-control', 'maxlength'=>40]
				])->passwordInput(); ?>

				<?php echo $form->field($model,'repeat_password', [
					'template' => '{label} <div class="col-sm-4">{input}{error}{hint}</div>',
					'inputOptions' => ['class'=>'form-control', 'maxlength'=>40]
				])->passwordInput(); ?>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			</div>
		<?php ActiveForm::end() ?>

		</div><!-- /.box -->
	</div><!--/.col (left) -->
</div>   <!-- /.row -->