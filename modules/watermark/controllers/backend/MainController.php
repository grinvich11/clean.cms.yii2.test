<?php

namespace app\modules\watermark\controllers\backend;

use app\modules\admin\components\CrudAdminModuleController;

class MainController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\watermark\models\Watermark';
	}
}
