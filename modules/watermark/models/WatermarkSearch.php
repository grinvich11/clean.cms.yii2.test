<?php

namespace app\modules\watermark\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\watermark\models\Watermark;

/**
 * WatermarkSearch represents the model behind the search form about `Watermark`.
 */
class WatermarkSearch extends Watermark
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'size_id', 'angle', 'opacity', 'font_size', 'type'], 'integer'],
            [['image', 'text', 'color', 'font', 'alignment_1', 'alignment_2', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Watermark::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => ['class' => 'app\modules\admin\components\Pagination'],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'size_id' => $this->size_id,
            'angle' => $this->angle,
            'opacity' => $this->opacity,
            'font_size' => $this->font_size,
            'type' => $this->type,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'font', $this->font])
            ->andFilterWhere(['like', 'alignment_1', $this->alignment_1])
            ->andFilterWhere(['like', 'alignment_2', $this->alignment_2]);

        return $dataProvider;
    }
}
