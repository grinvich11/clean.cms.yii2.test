<?php

namespace app\modules\watermark\models;

use Yii;
use app\models\MainModel;
use app\helpers\AdminHelper;
use app\modules\admin\models\AdminImagesSizes;

/**
 * This is the model class for table "admin_watermark".
 *
 * @property integer $id
 * @property integer $size_id
 * @property string $image
 * @property string $text
 * @property string $color
 * @property integer $angle
 * @property integer $opacity
 * @property string $font
 * @property integer $font_size
 * @property string $alignment_1
 * @property string $alignment_2
 * @property integer $type
 * @property integer $x
 * @property integer $y
 * @property string $created
 * @property string $modified
 *
 * @property AdminImagesSizes $size
 */
class Watermark extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_watermark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size_id'], 'required'],
            [['size_id', 'angle', 'opacity', 'font_size', 'type', 'x', 'y'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['image', 'text'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 7],
            [['font'], 'string', 'max' => 100],
            [['alignment_1', 'alignment_2'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'size_id' => 'Размер',
            'image' => 'Изображение',
            'text' => 'Текст',
            'color' => 'Цвет',
            'angle' => 'Угол',
            'opacity' => 'Прозрачность',
            'font' => 'Шрифт',
            'font_size' => 'Размер шрифта',
            'alignment_1' => 'Позиция 1',
            'alignment_2' => 'Позиция 2',
            'type' => 'Тип',
            'x' => 'X',
            'y' => 'Y',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'x' => 'Смещение по X',
            'y' => 'Смещение по Y',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSize()
    {
        return $this->hasOne(AdminImagesSizes::className(), ['id' => 'size_id']);
    }

	public function afterSave($insert, $changedAttributes)
    {
		if($this->type=='1')
			\app\modules\watermark\helpers\WatermarkHelper::getTextImage($this);
		else
			\app\modules\watermark\helpers\WatermarkHelper::getImage($this);

		$path=$_SERVER['DOCUMENT_ROOT'].'/images/'.Yii::$app->getModule($this->size->modules)->path_images.'/'.$this->size->field.'/'.$this->size->size.'/';
		file_put_contents($path.'.htaccess', "RewriteEngine on\nRewriteCond %{HTTP:if-modified-since} .\nRewriteRule ^(.*)\.(gif|jpeg|jpg|png)$ watermark.php?image=$1.$2&ext=$2 [L]");
		copy(Yii::getAlias('@app').'/modules/watermark/helpers/watermark.php', $path.'watermark.php');

		//Yii::$app->session->setFlash('success', 'Watermark применен');

		return parent::afterSave($insert, $changedAttributes);
	}

	public function afterDelete()
    {
		set_time_limit(0);

		$path=$_SERVER['DOCUMENT_ROOT'].'/images/'.Yii::$app->getModule($this->size->modules)->path_images.'/'.$this->size->field.'/'.$this->size->size.'/';

		if(is_file($path.'.htaccess')) unlink($path.'.htaccess');
		if(is_file($path.'watermark.php')) unlink($path.'watermark.php');
		if(is_file($path.'watermark.png')) unlink($path.'watermark.png');

		//Yii::$app->session->setFlash('success', 'Watermark удален');

		parent::afterDelete();
	}
}
