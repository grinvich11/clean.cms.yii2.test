<?php
use app\helpers\ImageHelper;
use yii\image\drivers\Image;
?>
<div class="box">
	<div class="box-body">
		<form>
			<div id="alignmentbox">
				<?
				if(!isset($_GET['watermark']['alignment'])){
					$_GET['watermark']['alignment']='center center';
				}
				foreach (ImageHelper::$watermark_position as $key => $value) {
					++$key;
					echo '<label id="alignment-'.$key.'-label" for="alignment-'.$key.'"><input type="radio" id="alignment-'.$key.'" value="'.$value.'" name="watermark[alignment]" style="visibility: hidden;"'.($_GET['watermark']['alignment']==$value ? ' checked' : '').'></label>';
				}
				?>
			</div>
			<input type="text" name="watermark[text]" value="<?=$_GET['watermark']['text'];?>">
			<br>
			<?php
			echo \kartik\color\ColorInput::widget([
				'name' => 'watermark[color]',
				'value' => $_GET['watermark']['color'],
				'pluginOptions'=> [
					'allowEmpty' => FALSE
				]
			]);
			?>
			<label>Шрифт
				<select name="watermark[font]">
					<option value="arial" <? if($_GET['watermark']['font']=='arial') echo ' selected'; ?>>Arial</option>
					<option value="OpenSans-Light" <? if($_GET['watermark']['font']=='OpenSans-Light') echo ' selected'; ?>>OpenSans-Light</option>
				</select>
			</label>
			<br>
			<label>Размер шрифта <input type="number" name="watermark[font_size]" value="<?=$_GET['watermark']['font_size'];?>" min="1" max="40"></label>
			<br>
			<label>Прозрачность <input type="number" name="watermark[opacity]" value="<?=$_GET['watermark']['opacity'];?>" min="0" max="100"></label>
			<br>
			<label>Угол <input type="number" name="watermark[angle]" value="<?=$_GET['watermark']['angle'];?>" min="-360" max="360"></label>
			<br>
			<input type="submit">
		</form>

		<br><br><br>
		<?
		$font_size=$_GET['watermark']['font_size'];

		$font=\Yii::getAlias('@webroot').'/css/admin/fonts/watermark/'.$_GET['watermark']['font'].'.ttf';
		$box = @imageTTFBbox($font_size,$_GET['watermark']['angle'],$font,$_GET['watermark']['text']);
		$width = abs($box[4] - $box[0]).' ';
		$height = abs($box[5] - $box[1]);

		$im = imagecreatetruecolor($width , $height);
		imagealphablending($im, false);
		imagesavealpha($im, true);
		imagefill($im, 0, 0, imagecolorallocatealpha($im, 0, 0, 0, 127));

		$color=ImageHelper::HexToRGB($_GET['watermark']['color']);
		$opacity=127/100*$_GET['watermark']['opacity'];
		$text_color = imagecolorallocatealpha($im, $color['r'], $color['g'], $color['b'], $opacity);
		//imagestring($im, 12, 5, 5,  $_GET['watermark']['text'], $text_color);
		imagepng($im, 'tmp/watermark_test.png');
		imagedestroy($im);

		ImageHelper::applyText('tmp/watermark_test.png', $_GET['watermark']['text'], $_GET['watermark']['angle'], $text_color, $opacity, $font_size, $font);


		$watermark = Image::factory('tmp/watermark_test.png');
		$file=dirname($_SERVER['SCRIPT_FILENAME']).'/tmp/landscape.png';
		$image = Yii::$app->image->load($file);
		$image=ImageHelper::setPosition($_GET['watermark']['alignment'], $image, $watermark);
		$image->save(dirname($_SERVER['SCRIPT_FILENAME']).'/tmp/watermarked.jpeg');
		?>
		<img src="/tmp/watermarked.jpeg"><br><br>
		<img src="/tmp/watermark_test.png" style="border: 1px solid black"><br>
	</div>
</div>