<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'settingsButton' => false,
        'columns' => [
            [
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => 'id',
			],
			[
				'attribute' => 'size_id',
				'format' => 'raw',
				'value' => function($model){
					$size=$model->size;
					if($size){
						return $size->modules.'/'.$size->field.'/'.$size->size;
					}
				},
				'filter' => false
			],
			[
				'attribute' => 'text',
				'format' => 'raw',
				'value' => function($model){
					return '<span style="color:'.$model->color.';">'.$model->text.'</span>';
				},
			],
            // 'angle',
            // 'opacity',
            // 'font',
            // 'font_size',
            // 'alignment_1',
            // 'alignment_2',
            // 'created',
            /*[
				'class' => '\app\modules\admin\components\widgets\ImageColumn',
				'attribute' => 'image',
			],*/
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
			],
        ],
    ]);

PjaxGrid::end();
