<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use app\helpers\ImageHelper;

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
	],
]); ?>
	<? $this->beginBlock('base'); ?>

    <?= $form->field($model, 'size_id', ['options'=>['class'=>'form-group']])->dropDownList([''=>'']+\yii\helpers\ArrayHelper::map(\app\modules\admin\models\AdminImagesSizes::find()->select(['id', 'CONCAT(modules, "/", field, "/", size) as size', 'modules'])->andWhere('modules NOT IN("admin_users", "watermark")')->orderBy(['size'=>SORT_ASC])->all(), 'id', 'size', 'modules')) ?>

	<?= $form->field($model, 'type')->radioList(['0'=>'Картинка', '1'=>'Текст']) ?>

	<fieldset>
		<legend>Настройки изображения</legend>

		<?= app\modules\admin\widgets\jqUploadWidget::widget(['form'=>$form, 'model'=>$model, 'attribute'=>'image']); ?>
	</fieldset>

	<fieldset>
		<legend>Настройки текста</legend>

		<?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'color')->widget(\kartik\color\ColorInput::classname(), [
			'options' => ['placeholder' => 'Select color ...'],
		]); ?>

		<?= $form->field($model, 'angle')->textInput() ?>

		<?= $form->field($model, 'opacity')->textInput() ?>

		<?= $form->field($model, 'font', ['options'=>['class'=>'form-group']])->dropDownList(app\modules\watermark\helpers\WatermarkHelper::getFonts()) ?>

		<?= $form->field($model, 'font_size')->textInput() ?>

		<div class="form-group">
			<label class="control-label col-sm-3">Положение на картинке</label>
			<div class="col-sm-6">
				<div id="alignmentbox">
					<?
					if(!isset($_GET['watermark']['alignment'])){
						$_GET['watermark']['alignment']='center center';
					}
					foreach (ImageHelper::$watermark_position as $key => $value) {
						++$key;
						echo '<label id="alignment-'.$key.'-label" for="alignment-'.$key.'"><input type="radio" id="alignment-'.$key.'" value="'.$value.'" name="watermark[alignment]" style="visibility: hidden;"'.($model->alignment_1.' '.$model->alignment_2==$value ? ' checked' : '').'></label>';
					}
					?>
				</div>
			</div>
		</div>

		<?= $form->field($model, 'x')->textInput() ?>
		<?= $form->field($model, 'y')->textInput() ?>

		<?= $form->field($model, 'alignment_1', ['template'=>'{input}'])->hiddenInput()?>

		<?= $form->field($model, 'alignment_2', ['template'=>'{input}'])->hiddenInput()?>
	</fieldset>


    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/<?=Yii::$app->controller->module->id;?>/<?=Yii::$app->controller->id;?>">К списку</a>
    </div>

	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?= Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true],
							]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end(); ?>
