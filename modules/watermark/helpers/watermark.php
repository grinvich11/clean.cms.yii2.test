<?php
function getPos($pos=NULL){
	if($pos=='TRUE') return TRUE;
	if($pos=='FALSE') return FALSE;
	if($pos=='NULL') return NULL;
	if($pos=='0') return 0;
	return $pos;
}

function getOffset($offset_x, $offset_y, $image_width, $image_height, $watermark_width, $watermark_height, $watermark){

	$resize_watermark=false;

	if($watermark_height>$image_height){
		$watermark_height=$image_height;
		$resize_watermark=true;
	}
	if($watermark_width>$image_width){
		$watermark_width=$image_width;
		$resize_watermark=true;
	}

	if($resize_watermark){
		$watermark->scaleimage($watermark_width, $watermark_height);
	}

	if ($offset_x === NULL)
	{
			// Center the X offset
			$offset_x = round(($image_width - $watermark_width) / 2);
	}
	elseif ($offset_x === TRUE)
	{
			// Bottom the X offset
			$offset_x = $image_width - $watermark_width;
	}
	elseif ($offset_x < 0)
	{
			// Set the X offset from the right
			$offset_x = $image_width - $watermark_width + $offset_x;
	}

	if ($offset_y === NULL)
	{
			// Center the Y offset
			$offset_y = round(($image_height - $watermark_height) / 2);
	}
	elseif ($offset_y === TRUE)
	{
			// Bottom the Y offset
			$offset_y = $image_height - $watermark_height;
	}
	elseif ($offset_y < 0)
	{
			// Set the Y offset from the bottom
			$offset_y = $image_height - $watermark_height + $offset_y;
	}

	return ['offset_x'=>$offset_x, 'offset_y'=>$offset_y, 'watermark'=>$watermark];
}

$watermark_file=dirname($_SERVER['SCRIPT_FILENAME']).'/watermark.png';
$image_file=dirname($_SERVER['SCRIPT_FILENAME']).'/'.$_GET['image'];

/*if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
	if(max([filectime($image_file), filectime($watermark_file)])===strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
		header($_SERVER['SERVER_PROTOCOL'].' 304 Not Modified');
		return true;
	}
}*/


$image_file = $_GET['image'];
$ext=strtolower($_GET['ext']);

$types=['jpeg','jpg','png','gif'];
if (in_array($ext,$types)){
	if(!is_file($image_file) || !$image_size=getimagesize($image_file)){
		header("HTTP/1.1 404 Not Found");
		header('Content-type: text/html; charset=UTF-8');
		die('Запрашиваемая страница не существует.');
	}

	if(is_file($watermark_file) && ($watermark_size=getimagesize($watermark_file)) && $_GET['image']<>'no_image.png'){
		if (is_array($image_size) && count($image_size)) {
			header('content-type: image/'.$ext);

			// Convert the Image intance into an Imagick instance
			$watermark = new Imagick;
			$watermark->readimage(dirname($_SERVER['SCRIPT_FILENAME']).'/watermark.png');

			if ($watermark->getImageAlphaChannel() !== Imagick::ALPHACHANNEL_ACTIVATE)
			{
					// Force the image to have an alpha channel
					$watermark->setImageAlphaChannel(Imagick::ALPHACHANNEL_OPAQUE);
			}

			$im = new Imagick;
			$im->readImage($image_file);

			extract(
				getOffset(
					getPos($watermark->getImageProperty('x')),
					getPos($watermark->getImageProperty('y')),
					$image_size[0],
					$image_size[1],
					$watermark_size[0],
					$watermark_size[1],
					$watermark
				)
			);

			// Apply the watermark to the image
			$im->compositeImage($watermark, Imagick::COMPOSITE_DISSOLVE, $offset_x, $offset_y);

			header('Last-modified: ' . gmdate('D, d M Y H:i:s', max([filectime($image_file), filectime($watermark_file)])) . ' GMT');

			echo $im;
		}else{
			header("HTTP/1.1 404 Not Found");
			header('Content-type: text/html; charset=UTF-8');
			die('Запрашиваемая страница не существует.');
		}
	}else{
		header('content-type: image/'.$ext);

		$im = new Imagick;
		$im->readImage($image_file);
		echo $im;
	}
}
else  {
	header("HTTP/1.1 404 Not Found");
	header('Content-type: text/html; charset=UTF-8');
	die('Запрашиваемая страница не существует.');
}