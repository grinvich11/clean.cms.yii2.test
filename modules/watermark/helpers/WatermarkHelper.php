<?php
namespace app\modules\watermark\helpers;

use Yii;
use yii\image\drivers\Image;
use app\helpers\ImageHelper;

class WatermarkHelper {

	public static function getFonts(){
		return [
			'arial'=>'Arial',
			'OpenSans-Light'=>'OpenSans-Light',
		];
	}

	public static function getTextImage($watermark){
		$font=Yii::getAlias('@webroot').'/css/admin/fonts/watermark/'.$watermark->font.'.ttf';
		$rect = imagettfbbox($watermark->font_size, $watermark->angle, $font, $watermark->text);
		/*$width = abs($box[4] - $box[0])+$watermark->x;
		$height = abs($box[5] - $box[1])+$watermark->y;*/

		/*$rect=$box;*/
		$minX = min(array($rect[0],$rect[2],$rect[4],$rect[6]));
		$maxX = max(array($rect[0],$rect[2],$rect[4],$rect[6]));
		$minY = min(array($rect[1],$rect[3],$rect[5],$rect[7]));
		$maxY = max(array($rect[1],$rect[3],$rect[5],$rect[7]));

		$width  = $maxX - $minX+$watermark->x;
		$height = $maxY - $minY+$watermark->y;

		$im = imagecreatetruecolor($width, $height);
		imagealphablending($im, false);
		imagesavealpha($im, true);
		imagefill($im, 0, 0, imagecolorallocatealpha($im, 0, 0, 0, 127));

		$color=ImageHelper::HexToRGB($watermark->color);
		$opacity=127/100*$watermark->opacity;
		$text_color = imagecolorallocatealpha($im, $color['r'], $color['g'], $color['b'], $opacity);

		$watermark_text='images/'.Yii::$app->getModule($watermark->size->modules)->path_images.'/'.$watermark->size->field.'/'.$watermark->size->size.'/watermark.png';

		imagepng($im, $watermark_text);
		imagedestroy($im);

		switch ($watermark->alignment_1.' '.$watermark->alignment_2) {
			case 'right center':
				$watermark->x=0;
				$watermark->y    = abs($minY) - 1 + $watermark->y;
				break;
			case 'left top':
				$watermark->y    = abs($minY) - 1 + $watermark->y;
				break;
			case 'right top':
				$watermark->x=0;
				$watermark->y    = abs($minY) - 1 + $watermark->y;
				break;
			case 'left center':
				$watermark->y    = abs($minY) - 1 + $watermark->y;
				break;
			case 'left bottom':
				$watermark->y    = abs($minY) - 1 + $watermark->y;
				break;
			case 'center top':
			case 'right bottom':

			case 'center bottom':
			case 'center center':
			default:
				//$watermark->y=$height;

				$watermark->x   = abs($minX) - 1;
				$watermark->y    = abs($minY) - 1;
				break;
		}

		ImageHelper::applyText($watermark_text, $watermark->text, $watermark->angle, $text_color, $opacity, $watermark->font_size, $font, $watermark->x, $watermark->y, $watermark);

		return Image::factory($watermark_text);
	}

	public static function getImage($watermark){
		$sOrigImg='images/watermark/image/'.$watermark->image;
		$im = new \Imagick;
		$im->readImage($sOrigImg);
		$im->setImageFormat('png');
		$offset=ImageHelper::getImageOffset($watermark->alignment_1.' '.$watermark->alignment_2);
		$im->setImageProperty('x', $offset[0]);
		$im->setImageProperty('y', $offset[1]);
		file_put_contents('images/'.Yii::$app->getModule($watermark->size->modules)->path_images.'/'.$watermark->size->field.'/'.$watermark->size->size.'/watermark.png', $im);
	}
}

