<?php

namespace app\modules\watermark;

use Yii;
use app\components\Module as ParentModule;

class Module extends ParentModule
{
	public $version='1';
	public $name='Watermark';

	public $install=[
		'images'=>[
			'image'=>[
				'admin'=>[
					'width'=>88,
					'heigth'=>88,
					'method'=>'auto'
				],
			]
		]
	];

    /*public function getMenuItems(){
        return [
			[
				'label' => 'Watermark',
				'icon' => 'a fa-trademark',
				'url' => '/watermark/backend/main',
				'active' => Yii::$app->controller->module->id=='watermark',
			]
        ];
    }*/
}
