<?php

use yii\db\Migration;

class m160216_085626_create_table_admin_watermark extends Migration
{
    public $tableName='{{admin_watermark}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			[
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'size_id' => 'INT UNSIGNED NOT NULL COMMENT "Размер"',

				'image' => 'VARCHAR(255) DEFAULT NULL COMMENT "Изображение"',

				'text' => 'VARCHAR(255) NULL COMMENT "Текст"',
				'color' => 'VARCHAR(7) NULL DEFAULT "#000000" COMMENT "Цвет"',
				'angle' => 'INT DEFAULT "0" COMMENT "Угол"',
				'opacity' => 'SMALLINT UNSIGNED DEFAULT "0" COMMENT "Прозрачность"',
				'font' => 'VARCHAR(100) NULL COMMENT "Шрифт"',
				'font_size' => 'TINYINT UNSIGNED NULL DEFAULT 15 COMMENT "Размер шрифта"',
				'alignment_1' => 'VARCHAR(15) NULL DEFAULT "center" COMMENT "Позиция 1"',
				'alignment_2' => 'VARCHAR(15) NULL DEFAULT "center" COMMENT "Позиция 2"',
				'x' => 'INT NOT NULL DEFAULT "0" COMMENT "X"',
				'y' => 'INT NOT NULL DEFAULT "0" COMMENT "Y"',

				'type' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT "Тип"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',

				'CONSTRAINT `watermark_size` FOREIGN KEY (size_id) REFERENCES {{admin_images_sizes}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}
