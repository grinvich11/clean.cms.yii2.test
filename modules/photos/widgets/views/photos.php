<!-- Photo carousel -->
<div class="om-carousel">
    <div class="owl-carousel">
		<? foreach ($items as $item): ?>
        <img src="/images/photos/image/big/<?=\app\helpers\SiteHelper::returnOneImages($item['image']);?>" alt="Омега. Все, что нужно для экзаменов"/>
		<? endforeach; ?>
    </div>
</div>