<?php
namespace app\modules\photos\widgets;

use yii\base\Widget;

class photosWidget extends Widget
{
    public function run()
    {
		$sql='SELECT id,image FROM {{photos}} WHERE published=1 ORDER BY position ASC';
		$items=\Yii::$app->db->createCommand($sql)->queryAll();

		if($items)
        return $this->render('photos', ['items'=>$items]);
    }
}