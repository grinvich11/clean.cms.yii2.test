<?php

namespace app\modules\photos\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property string $image
 * @property integer $region_id
 * @property integer $position
 * @property integer $published
 * @property string $created
 * @property string $modified
 */
class Photos extends \app\models\MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'position', 'published'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Изображение',
            'region_id' => 'Регион',
            'position' => 'Позиция',
            'published' => 'Опубликована',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->isNewRecord)
				$this->position=(int)Yii::$app->db->createCommand("SELECT MAX(`position`)+1 FROM ".self::tableName())->queryScalar();
			return true;
		}else{
			return false;
		}
	}
}
