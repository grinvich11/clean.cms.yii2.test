<?php

namespace app\modules\photos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\photos\models\Photos;

/**
 * PhotosSearch represents the model behind the search form about `app\modules\photos\models\Photos`.
 */
class PhotosSearch extends Photos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'position', 'published'], 'integer'],
            [['image', 'created', 'modified'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Photos::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination'=>false,
			'sort'=> ['defaultOrder' => ['position'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'region_id' => $this->region_id,
            'position' => $this->position,
            'published' => $this->published,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
