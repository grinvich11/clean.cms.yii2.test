<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;
?>
<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>array(
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
	),
]); ?>


	<? $this->beginBlock('base'); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo app\modules\admin\widgets\jqUploadWidget::widget(['form'=>$form, 'model'=>$model, 'attribute'=>'image']); ?>


	<?php echo $form->field($model, 'published')->checkBox(); ?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/admin/<?=\Yii::$app->controller->module->id;?>">К списку</a>
		</div>
	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo yii\bootstrap\Tabs::widget([
			'items'=>array(
				array('label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true),
			)
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>