<?
$module_id=\Yii::$app->controller->module->id;
$script = <<< JS
$(function(){
	$("#grid tbody").sortable({
		placeholder: "ui-state-highlight",
		scrollSpeed: 0,
		cursor: "move",
		delay: 0,
		cancel: "button",
		handle: ".glyphicon.glyphicon-move",
		stop : function(){
			rebuildItems();
		}
	});

});

function rebuildItems(){
	var s=[];
	$(".sortableItem").each(function(indx, element){
		s.push($(element).attr("href"));
	});
	$.ajax({
		url:"/admin/{$module_id}/setsortvalue",
		type:"GET",
		data: {
			ids: s.join(",")
		}
	});
}
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

<?php \yii\widgets\Pjax::begin(['id'=>'pajax_grid']); ?>
<?= app\modules\admin\components\widgets\GridView::widget(array(
	'dataProvider'=>$dataProvider,
	'filterModel' =>$searchModel,
	'columns'=>array(
		array('attribute'=>'image','format'=>'raw','value'=>function ($model) {
			return "<a href=/admin/photos/update?id=".$model->id." data-pjax=0><img style=max-width:50px; src=/images/photos/image/admin/".\app\helpers\SiteHelper::returnOneImages($model->image)."?m=".$model->modified." /></a>";
		},'contentOptions'=>array('style'=>'max-width:70px;width:70px;')),
		[
			'class' => '\app\modules\admin\components\widgets\ToggleColumn',
			'attribute' => 'published',
		],
		[
            'class' => 'yii\grid\ActionColumn',
			'header' => \yii\helpers\Html::a('<i class="glyphicon glyphicon-remove"></i>', ['index'], ['class'=>'btn btn-mini btn-danger']),
			'template' => '{move} {update} {delete}',
			'buttons'=>[
				'move' => function ($url, $model) {
					return \yii\helpers\Html::a('<i class="glyphicon glyphicon-move"></i>', $model->id, ['title'=>'Переместить','class'=>'sortableItem','onclick'=>'return false;']);
				},
			],
            'contentOptions'=>array('style'=>'width: 90px;text-align:center;')
        ],
	)
));
?>
<?php \yii\widgets\Pjax::end(); ?>