<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_072436_create_table_photos extends Migration
{
    public $tableName='{{photos}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'image' => 'VARCHAR(255) DEFAULT NULL COMMENT "Изображение"',

				'region_id' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT "Регион"',
				
				'position' => 'INT UNSIGNED NOT NULL DEFAULT "0" COMMENT "Позиция"',
				'published' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT "Опубликована"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}
