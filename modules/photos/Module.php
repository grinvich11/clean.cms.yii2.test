<?php

namespace app\modules\photos;

class Module extends \app\components\Module {

    public $version = '1';
    public $name = 'Фотогалерея';
    public $model = 'Photos';
    public $install = array(
        'images' => array(
            'image' => array(
                'admin' => array(
                    'width' => 100,
                    'heigth' => 100,
                    'method' => 'auto'
                ),
                'big' => array(
                    'width' => 272,
                    'heigth' => 272,
                    'method' => 'height'
                )
            )
        )
    );

    /*public function getMenuItems() {

        return array(
            array('label' => 'Фотогалерея', 'icon' => 'fa fa-picture-o', 'url' => '/admin/photos', 'active' => \Yii::$app->controller->module->id == 'photos'),
        );
    }*/
}
