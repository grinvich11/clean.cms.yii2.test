<?php

class m150327_121231_create_table_feedback extends yii\db\Migration
{
	public $tableName='{{feedback}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'name' => 'VARCHAR(255) NOT NULL COMMENT "Имя"',
				'phone' => 'VARCHAR(255) NOT NULL COMMENT "Телефон"',

				'type' => 'TINYINT(1) DEFAULT "0" COMMENT "Тип"',
				'processed' => 'TINYINT(1) DEFAULT "0" COMMENT "Обработана"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);

		$this->createIndex('processed_NK1', $this->tableName, 'processed');

		$this->execute('CREATE PROCEDURE `feedback_count`(OUT `count` int)
			NOT DETERMINISTIC
			  SQL SECURITY INVOKER
			  COMMENT ""
		  BEGIN
			  SELECT COUNT(id) as count FROM feedback;
		  END;');

		$this->execute('CREATE PROCEDURE `feedback_count_new`(OUT `count_new` int)
			NOT DETERMINISTIC
			  SQL SECURITY INVOKER
			  COMMENT ""
		  BEGIN
			  SELECT COUNT(id) as count_new FROM feedback WHERE processed=0;
		  END;');
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
		$this->execute('DROP PROCEDURE IF EXISTS `feedback_count`');
		$this->execute('DROP PROCEDURE IF EXISTS `feedback_count_new`');
	}
}