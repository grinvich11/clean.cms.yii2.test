<?php
namespace app\modules\feedback\controllers\frontend;

use Yii;
use app\modules\feedback\models\Feedback;
use app\components\FrontController;

class MainController extends FrontController
{
	public function actionCreate()
	{
		$model=new Feedback();

		$model->name=$_POST['Feedback']['name'];
		$model->phone=$_POST['Feedback']['phone'];
		$model->type=$_POST['Feedback']['type'];

		if($model->save()){
			$emails=Yii::$app->db->createCommand("SELECT email FROM {{admin_users}}")->queryColumn();
			if(isset($model::$type_values[$model->type]))
				$content.='Новая заявка с формы "'.$model::$type_values[$model->type].'"<br>';
			$content.='Имя: '.$model->name.'<br>';
			$content.='Телефон: '.$model->phone.'<br>';

			Yii::$app->mailer->compose('@app/modules/feedback/views/email/feedback',
			[
				'content'=>$content
			])
			->setFrom(Yii::$app->params['noreplyEmail'])
			->setTo($emails)
			->setSubject(isset($model::$type_values[$model->type]) ? $model::$type_values[$model->type] : '')
			->send();


			return '1';
		}else{
			return '0';
		}
	}
}