<?php

namespace app\modules\feedback\controllers\backend;

use app\modules\admin\components\CrudAdminModuleController;

class MainController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\feedback\models\Feedback';
	}
}
