<?php
namespace app\modules\feedback;

use Yii;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Обратная связь';
	public $model='Feedback';

	public function getMenuItems(){
		return [
			[
				'label' => 'Обратная связь',
				'icon' => 'fa fa-envelope-o',
				'url' => '/admin/feedback',
				'activate_when_module' => 1,
				'sql_count'=> 'feedback_count',
				'sql_count_new'=> 'feedback_count_new'
				//'count'=> Yii::$app->db->createCommand("SELECT COUNT(id) FROM {{feedback}}")->queryScalar(),
				//'count_new'=> Yii::$app->db->createCommand("SELECT COUNT(id) FROM {{feedback}} WHERE processed=0")->queryScalar()
			]
        ];
	}

	public static function rules()
    {
        return [
            'feedback/create' => 'feedback/frontend/main/create',
        ];
    }
}
