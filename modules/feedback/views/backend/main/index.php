<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use app\modules\admin\components\widgets\GridView;

PjaxGrid::begin();

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'createButton' => false,
        'columns' => [
			[
				'class' => 'yii\grid\CheckboxColumn',
			],
            [
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => 'id',
			],
            'name',
            'phone',
            'created',
            [
			'attribute' => 'type',
				'value' => function($model){
					return app\modules\feedback\models\Feedback::getTypeValues()[$model->type];
				},
				'filter' => app\modules\feedback\models\Feedback::getTypeValues()
			],
            [
				'class' => '\app\modules\admin\components\widgets\ToggleColumn',
				'attribute' => 'processed',
			],
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
				'template' => '{delete}',
			],
        ],
    ]);

PjaxGrid::end();
