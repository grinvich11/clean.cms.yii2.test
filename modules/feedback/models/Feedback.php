<?php

namespace app\modules\feedback\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $type
 * @property integer $processed
 * @property string $created
 * @property string $modified
 */
class Feedback extends \app\models\MainModel
{
	public static $type_values=[
		'0'=>'Перезвоните мне',
	];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['type', 'processed'], 'integer'],
            [['created', 'modified'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'type' => 'Тип',
            'processed' => 'Обработана',
            'created' => 'Оставлена',
            'modified' => 'Modified',
        ];
    }

	public static function getTypeValues(){
		$values=self::$type_values;
		return $values;
	}
}
