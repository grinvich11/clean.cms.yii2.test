<?php
namespace app\modules\feedback\widgets;

use yii\base\Widget;

class feedbackWidget extends Widget
{
	public $type=0;
	public $value;

    public function run()
    {
        $model = new \app\modules\feedback\models\Feedback();

        return $this->render('feedback_'.$this->type, ['model'=>$model, 'type'=>$this->type, 'value'=>$this->value]);
    }
}