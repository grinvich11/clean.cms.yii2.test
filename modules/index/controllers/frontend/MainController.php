<?php
namespace app\modules\index\controllers\frontend;

use Yii;
use app\components\FrontController;
use yii\web\NotFoundHttpException;

class MainController extends FrontController
{
	public function actionIndex()
	{
		\app\helpers\SeoHelper::registerSeoPage('index_page');

        return $this->render('index');
	}
}