<?php
namespace app\modules\index;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Главная страница';

	public static function rules()
    {
        return [
            ''=>'index/frontend/main/index',
        ];
    }
}
