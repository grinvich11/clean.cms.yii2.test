<? //echo app\modules\slider\widgets\sliderWidget::widget(); ?>
<? //echo app\modules\photos\widgets\photosWidget::widget(); ?>

<section class="om-press">
    <div class="container">
        <div class="row">
            <div class="col-xs-36 text-center">

                <h2 class="om-press__title">Убедили?</h2>
                <p class="om-press__click">Жмите на кнопку!</p>
                <a class="om-btn om-btn--cta" href="#cta-form">Оставить заявку</a>

            </div>
        </div>
    </div>
</section>

<? //echo app\modules\responses\widgets\responsesWidget::widget(); ?>

<section class="om-form">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <h2 class=om-form__title></h2>

                <? echo app\modules\feedback\widgets\feedbackWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>

<? echo app\modules\contacts\widgets\contactsWidget::widget(); ?>