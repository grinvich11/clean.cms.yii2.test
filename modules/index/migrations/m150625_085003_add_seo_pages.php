<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_085003_add_seo_pages extends Migration
{
    public $tableName='{{seo_pages}}';
	public $moduleName='index';

	public function safeUp()
	{
		$this->insert(
			$this->tableName,
			[
				'module' => $this->moduleName,
				'alias' => $this->moduleName.'_page',
				'name' => 'Главная страница'
			]
		);
	}

	public function safeDown()
	{
		$this->delete($this->tableName, 'module=:module', [':module'=>$this->moduleName]);
	}
}
