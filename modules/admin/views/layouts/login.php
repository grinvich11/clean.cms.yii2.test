<?
use app\modules\admin\components\AdminAsset;
use app\modules\admin\components\AdminLteAsset;

AdminAsset::register($this);
AdminLteAsset::register($this);

$baseUrl = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
	<?
	//$assets = Yii::getAlias('@app/vendor/almasaeed2010/adminlte');

	/*use app\assets\AppAsset;
	AppAsset::register($assets);*/

	//$baseUrl = Yii::$app->getAssetManager()->publish($assets);



	/*$cs = Yii::$app->getClientScript();
	//$cs->registerScriptFile('/js/full.js',CClientScript::POS_END);
	$cs->coreScriptPosition=CClientScript::POS_END;
	$cs->defaultScriptPosition=CClientScript::POS_END;
	$cs->defaultScriptFilePosition=CClientScript::POS_END;
	Yii::$app->clientScript->scriptMap['jquery.min.js'] = false;
	Yii::$app->clientScript->scriptMap['jquery.js'] = false;*/
	 ?>
    <title><? echo $this->title; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=$baseUrl;?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=$baseUrl;?>/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?=$baseUrl;?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<?php $this->head() ?>
  </head>
  <body class="login-page">
	  <?php $this->beginBody() ?>

	<?=$content;?>
	<!-- jQuery 2.2.3 -->
    <script src="<?=$baseUrl;?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 JS -->
    <script src="<?=$baseUrl;?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?=$baseUrl;?>/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
		$('div.checkbox').addClass('icheck');
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
	<?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>