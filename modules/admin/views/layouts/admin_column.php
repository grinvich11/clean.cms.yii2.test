<?php
use app\modules\admin\components\widgets\PjaxAll as Pjax;

$this->beginContent('@app/modules/admin/views/layouts/admin.php');

if(is_array(Yii::$app->controller->breadcrumbs)){
	foreach (array_reverse(Yii::$app->controller->breadcrumbs) as $key => $value) {
		$title[]=(is_string($value) ? $value :  $value['label']);
	}
}else{
	if(is_array($this->title))
		foreach ($this->title as $key => $value) {
			$title[]=(is_string($key) ? $key :  $value);
		}
}
if(Yii::$app->params['admin_pjax_reload']){
	Pjax::begin(['options'=>['id'=>'pjax_layout'], 'timeout'=>0, 'scrollTo'=>0
		//	, 'clientOptions'=>['fragment'=>'#pajax-application']
	]);
}
?>
<div id="pajax-application" data-title="<?=\yii\helpers\Html::encode($title ? \yii\helpers\Html::encode(implode(' - ',$title)) : $this->title.' '.Yii::$app->params['siteName']);?>">

<? if(!Yii::$app->user->isGuest): ?>
<aside class="main-sidebar">
	<section class="sidebar">
		<?php
		$menu=app\helpers\AdminHelper::returnMenuArray();

		if(\Yii::$app->user->identity->getIsAdmin())
		$settings=array('label' => 'Настройки сайта', 'icon' => 'fa fa-wrench', 'url' => '/admin/config/all',
		'items'=>array(
			array('label' => 'Модули', 'icon' => 'fa fa-bars', 'url' => '/admin/modules', 'active' => Yii::$app->controller->id=='modules'),
			//array('label' => 'Меню', 'icon' => 'fa fa-bars', 'url' => '/admin/menu', 'active' => Yii::$app->controller->id=='menu'),
			[
				'label' => 'Watermark',
				'icon' => 'fa fa-trademark',
				'url' => '/watermark/backend/main',
				'active' => Yii::$app->controller->module->id=='watermark',
			],
			array('label' => 'Параметры', 'icon' => 'fa fa-wrench', 'url' => '/admin/config/all', 'active' => Yii::$app->controller->id=='config' && Yii::$app->controller->action->id=='all'),
			array('label' => 'Seo других страниц', 'icon' => 'fa fa-file-code-o', 'url' => '/seo/backend/seopages', 'active' => Yii::$app->controller->module->id=='seo'),
			//array('label' => 'Добавить параметр', 'icon' => 'glyphicon glyphicon-plus-sign', 'url' => '/admin/config/create', 'active' => Yii::$app->controller->id=='config' && Yii::$app->controller->action->id=='create', 'linkOptions' => array('class' => 'sub')),
			//array('label' => 'Бекапы бд сайта', 'icon' => 'fa fa-hdd-o', 'url' => '/admin/database', 'active' => Yii::$app->controller->id=='database' && Yii::$app->controller->action->id=='index'), //https://github.com/spanjeta/yii2-backup
		));

		$controller=Yii::$app->controller;

		echo \app\modules\admin\components\AdminMenu::widget([
		'options'=>['class'=>'sidebar-menu'],
		//'encodeLabel'=>false,
		'activateParents'=>true,
		'activateItems'=>true,
		'items' => array_merge([
			[
				'label' => 'Главная',
				'icon' => 'fa fa-dashboard',
				'url' => '/admin',
				'active' => $controller->id=='admin'
			],
		],array_merge($menu,
			[
				$settings,
		])
	)]); ?>
	</section>
</aside>
<? endif; ?>
<div class="content-wrapper">
	<section class="content-header">
	  <h1>
		<?
		if(is_array(Yii::$app->controller->breadcrumbs))
			echo end(Yii::$app->controller->breadcrumbs); ?>
		<small></small>
	  </h1>
	<?php echo yii\widgets\Breadcrumbs::widget([
			'homeLink' =>[
				'label'=>'<i class="fa fa-dashboard"></i> Home',
				'url'=>'/admin'
			],
			'links' => Yii::$app->controller->breadcrumbs,
			'tag' => 'ol',
			'encodeLabels' => false
	  ]); ?>
	</section>

	<section class="content">
	<?php
		$flashMessages = Yii::$app->session->getAllFlashes();
		if ($flashMessages) {
			echo '<div class="flash-messages">';
			foreach ($flashMessages as $key => $message) {
				echo '<div class="alert alert-' . $key . '">' . "
<button class=close aria-hidden=true data-dismiss=alert type=button>×</button>
".($key=='success' ? '<i class="icon fa fa-check"></i>' : '')."{$message}
\n";
			}
			echo '</div></div>';
		}
		?>
		<div class="row">
			<div class="col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</section>
</div>
</div>
<?
if(Yii::$app->params['admin_pjax_reload']){
	Pjax::end();
}
$this->endContent();
?>