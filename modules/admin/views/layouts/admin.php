<?
use app\modules\admin\components\AdminAsset;
use app\modules\admin\components\AdminLteAsset;
use yii\helpers\Html;
use app\helpers\SiteHelper;

AdminAsset::register($this);
AdminLteAsset::register($this);

$baseUrl = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<title><?php
		if(is_array(Yii::$app->controller->breadcrumbs)){
			foreach (array_reverse(Yii::$app->controller->breadcrumbs) as $key => $value) {
				$title[]=(is_string($value) ? $value :  $value['label']);
			}
		}else{
			if(is_array($this->title))
				foreach ($this->title as $key => $value) {
					$title[]=(is_string($key) ? $key :  $value);
				}
		}
		echo $title ? Html::encode(implode(' - ',$title)) : $this->title.' '.Yii::$app->params['siteName'];
	?></title>

	 <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	 <?
/*
	$cs = Yii::$app->getClientScript();
	//$cs->registerScriptFile('/js/full.js',CClientScript::POS_END);
	$cs->coreScriptPosition=CClientScript::POS_END;
	$cs->defaultScriptPosition=CClientScript::POS_END;
	$cs->defaultScriptFilePosition=CClientScript::POS_END;
*/
	/*Yii::$app->assetManager->assetMap['jquery.min.js'] = false;
	Yii::$app->assetManager->assetMap['jquery.js'] = false;
	Yii::$app->assetManager->assetMap['bootstrap.min.js'] = false;
	Yii::$app->assetManager->assetMap['bootstrap.js'] = false;
	Yii::$app->assetManager->assetMap['jquery-ui.min.js'] = false;*/
	 ?>
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="<?=$baseUrl;?>/bootstrap/css/bootstrap.min.css">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
	<link rel="stylesheet" href="<?=$baseUrl;?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?=$baseUrl;?>/dist/css/skins/_all-skins.min.css">


    <!-- iCheck -->
    <link href="<?=$baseUrl;?>/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="<?=$baseUrl;?>/plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="<?=$baseUrl;?>/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<!-- DATA TABLES -->
    <link href="<?=$baseUrl;?>/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

	<!-- Pace style -->
	<link href="<?=$baseUrl;?>/plugins/pace/pace.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<link rel="stylesheet" href="/css/admin/admin.css" />
	<link rel="stylesheet" href="/css/admin/jquery-ui-bootstrap.css" />
	<link href="/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
	<?php $this->head() ?>
</head>
<body class="<?=Yii::$app->controller->body_class; ?>" data-module="<?=Yii::$app->controller->module->id;?>" data-controller="<?=Yii::$app->controller->id;?>">
<?php $this->beginBody() ?>
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="/" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini">cms</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><?=Html::encode(Yii::$app->params['siteName']);?></span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top" role="navigation">
			  <!-- Sidebar toggle button-->
			  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			  </a>
			  <div class="navbar-custom-menu">
				<? if(!Yii::$app->user->isGuest): ?>
				<ul class="nav navbar-nav">
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="/images/admin_users/images/admin/<?=SiteHelper::returnOneImages(Yii::$app->session->get('images'));?>" class="user-image" alt="User Image"/>
						<span class="hidden-xs"><?
						$name=Yii::$app->session->get('name');
						$login=Yii::$app->session->get('login');
						$email=Yii::$app->session->get('email');
						if($name)
							echo $name;
						else
							echo (!empty($login) ? $login : (!empty($email) ? $email : ''));
						?></span>
					  </a>
					  <ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
						  <img src="/images/admin_users/images/small/<?=SiteHelper::returnOneImages(Yii::$app->session->get('images'));?>" class="img-circle" alt="User Image" />
						  <p>
							 <?
							 if($name){
								 echo $name;
								 echo '<small>'.(!empty($login) ? $login : (!empty($email) ? $email : '')).'</small>';
							 }else{
								 echo (!empty($login) ? $login : (!empty($email) ? $email : ''));
							 }
							 ?>
						  </p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
						  <div class="pull-left">
							<a href="/admin/admin_users/passchange" class="btn btn-default btn-flat">Смена пароля</a>
						  </div>
						  <div class="pull-right">
							<a href="/admin/login/logout" class="btn btn-default btn-flat">Выход</a>
						  </div>
						</li>
					  </ul>
					</li>
					<!-- Control Sidebar Toggle Button -->
					<li>
					  <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
					</li>
				  </ul>
				  <? endif; ?>
				</div>
			</nav>
      </header>

		<?php echo $content; ?>

		<div id="confirmDiv"></div>

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
			  Copyright &copy; 2016
			</div>
			Отработало за <?php echo sprintf('%0.5f', Yii::getLogger()->getElapsedTime()); ?> с. Память: <?php echo round(memory_get_peak_usage() / (1024 * 1024), 2) . "MB"; ?>
		</footer>

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
		  <!-- Create the tabs -->
		  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		  </ul>
		  <!-- Tab panes -->
		  <div class="tab-content">
		  </div>
		</aside>
		<!-- /.control-sidebar -->
		<!-- Add the sidebar's background. This div must be placed
			 immediately after the control sidebar -->
		<div class="control-sidebar-bg"></div>
	</div> <!-- wrapper -->

	<!-- jQuery 2.2.3 -->
    <script src="<?=$baseUrl;?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 JS -->
    <script src="<?=$baseUrl;?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$baseUrl;?>/plugins/jQueryUI/jquery-ui.min.js"></script>
    <!-- FastClick -->
    <script src='<?=$baseUrl;?>/plugins/fastclick/fastclick.min.js'></script>
    <!-- SlimScroll 1.3.0 -->
	<script src="<?=$baseUrl;?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <script src='<?=$baseUrl;?>/plugins/iCheck/icheck.min.js'></script>
	<? if(Yii::$app->params['admin_pjax_reload']){ ?>
    <script data-pace-options='{ "restartOnPushState": false, "restartOnRequestAfter": false, "startOnPageLoad": false }' src='<?=$baseUrl;?>/plugins/pace/pace.min.js'></script>
	<? } ?>
    <!-- AdminLTE App -->
    <script src="<?=$baseUrl;?>/dist/js/app.min.js"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="/js/admin/admin_lte_demo.js"></script>

	<script src="/js/admin/admin.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>