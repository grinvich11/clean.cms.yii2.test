<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use app\modules\admin\components\widgets\ActiveForm;

$this->title=Yii::$app->params['siteName'] . ' - Вход';
/*$this->view->title=array(
	'Вход',
);*/
?>

<div class="login-box">
	<div class="login-logo">
	  <a href="/admin"><b>Clean.</b>cms</a>
	</div><!-- /.login-logo -->
	<div class="login-box-body">
	  <p class="login-box-msg">Войдите</p>
	  <? $form = ActiveForm::begin([
			'id' => 'login-form',
			/*'enableClientValidation'=>true,
			'validateOnSubmit'=>true,*/
		]);
	  ?>
		<?php echo $form->field($model,'username',
			[
				'options'=>['class'=>'form-group has-feedback'],
				'inputOptions' => ['class'=>'form-control', 'placeholder'=>'Логин или email'],
				'template' => "{label}\n<div class='input-group'>{input}<span class='input-group-addon'><span class='glyphicon glyphicon-envelope form-control-feedback'></span>&nbsp;&nbsp;</span></div>\n{hint}\n{error}"
		]); ?>
		<?php echo $form->field($model,'password',
			[
				'options'=>['class'=>'form-group has-feedback'],
				'inputOptions' => ['class'=>'form-control', 'placeholder'=>'Пароль'],
				'template' => "{label}\n<div class='input-group'>{input}<span class='input-group-addon'><span class='glyphicon glyphicon-lock form-control-feedback'></span>&nbsp;&nbsp;</span></div>\n{hint}\n{error}"
			])->passwordInput(); ?>
		<div class="row">
		  <div class="col-xs-8">
			<?php echo $form->field($model,'rememberMe')->checkbox(); ?>
		  </div><!-- /.col -->
		  <div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
		  </div><!-- /.col -->
		</div>
	  <?php ActiveForm::end() ?>

<!--	  <a href="#">I forgot my password</a><br>-->

	</div><!-- /.login-box-body -->
</div><!-- /.login-box -->