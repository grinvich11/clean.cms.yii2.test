<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;
?>
<? $form = ActiveForm::begin([
	'id'=>'form-sizes',
	'validateOnSubmit' => true,
	'enableClientValidation' => true,
	//'action'=>'/admin/modules/create_size',
	//'layout'=>'horizontal',
]); ?>

	<? echo $form->field($model,'field', ['options' => ['class'=>'col-sm-12']]); ?>
	<? echo $form->field($model,'size', ['options' => ['class'=>'col-sm-12']]); ?>
	<? echo $form->field($model, 'modules', ['template'=>'{input}'])->hiddenInput(['value'=>isset($_GET['modules']) ? $_GET['modules'] : $model->modules])?>
	<? echo $form->field($model,'width', ['options' => ['class'=>'col-sm-6', 'min'=>1]])->input('number'); ?>
	<? echo $form->field($model,'heigth', ['options' => ['class'=>'col-sm-6', 'min'=>1]])->input('number'); ?>
	<? echo $form->field($model,'method', ['options' => ['class'=>'col-sm-12']])->dropDownList($model->method_array); ?>
	<div class="clearfix"></div>

	<div class="box-footer">
		<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
	<!--	<a class="btn btn-default btn-link" href="/admin/config">К списку</a>-->
	</div>

<?php ActiveForm::end() ?>