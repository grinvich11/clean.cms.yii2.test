<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="small-box <? if(isset($item['bg'])) echo 'bg-'.$item['bg']; else echo 'bg-aqua'; ?>">
		<div class="inner">
		  <h3><? if(isset($item['count_new'])) echo $item['count_new'].' / '; ?><?=$item['count'];?></h3>
		  <p><?=$item['label'];?></p>
		</div>
		<div class="icon">
		  <i class="<?=$item['icon'];?>"></i>
		</div>
		<a class="small-box-footer" href="<?=$item['url'];?>">
		  Перейти <i class="fa fa-arrow-circle-right"></i>
		</a>
	</div>
</div>