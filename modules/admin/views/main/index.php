<?php
Yii::$app->controller->breadcrumbs=[
	'Панель администратора',
];
?>

<div class="row">
	<?
	$menu=app\helpers\AdminHelper::returnMenuArray();
	/*echo '<pre>';
	print_r($menu);
	echo '</pre>';*/
	if($menu):
		foreach($menu as $item):
			if(isset($item['count'])):
				echo $this->render('_view', ['item' => $item]);
			endif; ?>
		<? endforeach; ?>
	<? endif; ?>
</div>
<?/*
<div class="row">
	<section class="col-lg-7 connectedSortable">
		<div class="box box-primary">
		  <div class="box-header with-border">
			<i class="fa fa-envelope-o"></i>

			<h3 class="box-title">Последние сообщения в обратной связи</h3>

			<div class="box-tools pull-right">
			  <button type="button" class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
			  </button>
<!--			  <button type="button" class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i>
			  </button>-->
			</div>
		  </div>
		  <div class="box-body border-radius-none">
			<?
			$items=  app\modules\feedback\models\Feedback::find()->limit(5)->orderBy(['created'=>SORT_DESC])->all();
			if($items): ?>
			<ul class="products-list product-list-in-box">
				<? foreach ($items as $value): ?>
				<li class="item">
                  <div class="product-info" style="margin-left: 0;">
                    <a class="product-title" href="/admin/feedback?FeedbackSearch[id]=<?=$value->id;?>">
						<?=$value->name;?> (<?=$value->phone;?>)
						<span class="label label-primary pull-right"><?=$value->created;?></span>
					</a>
                  </div>
                </li>
				<? endforeach; ?>
              </ul>
			  <?
			  else:
				  echo 'Список пуст';
			  endif;
			  ?>
		  </div>
		  <!-- /.box-body -->
		  <div class="box-footer text-center">
			<a class="uppercase" href="/admin/feedback">Показать все сообщения</a>
		  </div>
		  <!-- /.box-footer -->
		</div>
	</section>
	<section class="col-lg-5 connectedSortable">

		<div class="box box-success">
		  <div class="box-header with-border">
			<i class="fa fa-newspaper-o"></i>

			<h3 class="box-title">Последние новости</h3>

			<div class="box-tools pull-right">
			  <button type="button" class="btn btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
			  </button>
<!--			  <button type="button" class="btn btn-sm" data-widget="remove"><i class="fa fa-times"></i>
			  </button>-->
			</div>
		  </div>
		  <div class="box-body border-radius-none">
			<?
			$news=app\modules\news\models\News::find()->limit(3)->orderBy(['posted'=>SORT_DESC])->all();
			if($news): ?>
			<ul class="products-list product-list-in-box">
				<? foreach ($news as $value): ?>
				<li class="item">
                  <div class="product-img">
					  <img alt="News Image" src="/images/news/images/admin_widget/<?=app\helpers\SiteHelper::returnOneImages($value->images);?>">
                  </div>
                  <div class="product-info">
                    <a class="product-title" href="/news/backend/main/update?id=<?=$value->id;?>"><?=$value->name;?>
                      <span class="label label-primary pull-right"><?=$value->posted;?></span></a>
                        <span class="product-description">
                          <?=$value->preview;?>
                        </span>
                  </div>
                </li>
				<? endforeach; ?>
              </ul>
			  <?
			  else:
				  echo 'Список пуст';
			  endif;
			  ?>
		  </div>
		  <!-- /.box-body -->
		  <div class="box-footer text-center">
			<a class="uppercase" href="/admin/news">Показать все новости</a>
		  </div>
		  <!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</section>
</div>
 *
 */?>