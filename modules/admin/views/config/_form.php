<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="box box-primary">
<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
]); ?>
	<div class="box-body">
	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->field($model,'label'); ?>
	<?php echo $form->field($model,'param'); ?>

	<?php
	if($model->type=='text')
		echo $form->field($model,'value')->textArea();
	elseif($model->type=='bool')
		echo $form->field($model,'value')->checkbox();
	else/*if($model->type=='bool')
		echo $form->switchGroup($model, 'value',array('enabledLabel'=>'Да','disabledLabel'=>'Нет',));
	elseif($model->type=='html'){
	?>
		<div class="form-group ">
			<label for="Config_value" class="col-sm-3 control-label required">Текст</label>
			<div class="col-sm-9">
			<? $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget',array(
					'model'=>$model,
					'attribute'=>'value',
					'options'=>array(
						'formattingTags'=>array('p','h1', 'h2'),
						'buttons'=>array('html',  'bold'),
						'minHeight'=> 150,
						'lang'=>'ru',
					),
				)); ?>
			<?php echo $form->error($model,'value'); ?>
			</div>
		</div>
<?
	}else*/
		echo $form->field($model,'value'); ?>
	<?php echo $form->field($model,'section'); ?>
	</div>

	<div class="box-footer">
		<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/admin/config">К списку</a>
	</div>

<?php ActiveForm::end() ?>
</div>