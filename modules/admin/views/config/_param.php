<div class="form-group">
	<? if($v->type!=='bool'):?>
	<label for="Config_<?=$v->id;?>" class="col-sm-3 control-label"><?=$v->label;?></label>
	<? endif; ?>
	<div class="<? if($v->type=='int') echo 'col-sm-2'; else echo 'col-sm-9'; if($v->type=='bool') echo ' col-sm-offset-3'; ?>">
		<?php
		if($v->type=='text'){
			echo $form->field($v, 'value', [
				'options' => [
					'class' => ''
				],
				'template' => "{input}\n{error}",
				'inputOptions' => [
					'class'=>'col-sm-5 form-control',
					'rows'=>5,
					'name'=>'Config['.$v->id.']',
					'id'=>'Config_'.$v->id
				]
			])->textArea();
		}elseif($v->type=='bool'){
			echo $form->field($v,'value', [
				'labelOptions'=>[
					'label'=>$v->label
				],
				'horizontalCssClasses'=>[
					'offset'=>''
				],
				'template'=>"{input}\n{error}"
			])->checkbox([
					'name'=>'Config['.$v->id.']',
					'id'=>'Config_'.$v->id
				]);
		}elseif($v->type=='int'){
			echo $form->field($v, 'value', [
				'template'=>"{input}\n{error}",
				'inputOptions' => [
					'name'=>'Config['.$v->id.']',
					'id'=>'Config_'.$v->id
				],
				'options' => [
					'class' => ''
				]
			])->textInput([
				'type'=>'number',
				'min'=>1
			]);
		}elseif($v->type=='email'){
			echo $form->field($v, 'value', [
				'template'=>"{input}\n{error}",
				'inputOptions' => [
					'name'=>'Config['.$v->id.']',
					'id'=>'Config_'.$v->id
				],
				'options' => [
					'class' => ''
				]
			])->textInput([
				'type'=>'email'
			]);
		}else/*
		elseif($v->type=='html'){
		?>

		<? $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget',array(
				'model'=>$v,
				'attribute'=>'value',
				'options'=>array(
					'formattingTags'=>array('p','h1', 'h2'),
					'buttons'=>array('html',  'bold'),
					'minHeight'=> 150,
					'lang'=>'ru',
				),
				'htmlOptions'=>array(
					'name'=>'Config['.$v->id.']'
				)
			)); ?>
	<?
		}else*/{
			echo $form->field($v, 'value', [
				'template'=>"{input}\n{error}",
				'inputOptions' => [
					'name'=>'Config['.$v->id.']',
					'id'=>'Config_'.$v->id
				],
				'options' => [
					'class' => ''
				]
			])->textInput();
		} ?>
		<p class="help-block"><?=$v->help;?></p>
	</div>
</div>