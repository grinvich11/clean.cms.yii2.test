<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\modules\admin\components\widgets\ActiveForm;

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
]);

$this->beginBlock('params');

	if($sections):
		foreach ($sections as $section=>$params):
		echo '<fieldset>';
			echo '<legend>'.$section.'</legend>';
			foreach ($params as $v):
				echo $this->render('_param', ['v'=>$v, 'form'=>$form]);
			endforeach;
		echo '</fieldset>';
		endforeach;
	endif;
	?>

	<div class="box-footer">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>
<? $this->endBlock();

$this->beginBlock('modules');

	if($sections_module):
		foreach ($sections_module as $section=>$params):
		echo '<fieldset>';
			echo '<legend>'.$section.'</legend>';
			foreach ($params as $v):
				echo $this->render('_param', ['v'=>$v, 'form'=>$form]);
			endforeach;
		echo '</fieldset>';
		endforeach;
	endif;
?>


	<div class="box-footer">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>
<? $this->endBlock(); ?>

<div class="box-body">
	<div class="nav-tabs-custom">
	<?
	echo Tabs::widget([
		'items'=>[
			['label'=>'Параметры', 'content'=>$this->blocks['params'], 'active'=>true],
			['label'=>'Параметры модулей', 'content'=>$this->blocks['modules']],
		]
	]);
	?>
	</div>
</div>

<?php ActiveForm::end() ?>