<?php \yii\widgets\Pjax::begin(['id'=>'pajax_grid']); ?>
<?= app\modules\admin\components\widgets\GridView::widget(array(
	'dataProvider'=>$dataProvider,
	'filterModel' =>$searchModel,
	'settingsButton' =>false,
	'columns'=>array(
		array(
			'attribute' => 'section',
			'contentOptions'=>array('style'=>'width:140px;'),
			'filter'=>  yii\helpers\ArrayHelper::map(app\models\Config::find()->select('section')->where('visible=1 AND section<>""')->groupBy('section')->all(), 'section', 'section'),
		),
		array('attribute'=>'label','format'=>'raw','value'=>function ($model) {
			return \yii\helpers\Html::a(\yii\helpers\Html::encode($model->label), array("update", "id"=>$model->getPrimaryKey()),['data-pjax'=>0]);
		},'contentOptions'=>array('style'=>'width:340px;')),
		'value',
		[
            'class' => 'yii\grid\ActionColumn',
			'header' => \yii\helpers\Html::a('<i class="glyphicon glyphicon-remove"></i>', ['index'], ['class'=>'btn btn-mini btn-danger']),
			'template' => '{update} '.(isset($_COOKIE['dev_mode_root']) ? '{delete}' : ''),
            'contentOptions'=>array('style'=>'width: 60px;text-align:center;')
        ],
	)
));
?>
<?php \yii\widgets\Pjax::end(); ?>