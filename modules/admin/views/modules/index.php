<script>
function turnModuleON(id){
	$('#grid').addClass('grid-view-loading');
	 $.ajax({
		url: '/admin/modules/turnmodule',
		type: 'GET',
		data: {
			'action': 'ON',
			'id': id
		},
		success: function(data) {
			$.pjax.reload({container: '#pajax_grid', timeout: 0});
			$(".main-sidebar").load("/admin/modules .main-sidebar > *",function(){
				$.AdminLTE.tree('.sidebar');
			});
		}
	});
}
function turnModuleOFF(id){
	if(confirm('Отключить модуль? Внимание будут удалены все записи, и изображения связанные с модулем!')){
		$('#grid').addClass('grid-view-loading');
		 $.ajax({
			url: '/admin/modules/turnmodule',
			type: 'GET',
			data: {
				'action': 'OFF',
				'id': id
			},
			success: function(data) {
				$.pjax.reload({container: '#pajax_grid', timeout: 0});
				$(".main-sidebar").load("/admin/modules .main-sidebar > *",function(){
					$.AdminLTE.tree('.sidebar');
				});
			}
		});
	}
}
</script>
<?php \yii\widgets\Pjax::begin(['id'=>'pajax_grid', 'timeout'=>0]); ?>
<?= app\modules\admin\components\widgets\GridView::widget(array(
	'dataProvider'=>$dataProvider,
	'filterModel' =>$searchModel,
	'createButton' =>false,
	'settingsButton' =>false,
	'actionsAllow'=>false,
	'columns'=>array(
		array('attribute'=>'name','format'=>'raw','value'=>function ($model) {
			if($model->state=="1")
				return \yii\helpers\Html::a(\yii\helpers\Html::encode($model->name), array("update", "id"=>$model->getPrimaryKey()), ['data-pjax'=>0]);
			else
				return \yii\helpers\Html::encode($model->name);
		}),
		array(
			'attribute' => 'state',
			'format'=>'raw',
			'value'=>function($model){
				return $model->state==1 ? "Установлен" : "Не установлен";
			},
			'contentOptions'=>array('style'=>'width:140px;'),
			'filter'=>\app\helpers\SiteHelper::$yes_no,
		),
		[
            'class' => 'app\modules\admin\components\widgets\GridActionColumn',
			'template' => '{move} {update} {on}{off}',
			'buttons'=>[
				'update' => function ($url, $model) {
					if($model->state=="1")
						return \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/admin/modules/update/'.$model->getPrimaryKey()], ['data-pjax'=>0]);
				},
				'on' => function ($url, $model) {
					if($model->state=="1" && $model->delete=="1")
						return \yii\helpers\Html::a('<i class="glyphicon glyphicon-remove"></i>', $model->getPrimaryKey(), ['title'=>'Отключить', 'onclick'=>'turnModuleOFF($(this).attr("href"));return false;']);
				},
				'off' => function ($url, $model) {
					if($model->state=="0")
						return \yii\helpers\Html::a('<i class="glyphicon glyphicon-ok"></i>', $model->getPrimaryKey(), ['title'=>'Включить', 'onclick'=>'turnModuleON($(this).attr("href"));return false;']);
				},
			],
			'contentOptions'=>['style'=>'width: 90px;text-align:left;']
        ],
	)
));
?>
<?php \yii\widgets\Pjax::end(); ?>