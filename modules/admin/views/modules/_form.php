<?php
use app\modules\admin\components\widgets\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('/js/admin/jquery.ui.dialog.js');
$script = <<< JS
jQuery(function($) {
	$('body').on('click','.images_size',function(){
		if ($('#add-size-dialog').data('bs.modal').isShown) {
            $('#add-size-dialog').find('#modalContent')
                    .load($(this).attr('href'));
        } else {
            $('#add-size-dialog').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
        }
		$('#add-size-dialog').find('#modalHeader').html('<h4>' + $(this).attr('data-title') + '</h4>');
		return false;
	});

	$('body').on('click','.seo_pages',function(){
		if ($('#add-seo-pages-dialog').data('bs.modal').isShown) {
            $('#add-seo-pages-dialog').find('#modalContent')
                    .load($(this).attr('href'));
        } else {
            $('#add-seo-pages-dialog').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
        }
		$('#add-seo-pages-dialog').find('#modalHeader').html('<h4>' + $(this).attr('data-title') + '</h4>');
		return false;
	});

	$('body').on('click','.delete_size',function(){
		var button = $(this);
		if(confirm('Вы уверены что хотите удалить?'))
			$.ajax({
				url: button.attr('href'),
				type: 'get',
				success: function(data) {
					$.pjax.reload({container:'#pajax_grid'});
				}
			});
		return false;
	});

	$('body').on('submit','#form-sizes',function(){
		var form = $(this);
		if(form.find('.has-error').length) {
			return false;
		}

		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			beforeSend: function(){
				$('#form-sizes').attr('sent',true);
			},
			error: function(){
				$('#form-sizes').removeAttr('sent');
			},
			success: function(data) {
				console.log(data);
				if(data.success=='true'){
					$.pjax.reload({container:'#pajax_grid'});
					$('#add-size-dialog').modal('hide')
				}else{
					alert('Ошибка');
				}
				$('#form-sizes').removeAttr('sent');
			}
		});

		return false;
	});


	$('body').on('submit','#form-seo-pages',function(){
		var form = $(this);
		if(form.find('.has-error').length) {
			return false;
		}

		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			beforeSend: function(){
				$('#form-seo-pages').attr('sent',true);
			},
			error: function(){
				$('#form-seo-pages').removeAttr('sent');
			},
			success: function(data) {
				console.log(data);
				if(data.success=='true'){
					$.pjax.reload({container:'#pajax_pages'});
					$('#add-seo-pages-dialog').modal('hide')
				}else{
					alert('Ошибка');
				}
				$('#form-seo-pages').removeAttr('sent');
			}
		});

		return false;
	});
});
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>
<? $form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
]); ?>

<? $this->beginBlock('params'); ?>
	<?
	if($params): ?>
		<? foreach ($params as $v): ?>
		<div class="form-group">
			<label for="Config_<?=$v->id;?>" class="col-sm-3 control-label"><?=$v->label;?></label>
			<div class="col-sm-9">
				<?php
				if($v->type=='text'){
					$field = $form->field($v, 'value', ['options' => ['class' => '']]);
					$field->template = "{input}\n{error}";
					echo $field->textArea(['class'=>'col-sm-5 form-control','rows'=>5,'name'=>'Config['.$v->id.']']);
				}else/*if($v->type=='bool')
					echo $form->switchGroup($v, 'value',array('enabledLabel'=>'Да','disabledLabel'=>'Нет','name'=>'Config['.$v->id.']'));
				elseif($v->type=='int')
					echo $form->numberField($v,'value',array('class'=>'col-sm-2 form-control','min'=>1,'name'=>'Config['.$v->id.']'));
				elseif($v->type=='email')
					echo $form->emailField($v,'value',array('class'=>'col-sm-3 form-control'));
				elseif($v->type=='html'){
				?>

				<? $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget',array(
						'model'=>$v,
						'attribute'=>'value',
						'options'=>array(
							'formattingTags'=>array('p','h1', 'h2'),
							'buttons'=>array('html',  'bold'),
							'minHeight'=> 150,
							'lang'=>'ru',
						),
						'htmlOptions'=>array(
							'name'=>'Config['.$v->id.']'
						)
					)); ?>
			<?
				}else*/{
					$field = $form->field($v, 'value', ['options' => ['class' => '']]);
					$field->template = "{input}\n{error}";
					echo $field->textInput(['name'=>'Config['.$v->id.']']);
				} ?>
				<p class="help-block"><?=$v->help;?></p>
			</div>
		</div>
		<? endforeach;
	endif;
	?>

	<div class="box-footer">
		<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
		<a class="btn btn-default btn-link" href="/admin/<?=$model->module;?>">К модулю</a>
		<a class="btn btn-default btn-link" href="/admin/modules">К списку</a>
	</div>
<? $this->endBlock(); ?>

<? $this->beginBlock('pages'); ?>
	<?php \yii\widgets\Pjax::begin(['id'=>'pajax_pages']); ?>
	<?= app\modules\admin\components\widgets\GridView::widget(array(
		'id'=>'grid',
		'dataProvider'=>$pages,
		'layout'=>'<div class=box>
	<div class=box-header>
	  <h3 class=box-title></h3>
	</div><!-- /.box-header -->
	<div class=box-body>{items}</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<div class="row">
			<div class="col-xs-6">
				<div class="dataTables_info pull-left">{summary}</div>
			</div>
			<div class="col-xs-6">
			{pager}
			</div>
		</div>
	</div>
  </div><!-- /.box -->',
		'columns'=>array(
			'name',
			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'buttons'=>[
					'update'=>function ($url, $model) {
						return \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/seo/backend/seopages/ajax?id='.$model->getPrimaryKey()], ['data-title'=>'Редактирование', 'data-pjax'=>0, 'class'=>'seo_pages']);
					},
					/*'delete'=>function ($url, $model) {
						return \yii\helpers\Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/admin/admin-images-sizes/delete?id='.$model->getPrimaryKey()], ['class'=>'delete_size', 'title'=>'Удаление']);
					},*/
				],
				'contentOptions'=>array('style'=>'width: 60px;text-align:center;')
			],
		),
	));
	?>
	<?php \yii\widgets\Pjax::end(); ?>
<? $this->endBlock(); ?>

<? $this->beginBlock('images'); ?>
	<? if($sizes): ?>
		<?php \yii\widgets\Pjax::begin(['id'=>'pajax_grid']); ?>
		<?= app\modules\admin\components\widgets\GridView::widget(array(
			'id'=>'grid',
			'dataProvider'=>$sizes,
			'layout'=>'<div class=box>
		<div class=box-header>
		  <h3 class=box-title></h3>
		  <div class="box-tools btn-group pull-right">
		    <button class="btn btn-success images_size" type="button" name="yt1" data-title="Добавить размер" href="/admin/admin-images-sizes/create?modules='.$model->getPrimaryKey().'"><span class="glyphicon glyphicon-plus"></span> Добавить размер</button>
			<button class="btn btn-warning" type=button onclick="$.pjax.reload({container:\'#pajax_grid\'});"><span class="glyphicon glyphicon-refresh"></button>
		  </div>
		</div><!-- /.box-header -->
		<div class=box-body>{items}</div><!-- /.box-body -->
		<div class="box-footer clearfix">
			<div class="row">
				<div class="col-xs-6">
					<div class="dataTables_info pull-left">{summary}</div>
				</div>
				<div class="col-xs-6">
				{pager}
				</div>
			</div>
		</div>
	  </div><!-- /.box -->',
			'columns'=>array(
				'field',
				'size',
				'width',
				'heigth',
				'method',
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update} {delete}',
					'buttons'=>[
						'update'=>function ($url, $model) {
							return \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/admin/admin-images-sizes/update?id='.$model->getPrimaryKey()], ['class'=>'images_size', 'data-pjax'=>0, 'data-title'=>'Редактирование']);
						},
						'delete'=>function ($url, $model) {
							return \yii\helpers\Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/admin/admin-images-sizes/delete?id='.$model->getPrimaryKey()], ['class'=>'delete_size', 'title'=>'Удаление']);
						},
					],
					'contentOptions'=>array('style'=>'width: 60px;text-align:center;')
				],
			),
		));
		?>
		<?php \yii\widgets\Pjax::end(); ?>

		<div class="box-footer">
			<?= Html::submitButton(($model->isNewRecord ? 'Добавить' : 'Сохранить'), ['class' => 'btn btn-primary']) ?>
			<a class="btn btn-default btn-link" href="/admin/<?=$model->module;?>">К модулю</a>
			<a class="btn btn-default btn-link" href="/admin/modules">К списку</a>
		</div>

	<? endif; ?>
<? $this->endBlock(); ?>
	<div class="box-body">
		<div class="nav-tabs-custom">
		<?
		echo yii\bootstrap\Tabs::widget([
			'items'=>array(
				array('label'=>'Параметры', 'content'=>$this->blocks['params'], 'active'=>true),
				array('label'=>'Seo страниц', 'content'=>$this->blocks['pages']),
				array('label'=>'Изображения', 'content'=>$this->blocks['images'],'visible'=>$sizes),
			)
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end() ?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'add-size-dialog',
    //'size' => 'modal-sm',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    //'clientOptions' => ['backdrop' => 'static']
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'add-seo-pages-dialog',
    //'size' => 'modal-sm',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    //'clientOptions' => ['backdrop' => 'static']
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>