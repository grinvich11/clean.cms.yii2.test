<?php

class m150329_164957_create_table_admin_filters extends yii\db\Migration
{
	public $tableName='{{admin_filters}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			[
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'user' => 'INT UNSIGNED NOT NULL',
				'controller' => 'VARCHAR(255) NOT NULL',
				'filter' => 'TEXT',
				'uri' => 'TEXT',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}