<?php

class m150329_170200_create_table_admin_modules extends yii\db\Migration
{
	public $tableName='{{admin_modules}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'module' => 'VARCHAR(255) NOT NULL PRIMARY KEY COMMENT "Модуль"',

				'name' => 'VARCHAR(255) DEFAULT NULL',
				'version' => 'VARCHAR(255) DEFAULT NULL',
				'state' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "0"  COMMENT "0 - not installed;\r\n1 - installed;"',
				'delete' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT "1"  COMMENT "0 - not installed;\r\n1 - installed;"',
				'position' => 'TINYINT(3) UNSIGNED NOT NULL DEFAULT "0"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}