<?php

class m150329_164950_create_table_config extends yii\db\Migration
{
	public $tableName='{{config}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'section' => 'VARCHAR(255) NOT NULL',
				'module' => 'VARCHAR(255) DEFAULT NULL',
				'param' => 'VARCHAR(128) NOT NULL',
				'value' => 'TEXT NOT NULL',
				'default' => 'TEXT DEFAULT NULL',
				'label' => 'VARCHAR(255) NOT NULL',
				'type' => "enum('string','bool','text','html','email','int') NOT NULL",
				'help' => 'VARCHAR(255) DEFAULT NULL',
				'notDelete' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				'visible' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',

				'UNIQUE KEY `param` (`param`) USING BTREE'
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}