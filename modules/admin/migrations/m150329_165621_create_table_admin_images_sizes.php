<?php

class m150329_165621_create_table_admin_images_sizes extends yii\db\Migration
{
	public $tableName='{{admin_images_sizes}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			array(
				'id' => 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'modules' => 'VARCHAR(255) NOT NULL',
				'field' => 'VARCHAR(255) DEFAULT "images" NOT NULL COMMENT "Поле"',
				'size' => 'VARCHAR(255) NOT NULL COMMENT "Размер"',
				'width' => 'SMALLINT(255) UNSIGNED NOT NULL COMMENT "Ширина"',
				'heigth' => 'SMALLINT(255) UNSIGNED NOT NULL COMMENT "Высота"',
				'method' => 'VARCHAR(255) NOT NULL COMMENT "Метод"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			),
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}