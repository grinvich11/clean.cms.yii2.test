<?php

class m150329_170706_create_table_config_params extends yii\db\Migration
{
	public $tableName='{{config}}';
	public $moduleName='admin';

	public function safeUp()
	{
		$this->insert(
			$this->tableName,
			[
				'section' => 'Админ панель',
				'param' => 'siteName',
				'value' => 'clean.cms',
				'default' => '',
				'label' => 'Название сайта',
				'type' => 'string'
			]
		);

		$this->insert(
			$this->tableName,
			[
				'section' => 'Почта',
				'param' => 'noreplyEmail',
				'value' => 'test@example.com',
				'default' => '',
				'label' => 'No-reply email',
				'type' => 'email'
			]
		);

		$this->insert(
			$this->tableName,
			[
				'section' => 'Почта',
				'param' => 'fromName',
				'value' => 'TestSite',
				'default' => '',
				'label' => 'Имя при отправке почты с сайта',
				'type' => 'string'
			]
		);

		$this->insert(
			$this->tableName,
			[
				'section' => 'Сайт',
				'param' => 'siteIsConstruction',
				'value' => '0',
				'default' => '0',
				'label' => 'Закрыть на обслуживание',
				'type' => 'bool'
			]
		);

		$this->insert(
			$this->tableName,
			[
				'section' => 'Админ панель',
				'param' => 'admin_pjax_reload',
				'value' => '0',
				'default' => '0',
				'label' => 'Включить pjax загрузку',
				'help' => 'Быстрая загрузка админки',
				'type' => 'bool'
			]
		);
	}

	public function safeDown()
	{
		$this->delete($this->tableName, 'param=:param', [':param'=>'siteName']);
		$this->delete($this->tableName, 'param=:param', [':param'=>'noreplyEmail']);
		$this->delete($this->tableName, 'param=:param', [':param'=>'fromName']);
		$this->delete($this->tableName, 'param=:param', [':param'=>'siteIsConstruction']);
	}
}