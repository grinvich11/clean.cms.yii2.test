<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\helpers\AdminHelper;
use yii\web\NotFoundHttpException;
use app\modules\admin\models\AdminImagesSizes;
use app\modules\admin\components\CAdminController;

class AdminImagesSizesController extends CAdminController
{
	public function actionCreate()
	{
		set_time_limit(0);
		$model=new AdminImagesSizes();
		if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			if(!$model->validate()){
				return ActiveForm::validate($model);
			}else{
				if($model->save()){
					AdminHelper::regenerateImage(Yii::getAlias('@webroot').'/images/'.Yii::$app->getModule($model->modules)->path_images, $model);
				}
			}

			$response = [
				'data'=>$model->getAttributes(),
				'success'=>'true'
			];
			return $response;
		}else{
			return $this->renderAjax('_form',array(
				'model'=>$model
			));
		}
	}

	public function actionUpdate($id)
	{
		set_time_limit(0);
		$model=$this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
			Yii::$app->response->format = Response::FORMAT_JSON;

			if(!$model->validate()){
				return ActiveForm::validate($model);
			}else
				if($model->save()){
					AdminHelper::regenerateImage(Yii::getAlias('@webroot').'/images/'.Yii::$app->getModule($model->modules)->path_images, $model);
				}

			$response = [
				'data'=>$model->getAttributes(),
				'success'=>'true'
			];
			return $response;
		}else{
			return $this->renderAjax('_form',array(
				'model'=>$model
			));
		}
	}

	public function actionDelete($id){
		$size=$this->findModel($id);

		AdminHelper::removeDir(Yii::getAlias('@webroot').'/images/'.Yii::$app->getModule($size->modules)->path_images.'/'.$size->field.'/'.$size->size);
		$size->delete();
	}

	protected function findModel($id)
	{
		$model=AdminImagesSizes::findOne($id);
		if($model===null)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}

	public function actionClearUnUse(){
		$path=Yii::getAlias('@webroot').'/images/news/images';

		$files = glob($path.'/*');
		$size=$this->clean($files, $size);

		$files = glob($path.'/*/*');
		$size=$this->clean($files, $size);

		echo '<br>clean - '.round($size,2).'Mb';
	}

	private function clean($files, $size){
		if($files){
			foreach($files as $file){
				if(is_file($file)){
					$file_name=end(explode('/', $file));
					if(!in_array($file_name, ['watermark.png', 'no_image.png', 'watermark.php', '.htaccess'])){
						$news=\app\modules\news\models\News::find()->select(['id', 'images'])->where(['LIKE', 'images', $file_name])->one();
						if(!$news){
							$size+=filesize($file)/1024/1024;
							//echo $file.'<br>';
							echo $file_name.'<br>';
							unlink($file);
						}
					}
				}
			}
		}
		return $size;
	}
}