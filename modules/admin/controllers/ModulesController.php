<?php
namespace app\modules\admin\controllers;

use Yii;
use app\helpers\AdminHelper;
use yii\filters\AccessControl;
use app\modules\admin\models\AdminModulesSearch;
use app\modules\admin\components\CrudAdminModuleController;

class ModulesController extends CrudAdminModuleController
{
	public function getModelClass(){
		return $this->model='app\modules\admin\models\AdminModules';
	}

	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
						'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->getIsAdmin();
                        }
                    ]
                ]
            ]
        ];
    }

	public function actionIndex() {
		AdminHelper::scanModules();

		$this->breadcrumbs=[
			'Модули',
		];

		$searchModel = new AdminModulesSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}

	public function actionUpdate($id)
	{
		$model=$this->findModel($id);

		if(Yii::$app->request->post()){
			$model->saveParams();
			return $this->redirect(['index']);
		}

		$params=\app\models\Config::findAll(['module'=>$model->getPrimaryKey()]);

		//$sizes=\app\modules\admin\models\AdminImagesSizes::findAll(['module'=>$model->getPrimaryKey()])->getModels();

		$query = \app\modules\admin\models\AdminImagesSizes::find()->where(['modules'=>$model->getPrimaryKey()]);

		$sizes= new \yii\data\ActiveDataProvider([
			'query'=>$query,
			'pagination'=>false,
			//'sort' => ['attributes' => ['section', 'label', 'value']]
		]);

		$query = \app\modules\seo\models\SeoPages::find()->where(['module'=>$model->getPrimaryKey()]);

		$pages= new \yii\data\ActiveDataProvider([
			'query'=>$query,
			'pagination'=>false,
			//'sort' => ['attributes' => ['section', 'label', 'value']]
		]);

		$this->breadcrumbs=[
			['label'=>'Модули', 'url'=>['index']],
			$model->name,
			'Редактирование',
		];

		return $this->render('_form', [
			'model'=>$model,
			'params'=>$params,
			'sizes'=>$sizes,
			'pages'=>$pages,
		]);
	}

	protected function findModel($id)
	{
		$model=\app\modules\admin\models\AdminModules::findOne(['module'=>$id]);
		if($model===null)
			throw new \yii\web\NotFoundHttpException('The requested module does not exist.');
		return $model;
	}

	public function actionTurnmodule($id)
	{
		$model=$this->findModel($id);
		$ih=new \app\modules\admin\components\MainInstallHelper($id);

		if($_GET['action']=='OFF' && $model->delete)
			$ih->uninstall();
		if($_GET['action']=='ON')
			$ih->install();
	}

	public function actionSetsortvalue(){
		if(isset($_GET['ids'])){
			$ids=explode(',', $_GET['ids']);
			foreach ($ids as $id) {
				echo $id;
				$find=call_user_func_array([&$this->model, 'findOne'], [(int) $id]);
				if($find){
					$find->updateAll(['position'=>$i++], 'module=:id', [':id'=>$id]);
				}
			}
		}
	}
}
