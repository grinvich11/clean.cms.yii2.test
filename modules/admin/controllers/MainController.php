<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\CAdminController;
use Yii;

class MainController extends CAdminController
{

    public function actionIndex()
    {
		$this->view->title=Yii::$app->params['siteName'].' - Панель администратора';
        return $this->render('index');
    }

	public function actionSavestate(){
		setcookie('body_class', $_GET['body_class'], time()+60*60*24*365, '/admin', $_SERVER['HTTP_HOST']);
	}

	/*public function actionGenerateImage(){
		$items=News::model()->findAll(array('condition'=>'images<>""'));
		if($items){
			$imagePath = $_SERVER['DOCUMENT_ROOT'] . '/images/news/';
			foreach ($items as $item) {
				$images=SiteHelper::returnArrayImagesFromDB($item->images);
				if($images){
					foreach ($images as $name) {
						if(is_file($imagePath. $name)){
							$image = Yii::app()->image->load($imagePath. $name);
							$image->resize(88, 88, Image::AUTO);
							$image->save($imagePath .'mini/'. $name);

							copy($imagePath. $name,$imagePath .'small/'. $name);
							ImageHelper::thumb($imagePath .'small/'. $name, 157, 157, "crop", "", "");

							copy($imagePath. $name,$imagePath .'middle/'. $name);
							ImageHelper::thumb($imagePath .'middle/'. $name, 290, 136, "crop", "", "");
						}
					}
				}
			}
		}
		echo 'finished';
	}*/
}
