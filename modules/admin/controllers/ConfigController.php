<?php
namespace app\modules\admin\controllers;

use Yii;
use app\models\Config;
use app\models\ConfigSearch;
use app\components\MyConfig;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\modules\admin\components\CAdminController;

class ConfigController extends CAdminController
{
	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
						'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->getIsAdmin();
                        }
                    ]
                ]
            ]
        ];
    }

	public function actionCreate()
    {
        $model=new Config();

		if($model->load(Yii::$app->request->post()))
		{
			if($model->save()){
				return $this->redirect(['index']);
			}
		}

		$this->breadcrumbs=[
			['label'=>'Настройки сайта', 'url'=>['index']],
			'Добавить',
		];

		return $this->render('_form', [
			'model'=>$model,
		]);
    }

	public function actionUpdate($id)
	{
		$model=$this->findModel($id);

		if($model->load(Yii::$app->request->post()))
		{
			if($model->save()){
				new MyConfig();
				return $this->redirect(['index']);
			}
		}

		$this->breadcrumbs=[
			['label'=>'Настройки сайта', 'url'=>['index']],
			$model->label,
			'Редактирование',
		];

		return $this->render('_form', [
			'model'=>$model,
		]);
	}

	public function actionIndex()
	{
		$this->breadcrumbs=[
			'Настройки сайта',
		];

		$searchModel = new ConfigSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}

	public function actionAll()
	{
		$this->breadcrumbs=[
			'Настройки сайта',
		];

		if(Yii::$app->request->post()){
			$settings=$_POST['Config'];
			if(!empty($settings)){
				foreach ($settings as $key => $value) {
					$find=Config::findOne($key);
					if($find){
						if($find->type=='int')
							$value=(int)$value;
						if($find->type=='bool')
							$value=(bool)$value;

						$find->value=$value;
						$find->save(false);
					}
				}
			}

			Yii::$app->session->setFlash('success', 'Параметры успешно сохранены');
			if(!Yii::$app->getRequest()->getIsPjax()){
				return $this->redirect(['all']);
			}
		}

		$params=Config::find()->where('ISNULL(module)')->orderBy(['section'=>SORT_ASC, 'label'=>SORT_ASC])->all();
		if($params){
			foreach ($params as $value) {
				$sections[$value->section][]=$value;
			}
		}

		$params_module=Config::find()->where('!ISNULL(module)')->orderBy(['section'=>SORT_ASC, 'label'=>SORT_ASC])->all();
		if($params_module){
			foreach ($params_module as $value) {
				if($value->modules){
					$sections_module[$value->modules->name][]=$value;
				}
			}
		}

        return $this->render('all', [
            'sections' => $sections,
            'sections_module' => $sections_module,
        ]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
	}

	protected function findModel($id)
	{
		$model=Config::findOne(['id'=>$id]);
		if($model===null)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
}