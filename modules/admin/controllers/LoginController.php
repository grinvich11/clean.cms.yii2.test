<?
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use app\modules\admin\models\LoginForm;

class LoginController extends Controller
{
	public $layout='login';

	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
						'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
						'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
				'denyCallback' => function($rule, $action) {
                    if ($action->id == 'index') {
                        return $action->controller->redirect('/admin');
                    }
                    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                },
            ]
        ];
    }

	public function actionIndex()
    {
		if (!Yii::$app->user->isGuest) {
            return $this->goBack('/admin');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$url=Yii::$app->getUser()->getReturnUrl('/admin');
            return $this->redirect((substr_count($url, 'admin')>0 || substr_count($url, 'backend')>0) ? $url : '/admin');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

	public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}