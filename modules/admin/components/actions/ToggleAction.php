<?php
/**
 * @link http://phe.me
 * @copyright Copyright (c) 2014 Pheme
 * @license MIT http://opensource.org/licenses/MIT
 */

namespace app\modules\admin\components\actions;

use Yii;
use yii\base\InvalidConfigException;

/**
 * @author Aris Karageorgos <aris@phe.me>
 */
class ToggleAction extends \pheme\grid\actions\ToggleAction
{
    /**
     * Run the action
     * @param $id integer id of model to be loaded
     *
     * @throws \yii\web\MethodNotAllowedHttpException
     * @throws \yii\base\InvalidConfigException
     * @return mixed
     */
    public function run($id)
    {
        $id = (int)$id;
        $result = null;

        if (empty($this->modelClass) || !class_exists($this->modelClass)) {
            throw new InvalidConfigException("Model class doesn't exist");
        }
        /* @var $modelClass \yii\db\ActiveRecord */
        $modelClass = $this->modelClass;
		$this->attribute=$_GET['attribute'];
        $attribute = $this->attribute;
        $model = $modelClass::find()->select(['id', $attribute])->where(['id' => $id]);

        if (!empty($this->andWhere)) {
            $model->andWhere($this->andWhere);
        }

        $model = $model->one();

        if (!$model->hasAttribute($this->attribute)) {
            throw new InvalidConfigException("Attribute doesn't exist");
        }

        if ($model->$attribute == $this->onValue) {
            $model->$attribute = $this->offValue;
        } else {
            $model->$attribute = $this->onValue;
        }

        if ($model->updateAttributes([$attribute=>$model->$attribute])) {
            if ($this->setFlash) {
                Yii::$app->session->setFlash('success', $this->flashSuccess);
            }
        } else {
            if ($this->setFlash) {
                Yii::$app->session->setFlash('error', $this->flashError);
            }
        }
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->end();
        }
        /* @var $controller \yii\web\Controller */
        $controller = $this->controller;
        if (!empty($this->redirect)) {
            return $controller->redirect($this->redirect);
        }
        return $controller->redirect(Yii::$app->request->getReferrer());
    }
}
