<?php
namespace app\modules\admin\components\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

/**
 * PublishedMultipleAction published the selected rows of the GridView.
 */
class PublishedMultipleAction extends Action
{
    /**
     * @var string the model class name. This property must be set.
     */
    public $modelClass;
    /**
     * @var string the primary key name.
     */
    public $primaryKey = 'id';
    /**
     * @var string|array the URL to be redirected to after deleting.
     */
    public $redirectUrl;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->modelClass)) {
            throw new InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    /**
     * Runs the action.
     *
     * @throws \yii\base\NotFoundHttpException the models is not found.
     */
    public function run()
    {
        /* @var $modelClass \yii\db\ActiveRecord */
		$modelClass = $this->modelClass;
		$models = $modelClass::find()->where([$this->primaryKey => Yii::$app->request->post('ids')])->all();

		if (empty($models)) {
			throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
		} else {
			foreach ($models as $model) {
				$model->published=$model->published=='1' ? 0 : 1;
				$model->update();
			}
			if (isset($this->afterDeleteCallback)) {
				call_user_func($this->afterDeleteCallback, $this);
			}

			return true;
			//return $this->redirect();
		}
    }

    /**
     * Redirects the browser to the previous page or the specified URL from [[redirectUrl]].
     */
    public function redirect()
    {
        $previous = Url::previous(Widget::RETURN_URL_PARAM);

        return !empty($this->redirectUrl) ? $this->controller->redirect($this->redirectUrl)
            : $this->controller->redirect($previous);
    }
}
