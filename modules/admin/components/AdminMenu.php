<?php
namespace app\modules\admin\components;

use yii\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Bootstrap menu.
 *
 * @see <http://twitter.github.com/bootstrap/components.html#navs>
 *
 * @package booster.widgets.navigation
 */
class AdminMenu extends Menu {
	public $submenuTemplate = "\n<ul class='treeview-menu'>\n{items}\n</ul>\n";

	/**
	 *### .getDropdownCssClass()
	 *
	 * Returns the dropdown css class.
	 *
	 * @return string the class name
	 */
	public function getDropdownCssClass() {

		return 'treeview';
	}

	/**
	 *### .renderMenu()
	 *
	 * Renders the menu items.
	 *
	 * @param array $items menu items. Each menu item will be an array with at least two elements: 'label' and 'active'.
	 * It may have three other optional elements: 'items', 'linkOptions' and 'itemOptions'.
	 */
	protected function renderMenu($items) {
		$n = count($items);

		if ($n > 0) {
			foreach ($items as &$item) {
				$classes = array();

				if (!isset($item['divider'])) {
					if (isset($item['items'])) {
						$classes[] = $this->getDropdownCssClass();
					}

					if (!empty($classes)) {
						$classes = implode(' ', $classes);
						if (!empty($item['itemOptions']['class'])) {
							$item['itemOptions']['class'] .= ' ' . $classes;
						} else {
							$item['itemOptions']['class'] = $classes;
						}
					}
				}
			}
		}

		parent::renderMenu($items);
	}


	/**
     * Recursively renders the menu items (without the container tag).
     * @param array $items the menu items to be rendered recursively
     * @return string the rendering result
     */
    protected function renderItems($items)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
			if (!empty($item['items'])) {
				$class[] = $this->getDropdownCssClass();
			}

            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);
                $menu .= strtr($submenuTemplate, [
                    '{items}' => $this->renderItems($item['items']),
                ]);
            }
            if ($tag === false) {
                $lines[] = $menu;
            } else {
                $lines[] = Html::tag($tag, $menu, $options);
            }
        }

        return implode("\n", $lines);
    }

	/**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item)
    {
		if(isset($item['visible']) && $item['visible']===false)
			return true;

		if (isset($item['icon'])) {
			if (strpos($item['icon'], 'icon') === false && strpos($item['icon'], 'fa') === false && strpos($item['icon'], 'ion-') === false) {
				$item['icon'] = 'glyphicon glyphicon-' . implode(' glyphicon-', explode(' ', $item['icon']));
				$item['label'] = "<span class='" . $item['icon'] . "'></span>\r\n<span>" . $item['label']."</span>";
			} else {
				$item['label'] = "<i class='" . $item['icon'] . "'></i>\r\n<span>" . $item['label']."</span>";
			}
		}

		if (isset($item['count_new']) && $item['count_new']) {
			$item['label'] .= "<span class='pull-right-container'><span class='label label-primary pull-right'>{$item['count_new']}</span></span>";
		}

		if (isset($item['items']) && !empty($item['items'])) {
			//die('--'.print_r($item, 1).'--');
			$item['data-pjax']=0;
			$item['label'] = $item['label'].'<i class="fa fa-angle-left pull-right"></i>';
		}


        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
			if (isset($item['items']) && !empty($item['items']))
				$template=str_replace('<a href="{url}">', '<a href="{url}" data-pjax=0>', $template);

            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
            ]);
        } else {
            $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);

            return strtr($template, [
                '{label}' => $item['label'],
            ]);
        }
    }
}
