<?php
namespace app\modules\admin\components;

use yii\db\Schema;
use yii\db\Migration;
//Yii::import('application.vendor.yiiext.migrate-command.EMigrateCommand');

class MigrationCommand extends Migration
{/*
	public function actionCreate($args)
	{
		$moduleName=$args[0];

		///EMigrateCommand
		// if module is given adjust path
		if (count($args)==2) {
			if (!isset($this->modulePaths[$args[0]])) {
				echo "\nError: module '{$args[0]}' is not available!\n\n";
				return 1;
			}
			$this->migrationPath = Yii::getPathOfAlias($this->modulePaths[$args[0]]);
			$args = array($args[1]);
		} else {
			$this->migrationPath = Yii::getPathOfAlias($this->modulePaths[$this->applicationModuleName]);
		}
		if (!is_dir($this->migrationPath)) {
			echo "\nError: '{$this->migrationPath}' does not exist or is not a directory!\n\n";
			return 1;
		}
		///EMigrateCommand



		if(isset($args[0]))
			$name=$args[0];
		else
			$this->usageError('Please provide the name of the new migration.');

		if(!preg_match('/^\w+$/',$name)) {
			echo "Error: The name of the migration must contain letters, digits and/or underscore characters only.\n";
			return 1;
		}

		if(!is_file(Yii::app()->getBasePath().'/runtime/lang.txt'))
			$typeMigration = self::prompt("Enter type migration:\n1 - table\n2 - insert\n3 - config\n4 - seo_pages\ntype: ");
		else
			$typeMigration = self::prompt("Enter type migration:\n1 - table\n2 - insert\n3 - config\n4 - seo_pages\n5 - table with lang\ntype: ");

		$orig_name=$name;
		$name='m'.gmdate('ymd_His').'_'.$name;


		switch ($typeMigration) {
			case 1:
				$tableName='{{'.self::prompt("Enter table name (without prefix):").'}}';
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name, '{tableName}'=>$tableName, '{moduleName}'=>$moduleName));
				break;
			case 5:
				$tableName=self::prompt("Enter table name (without prefix):");
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name, '{tableName}'=>'{{'.$tableName.'}}', '{moduleName}'=>$moduleName));
				break;
			case 2:
				$tableName='{{'.self::prompt("Enter table name (without prefix):").'}}';
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name, '{tableName}'=>$tableName, '{moduleName}'=>$moduleName));
				break;
			case 3:
				$tableName='{{config}}';
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name, '{tableName}'=>$tableName, '{moduleName}'=>$moduleName));
				break;
			case 4:
				$tableName='{{seo_pages}}';
				$pageName=self::prompt("Enter page name:");
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name, '{tableName}'=>$tableName, '{moduleName}'=>$moduleName, '{pageName}'=>$pageName));
				break;
			default:
				$content=strtr($this->getTemplate($typeMigration), array('{ClassName}'=>$name));
				break;
		}
		$file=$this->migrationPath.DIRECTORY_SEPARATOR.$name.'.php';

		if($this->confirm("Create new migration '$file'?"))
		{
			file_put_contents($file, $content);
			echo "New migration created successfully.\n";
			if($typeMigration==5){
				$className = 'm' . gmdate('ymd_His', time() + 1) . '_' . $orig_name . '_lang';
				$content = strtr($this->getTemplate('lang'), array('{ClassName}' => $className, '{tableName}'=>'{{'.$tableName.'_lang'.'}}', '{tableName_rel}'=>$tableName));
				$file = $this->migrationPath . DIRECTORY_SEPARATOR . $className . '.php';
				file_put_contents($file, $content);
				echo "New migration for language model created successfully.\n";
			}
		}
	}

	protected function getTemplate($type)
	{
		switch ($type) {
			case 1:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_table').'.php');
				break;
			case 5:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_table').'.php');
				break;
			case 2:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_insert').'.php');
				break;
			case 3:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_config').'.php');
				break;
			case 4:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_seo_pages').'.php');
				break;
			case 5:
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_table').'.php');
				break;
			case 'lang':
				return file_get_contents(Yii::getPathOfAlias('application.modules.admin.migrations.templates.migration_type_lang').'.php');
				break;
			default:
				if ($this->templateFile!==null) {
					return parent::getTemplate();
				} else {
					return str_replace('CDbMigration', 'EDbMigration', parent::getTemplate());
				}
				break;
		}


	}*/
}