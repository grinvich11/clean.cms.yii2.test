<?php

namespace app\modules\admin\components;

use Yii;
use yii\filters\AccessControl;

class CAdminController extends \app\components\Controller
{
	public $layout='admin_column';
	public $body_class='hold-transition sidebar-mini skin-blue';

	public function init()
	{
		parent::init();

		if(!Yii::$app->user->isGuest){

		}else{
			if(Yii::$app->user->isGuest){
				//Yii::$app->runAction('admin/login', $params);
				$this->redirect('/admin/login');
			}else{
				$this->redirect('/');
			}
		}

		if(isset($_COOKIE['body_class']))
			$this->body_class=filter_var($_COOKIE['body_class'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		Yii::$app->params['rememberFilters']=Yii::$app->session->get('rememberFilters');
		Yii::$app->params['admin_pagination']=Yii::$app->session->get('admin_pagination');
	}


	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        /*'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->identity->getIsAdmin();
                        }*/
                    ]
                ]
            ]
        ];
    }

	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
/*
	protected function beforeAction($action)
	{
		if($action->id=='index' && $_SERVER['REQUEST_URI']!='/admin/'.$action->controller->id.'/index' && $_SERVER['REQUEST_URI']!='/admin/'.$action->controller->id && !isset($_GET['export_xls'])
			&& !isset($_GET['export']) && !isset($_GET['export_mnemo']) && !isset($_GET['export_english'])){
			$filter=AdminFilters::model()->findByAttributes(array('user'=>Yii::app()->user->getID(),'controller'=>$action->controller->id));
			if(!$filter){
				$filter=new AdminFilters();
				$filter->controller=$action->controller->id;
				$filter->filter=  serialize($_GET);
				$filter->uri= Yii::app()->getRequest()->getUrl();
				$filter->save();
			}else{
				$filter->controller=$action->controller->id;
				$filter->filter=serialize($_GET);
				$filter->uri=Yii::app()->getRequest()->getUrl();
				$filter->save();
			}
		}
		if($action->id=='index' && !isset($_GET['ajax'])){
			$filter=AdminFilters::model()->findByAttributes(array('user'=>Yii::app()->user->getID(),'controller'=>$action->controller->id));
			if($filter){
				//$this->redirect($filter->filter);
				$_GET=unserialize($filter->filter);
				$_POST['grid_keys']=$filter->uri;
			}
		}


		return true;
	}
*/
	public function actionProcessItems(){
		if($_POST['selectedItems']=='delete'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->delete();
				}
			}
		}

		if($_POST['selectedItems']=='publish'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->published=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unpublish'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->published=0;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='active'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->active=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unactive'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->active=0;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='event'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->state=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unevent'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->state=1;
					$model->save();
				}
			}
		}
	}
}
