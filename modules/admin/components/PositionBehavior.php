<?php
namespace app\modules\admin\components;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class PositionBehavior extends Behavior
{
    public $attribute = 'position';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'setPosition'
        ];
    }

    public function setPosition( $event )
    {
		if ($this->owner->hasAttribute($this->attribute))
			$this->owner->{$this->attribute} = (int)Yii::$app->db->createCommand("SELECT MAX(`{$this->attribute}`)+1 FROM " . call_user_func_array([&$this->owner, 'tableName'], []))->queryScalar();
    }
}