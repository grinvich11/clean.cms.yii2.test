<?php
namespace app\modules\admin\components;

use yii\data\Pagination as ParentPagination;
use Yii;

class Pagination extends ParentPagination
{
	public $pageParam='p';
	public $pageSizeParam='pp';
	public $size=NULL;

	public function init()
    {
		if ($this->size === NULL)
			$this->pageSize=Yii::$app->params['admin_pagination'];
		else
			$this->pageSize=$this->size;

		parent::init();
    }
}
