<?php
namespace app\modules\admin\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\helpers\AdminHelper;
use app\helpers\SiteHelper;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use app\modules\admin\models\AdminImagesSizes;
use vova07\imperavi\actions\GetAction;
use vova07\imperavi\actions\UploadAction;
use app\modules\admin\components\actions\ToggleAction;

class CrudAdminModuleController extends AdminModuleController
{   
   public $onlyForm=false;
   public $name_attribute='name';
   public $title_for_crumb;

   public function getTitleForCrumb(){
		if(!$this->title_for_crumb){
			return $this->module->name;
		}else{
			return $this->title_for_crumb;
		}
   }

   public function actions()
   {
		return [
			 'images-get' => [
				 'class' => GetAction::className(),
				 'url' => '/uploads/'.$this->module->id.'/',
				 'path' => '@webroot/uploads/'.$this->module->id.'/',
				 'type' => GetAction::TYPE_IMAGES,
			 ],
			 'image-upload' => [
				 'class' => UploadAction::className(),
				 'url' => '/uploads/'.$this->module->id.'/',
				 'path' => '@webroot/uploads/'.$this->module->id.'/'
			 ],
			'toggle' => [
				 'class' => ToggleAction::className(),
				 'modelClass' => $this->model,
			 ],
			'delete-multiple' => [
				 'class' => actions\DeleteMultipleAction::className(),
				 'modelClass' => $this->model,
			 ],
			'published-multiple' => [
				 'class' => actions\PublishedMultipleAction::className(),
				 'modelClass' => $this->model,
			 ],
		 ];
    }

	public function actionCreate()
	{
		$model=new $this->model;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if($model->load(Yii::$app->request->post()))
		{
			if($model->save()){
				if(Yii::$app->getRequest()->getIsPjax()){
					header('X-PJAX-URL: /'.$this->module->id.'/'.$this->module->module->controller->id.'/index');
					return Yii::$app->runAction($this->module->id.'/'.$this->module->module->controller->id.'/index');
				}else{
					return $this->redirect(['index']);
				}
			}
		}

		$this->breadcrumbs=[
			['label'=>$this->getTitleForCrumb(), 'url'=>['index']],
			'Добавить',
		];

		return $this->render('_form',[
			'model'=>$model,
		]);
	}

	public function actionUpdate($id)
	{
		$model=$this->findModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if($model->load(Yii::$app->request->post()))
		{
			if($model->save()){
				if(Yii::$app->getRequest()->getIsPjax()){
					header('X-PJAX-URL: /'.$this->module->id.'/'.$this->module->module->controller->id.'/index');
					return Yii::$app->runAction($this->module->id.'/'.$this->module->module->controller->id.'/index');
				}else{
					return $this->redirect(['index']);
				}
			}
		}

		$this->breadcrumbs=[
			['label'=>$this->getTitleForCrumb(), 'url'=>['index']],
			isset($model->{$this->name_attribute}) ? $model->{$this->name_attribute} : '',
			'Редактирование',
		];

		return $this->render('_form', [
			'model'=>$model,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			if(Yii::$app->getRequest()->getIsPjax()){
				header('X-PJAX-URL: '.(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : '/'.$this->module->id.'/'.$this->module->module->controller->id.'/index'));
				return Yii::$app->runAction($this->module->id.'/'.$this->module->module->controller->id.'/index');
			}else{
				return $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : ['index']);
			}
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->breadcrumbs=[
			$this->getTitleForCrumb(),
		];

		if(!$this->onlyForm){
			$searchModel = new $this->search_model;
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

			return $this->render('index', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
			]);
		}else{
			$model=$this->findModelOrCreate();

			if($model->load(Yii::$app->request->post()))
			{
				if($model->save()){
					Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
					//header('X-PJAX-URL: /'.$this->module->id.'/backend/main/index');
					//return Yii::$app->runAction($this->module->id.'/backend/main/index');
					if(!Yii::$app->getRequest()->getIsPjax()){
						return $this->redirect(['index']);
					}
				}
			}

			return $this->render('_form', [
				'model'=>$model,
			]);
		}
	}

	public function actionView()
	{
		$model=$this->findModel($_GET['id']);

		$this->breadcrumbs=[
			['label'=>$this->getTitleForCrumb(), 'url'=>['index']],
			isset($model->{$this->name_attribute}) ? $model->{$this->name_attribute} : '',
			'Просмотр',
		];

		return $this->render('view',[
			'model'=>$model,
		]);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	protected function findModel($id) {
		$tmp_model=new $this->model;

		if (isset($tmp_model->behaviors()['ml'])) {
			$model = call_user_func_array([&$this->model, 'find'], [])->multilingual()->where(['id'=>$id])->one();		
		} else {
			$model = call_user_func_array([&$this->model, 'findOne'], [(int) $id]);
		}
		if ($model === null)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}

	protected function findModelOrCreate() {
		$model=new $this->model;

		if (isset($tmp_model->behaviors()['ml'])) {
			$model = call_user_func_array([&$this->model, 'find'], [])->multilingual()->one();
		} else {
			$model = call_user_func_array([&$this->model, 'find'], [])->one();
		}
		if ($model === null)
			$model=new $this->model;
		return $model;
	}

	public function actionJupload() {
		$imagePath=$_SERVER['DOCUMENT_ROOT'].'/images/'.$this->module->path_images.'/';
		$data = [
			'error' => true,
		];

		$field=key(current($_FILES)['name']);
		$modelName=key($_FILES);
		$path=isset($_POST[$modelName][$field.'_']['path']) ? $_POST[$modelName][$field.'_']['path'] : '';
		//print_r($_POST);
		//return;
		$model = call_user_func_array([&$this->model, 'findOne'], [(int) $_POST[$modelName]['id']]);
		if(!$model)
			$model=new $this->model;
		//$modelName = CHtml::modelName($model);

        if (Yii::$app->request->isAjax) {
			$file = UploadedFile::getInstance($model, $field);
			if (!is_null($file)) {
				$mime=$file->type;
				if(in_array(str_replace('image/', '', $mime), ['jpg','jpeg','png','gif'])){
					$ext=str_replace('image/', '', $mime);
					if(isset($model->name)){
						$name=SiteHelper::str2url($model->name).'_'.  uniqid();
						if(file_exists($imagePath.$name.'.'.$ext)){
							 $name=SiteHelper::str2url($model->name).'_'.$i++;
						}
					}else{
						$name=uniqid();
						if(file_exists($imagePath.$name.'.'.$ext)){
							 $name=$name.$i++;
						}
					}

					$name=$name.'.'.$ext;
					$model->$field=$name;

					if(!$file->saveAs($imagePath.$field.'/'.$name)) throw new ServerErrorHttpException('Error upload');

					$sizes=AdminImagesSizes::find()->where(['modules'=>$this->module->id])->all();
					if($sizes){
						foreach ($sizes as $size) {
							AdminHelper::generateImage($imagePath,$name, $size);
						}
					}

					//if ($model->save()) {
						$data = [
							'error' => false,
							'image' => [
								'id' => $model->$field,
								'src' => '/images/'.$this->module->path_images.'/'.$path.$model->$field.'?='.$model->modified,
								'big_src' => '/images/'.$this->module->path_images.'/'.$field.'/'.$model->$field.'?='.$model->modified,
								'actions' => [
									'delete' => Url::toRoute(['/cabinet/settings/ImageDelete', 'file_id' => $model->$field]),
								],
							],
						];
					//}
				}
			}
		}

		echo Json::encode($data);
	}

	public function actionSetsortvalue(){
		if(isset($_GET['ids'])){
			$ids=explode(',', $_GET['ids']);
			foreach ($ids as $id) {
				echo $id;
				$find=call_user_func_array([&$this->model, 'findOne'], [(int) $id]);
				if($find){
					$find->updateAll(['position'=>$i++], 'id=:id', [':id'=>$id]);
				}
			}
		}
	}

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}
}
