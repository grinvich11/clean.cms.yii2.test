<?php
namespace app\modules\admin\components;

use Yii;
use app\modules\admin\models\AdminModules;
use app\modules\admin\models\AdminImagesSizes;
use app\helpers\ConsoleHelper;
use yii\image\drivers\Image;
use app\modules\menu\models\Menu;
use app\helpers\AdminHelper;

class MainInstallHelper {
	public $path_images;
	public $module;
	public $module_object;
	public $images;
	public $uploads_dir=false;

	public function __construct($module)
	{
		$this->module=$module;

		$this->module_object=Yii::$app->getModule($module);
		if(isset($this->module_object->path_images))
			$this->path_images=$this->module_object->path_images;

		if(isset($this->module_object->install) && isset($this->module_object->install['images']))
			$this->images=$this->module_object->install['images'];
		if(isset($this->module_object->install) && isset($this->module_object->install['uploads_dir']))
			$this->uploads_dir=$this->module_object->install['uploads_dir'];
	}

	public function install($hide_sql=false){
		if($this->module_object->delete){
			$module=AdminModules::findOne(['module'=>$this->module]);
			if(!$module){
				$module=new AdminModules();
			}
			if($module->state==0){
				$this->upSql($hide_sql);
				if($this->images)
					$this->generateImages();

				$this->addMenuItem();
			}
			$module->module=$this->module;
			$module->name=$this->module_object->name;
			$module->version=$this->module_object->version;
			$module->state=1;
			$module->delete=(int)$this->module_object->delete;
			if(!$module->save()){
				print_r($module->getErrors());
				echo 'error';
			}else{
				echo 'success';
			}
		}else{
			$this->upSql($hide_sql);
			if($this->images)
				$this->generateImages();
		}

		if($this->uploads_dir){
			$uploads=Yii::getAlias('@webroot').'/uploads';
			if(!is_dir($uploads)){
				mkdir($uploads);
			}
			$folder=Yii::getAlias('@webroot').'/uploads/'.$this->module;
			if(!is_dir($folder)){
				mkdir($folder);
			}
		}
	}

	public function uninstall(){
		$module=  AdminModules::findOne(['module'=>$this->module,'state'=>1,'delete'=>1]);
		if($module){
			$module->module=$this->module;
			$module->state=0;
			if($module->save()){
				echo 'saved';
			}  else {
				print_r($module->getErrors());
			}

			$mitems=Menu::find()
				->where(['by_module'=>$this->module])
				->orderBy(['lvl'=>SORT_DESC])
			->all();
			if($mitems){
				foreach ($mitems as $item) {
					$item->removeNode(false);
				}
			}

			$this->downSql();
			if($this->images)
				$this->deleteImages();

			if($this->uploads_dir){
				AdminHelper::rrmdir(Yii::getAlias('@webroot').'/uploads/'.$this->module);
			}
		}
	}

	private function generateImages(){

		$folder=Yii::getAlias('@webroot').'/images/';
		if(!is_dir($folder.$this->path_images)){
			mkdir($folder.$this->path_images);
		}

		if(isset($this->images) && is_array($this->images)){
			foreach ($this->images as $field => $sizes) {
				foreach ($sizes as $path => $size_arr) {
					$size=AdminImagesSizes::findOne([
						'modules'=>$this->module,
						'field'=>$field,
						'size'=>$path,
						'width'=>$size_arr['width'],
						'heigth'=>$size_arr['heigth'],
						'method'=>$size_arr['method']
					]);
					if(!$size)
						$size=new AdminImagesSizes();

					$size->modules=$this->module;
					$size->field=$field;
					$size->size=$path;
					$size->width=$size_arr['width'];
					$size->heigth=$size_arr['heigth'];
					$size->method=$size_arr['method'];
					$size->save();

					if(!is_dir($folder.$this->path_images.'/'.$field)){
						mkdir($folder.$this->path_images.'/'.$field);
					}
					if(!is_dir($folder.$this->path_images.'/'.$field.'/'.$path)){
						mkdir($folder.$this->path_images.'/'.$field.'/'.$path);
					}

					copy($_SERVER['DOCUMENT_ROOT'].'/../modules/admin/assets/no_image.png',$folder.$this->path_images.'/'.$field.'/'.$path.'/no_image.png');
					$image = Yii::$app->image->load($folder.$this->path_images.'/'.$field.'/'.$path.'/no_image.png');
					$image->resize($size_arr['width'], $size_arr['heigth'], Image::CROP);
					$image->save($folder.$this->path_images.'/'.$field.'/'.$path.'/no_image.png');
				}
			}
		}
	}

	private function deleteImages(){
		AdminHelper::rrmdir(Yii::getAlias('@webroot').'/images/'.$this->path_images);
		AdminImagesSizes::deleteAll('modules=:module', [':module'=>$this->module]);
	}

	protected function upSql($hide_sql){
		if(!is_dir(Yii::getAlias('@app')."/modules/{$this->module}/migrations"))
			return false;
		ob_start();
		ConsoleHelper::runConsoleActionFromWebApp('migrate/up', ['migrationPath' => '@app/modules/'.$this->module.'/migrations', 'interactive' => 0]);
		$str=htmlentities(ob_get_clean(), null, Yii::$app->charset);
		if(!$hide_sql){
			echo $str;
		}
	}

	protected function downSql(){
		if(!is_dir(Yii::getAlias('@app')."/modules/{$this->module}/migrations"))
			return false;
		ob_start();
		ConsoleHelper::runConsoleActionFromWebApp('migrate/down', ['migrationPath' => '@app/modules/'.$this->module.'/migrations', 'interactive' => 0]);
		$str=htmlentities(ob_get_clean(), null, Yii::$app->charset);
		if(!$hide_sql){
			echo $str;
		}
	}

	protected function addMenuItem(){
		$items=$this->module_object->getMenuItems();

		$root=Menu::findOne(['alias' => 'admin', 'lvl'=>0]);
		$this->parseModuleMenu($items, $root);
	}

	private function parseModuleMenu($items, $root){
		if($items && count($items)){
			foreach ($items as $item) {
				$menu=new Menu();
				$post=[
					'Menu'=>[
						'by_module'=>$this->module,
						'module'=>$this->module,
						'activate_when_module'=>isset($item['activate_when_module']) ? $item['activate_when_module'] : 0,
						'name'=>$item['label'],
						'icon'=>$item['icon'],
						'url'=>$item['url'],
						'sql_count'=>$item['sql_count'],
						'sql_count_new'=>$item['sql_count_new'],
					]
				];
				$menu->load($post);
				$menu->appendTo($root);

				if(isset($item['items'])){
					$this->parseModuleMenu($item['items'], $menu);
				}
			}
		}
	}
}
