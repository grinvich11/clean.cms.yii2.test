<?php
namespace app\modules\admin\components;

use yii\web\AssetBundle;

class PjaxAssetAll extends AssetBundle
{
    public $js = [
        'js/admin/pjax.mod.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
