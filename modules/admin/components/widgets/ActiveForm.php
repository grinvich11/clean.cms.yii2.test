<?php
namespace app\modules\admin\components\widgets;

use Yii;
use yii\bootstrap\ActiveForm as ParentForm;

class ActiveForm extends ParentForm{
	public $def_options = ['role' => 'form', 'data-pjax'=>0];

	/**
     * @inheritdoc
     */
    public function init()
    {
		$this->def_options['data-pjax']=Yii::$app->params['admin_pjax_reload'];

        $this->options=\yii\helpers\ArrayHelper::merge($this->def_options, $this->options);
        parent::init();
    }
}