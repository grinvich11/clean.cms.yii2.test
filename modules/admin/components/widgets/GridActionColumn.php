<?php
namespace app\modules\admin\components\widgets;

use yii\grid\ActionColumn;
use yii\helpers\Html;
use Yii;

class GridActionColumn extends ActionColumn
{
	public $template='{update} {delete}';

	/**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

		$this->initDefaultButtons();

		if(empty($this->header))
			$this->header=Html::a('<i class="glyphicon glyphicon-remove"></i>', ['index'], ['class'=>'btn btn-mini btn-danger', 'style'=>'width: 100%;']);
		if(empty($this->contentOptions)){
			$count_buttons=substr_count($this->template, '{');
			$this->contentOptions=['style'=>($count_buttons ? 'width: '.($count_buttons*30).'px;' : '').'text-align:center;'];
		}
    }

	protected function initDefaultButtons()
    {
		//parent::initDefaultButtons();

		if (!isset($this->buttons['move'])) {
            $this->buttons['move'] = function ($url, $model, $key) {
				$options = array_merge([
                    'title' => 'Переместить',
                    'aria-label' => 'Переместить',
                    'data-pjax' => '1',
					'class' => 'sortableItem',
					'onclick' => 'return false;',
					'data-id' => $model->getPrimaryKey()
                ], $this->buttonOptions);
				return Html::a('<i class="glyphicon glyphicon-move"></i>', '', $options);
            };
        }
		if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '1',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '1',
                ], $this->buttonOptions);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
            };
        }
	}
}