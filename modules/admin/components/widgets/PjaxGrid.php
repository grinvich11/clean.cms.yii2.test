<?php
namespace app\modules\admin\components\widgets;

use yii\widgets\Pjax;
use yii\helpers\Json;
use app\modules\admin\components\PjaxAssetAll;

class PjaxGrid extends Pjax
{
	public $timeout = 0;
	public $linkSelector = false;

	public function getId($autoGenerate = true)
    {
		return 'pajax_grid';
	}

	/**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        $id = $this->options['id'];
        $this->clientOptions['push'] = $this->enablePushState;
        $this->clientOptions['replace'] = $this->enableReplaceState;
        $this->clientOptions['timeout'] = $this->timeout;
        $this->clientOptions['scrollTo'] = $this->scrollTo;
        $options = Json::htmlEncode($this->clientOptions);
        $js = '';
        if ($this->linkSelector !== false) {
            $linkSelector = Json::htmlEncode($this->linkSelector !== null ? $this->linkSelector : '#' . $id . ' a');
            $js .= "jQuery(document).pjax($linkSelector, \"#$id\", $options);";
        }
        if ($this->formSelector !== false) {
            $formSelector = Json::htmlEncode($this->formSelector !== null ? $this->formSelector : '#' . $id . ' form[data-pjax]');
			$js .= "\nif(!$('#pjax_layout').hasClass('pjax-grid-apply-js')){";
            $js .= "\n\tjQuery(document).on('submit', $formSelector, function (event) {jQuery.pjax.submit(event, '#$id', $options);});";
			$js .= "\n\t$('#pjax_layout').addClass('pjax-grid-apply-js')";
			$js .= "\n}";
        }
        $view = $this->getView();
        PjaxAssetAll::register($view);

        if ($js !== '') {
            $view->registerJs($js);
        }
    }
}