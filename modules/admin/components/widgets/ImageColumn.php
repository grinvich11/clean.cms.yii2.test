<?php
namespace app\modules\admin\components\widgets;

use Yii;
use yii\web\View;
use yii\helpers\Html;
use yii\grid\DataColumn;
use app\helpers\SiteHelper;

class ImageColumn extends DataColumn
{
    public $size = 'admin';
    public $filter = false;
	public $headerOptions=[
		'style' => 'width:40px;'
	];


    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $attribute = $this->attribute;

        return Html::a(
			Html::img('/images/'.Yii::$app->getModule(Yii::$app->controller->module->id)->path_images.'/'.$attribute.'/'.$this->size.'/'.SiteHelper::returnOneImages($model->$attribute).'?m='.$model->modified),
            '/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/update?id='.$model->id,
			[
				'data-pjax'=>1
			]
        );
    }
}
