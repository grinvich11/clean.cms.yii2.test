<?php
namespace app\modules\admin\components\widgets;

use Yii;
use yii\grid\DataColumn;

class IdColumn extends DataColumn
{
	public $headerOptions=[
		'style' => 'width:70px;'
	];
}
