<?php
namespace app\modules\admin\components\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView as ParentGridView;
use app\modules\admin\components\widgets\ActionBar;

class GridView extends ParentGridView{
	public $options=[
		//'id'=>'grid',
		'class' => 'grid-view crud-grid'
	];
	public $summary='{begin}-{end} из {totalCount}';
	public $layout='<div class=box>
		<div class=box-header></div><!-- /.box-header -->
		<div class=box-body>{items}</div><!-- /.box-body -->
		<div class="box-footer clearfix">
			<div class="row">
				<div class="col-xs-6">
					<div class="dataTables_info pull-left">{summary}</div>
				</div>
				<div class="col-xs-6">
				{pager}
				</div>
			</div>
		</div>
	  </div><!-- /.box -->';
	public $tableOptions=['class' => 'items table table-bordered table-hover'];
	public $createButton=true;
	public $settingsButton=true;
	public $exportButton=false;
	public $importButton=false;
	public $actionsAllow=true;
	public $createButtonUrlParams; //дополнительные параметры, которые можно добавить к ссылке создания, к примеру ?type

	/**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate [[columns]] objects.
     */
    public function init()
    {
		if (!isset($this->options['id'])) {
            $this->options['id'] = 'grid-'.Yii::$app->controller->module->id.'_'.str_replace('/', '_', Yii::$app->controller->id).'_'.Yii::$app->controller->action->id.'-'.uniqid();
        }
        parent::init();
	}

	/**
     * Runs the widget.
     */
    public function run()
    {
		if($this->actionsAllow)
			$templates['{bulk-actions}']=['class' => 'col-xs-3'];

		if($this->createButton){
			$buttons.='<a href="/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/create'.$this->createButtonUrlParams.'" class="btn btn-success"><span class="glyphicon glyphicon-plus" title="Добавить"></span></a>';
		}

		$buttons.='<button class="btn btn-warning" type=button onclick="$.pjax.reload({container:\'#pajax_grid\'});" title="Обновить"><span class="glyphicon glyphicon-refresh"></button>';

		if($this->settingsButton){
			$buttons.='<a href="/admin/modules/update/'.Yii::$app->controller->module->id.'" class="btn btn-primary" data-pjax=1 title="Настройки модуля"><span class="glyphicon glyphicon-wrench"></span></a>';
		}

		if($this->exportButton){
			$db_buttons.='<a href="/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/export" class="btn btn-primary" data-pjax=0 target="_blank"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>Экспорт</a>';
		}
		if($this->importButton){
			$db_buttons.='<button class="btn btn-primary" id="import-btn" data-toggle="modal" data-target="#importModal"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>Импорт</button>';
			$this->layout.='<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
		<form id="import-form" name="importForm" method="post" action="/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/import" enctype="multipart/form-data">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <h4 class="modal-title" id="importModalLabel">Импорт</h4>
			</div>
			<div class="modal-body">
				<input type="file" name="file" class="col-sm-12" required />&nbsp;
			</div>
			<div class="modal-footer">
			  <button type="submit" class="btn btn-primary">Импортировать</button>
			  <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</form>
    </div>
  </div>
</div>';
		}

		$templates[$buttons]=['class' => 'box-tools btn-group pull-right'];
		if($this->exportButton || $this->importButton){
			$templates[$db_buttons]=['class' => 'box-tools btn-group pull-right', 'style'=>'position:relative;'];
		}

		$bulkActionsItems['general-delete']='&nbsp;Удалить';
		$bulkOptions['general-delete']=[
			'url' => Url::toRoute('delete-multiple'),
			'data-confirm' => 'Вы уверены, что хотите удалить эти элементы?',
		];

		$model=Yii::$app->controller->getModelClass();
		if($model){
			$model=new $model;
			if($model->hasAttribute('published')){
				$bulkActionsItems['general-published']='&nbsp;Сменить статус публикации';
				$bulkOptions['general-published']=[
					'url' => Url::toRoute('published-multiple'),
					'data-confirm' => 'Вы уверены, что хотите сменить статус публикации этих элементов?',
				];
			}

			$action_bar=ActionBar::widget([
				'grid' => 'grid',
				'renderContainer' => false,
				'options' => ['class' => 'widget-action-bar box-header'],
				'templates' => $templates,
				'bulkActionsItems' => $bulkActionsItems,
				'bulkActionsOptions' => [
					'encode' => false,
					'class' => 'form-control',
					'options' => $bulkOptions
				]
			]);

			$this->layout=str_replace('<div class=box-header></div>', $action_bar, $this->layout);
		}
        parent::run();
    }
}