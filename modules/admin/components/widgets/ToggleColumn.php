<?php

namespace app\modules\admin\components\widgets;

use yii\helpers\Html;
use yii\web\View;
use Yii;

class ToggleColumn extends \pheme\grid\ToggleColumn
{
	public $filter=[
		'1'=>'Да',
		'0'=>'Нет'
	];

	public $hints=[
		'1'=>'On',
		'0'=>'Off'
	];

	public $headerOptions=[
		'style' => 'width:76px;'
	];
	
	public $readonly=false;

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $url = [$this->action, 'id' => $model->id, 'attribute'=>$this->attribute];

        $attribute = $this->attribute;
        $value = $model->$attribute;

        if ($value === null || $value == true) {
            $icon = 'ok';
            $title = $this->hints[0];
        } else {
            $icon = 'remove';
            $title = $this->hints[1];
        }

		if(!$this->readonly){
			return Html::a(
				'<span class="glyphicon glyphicon-' . $icon . '"></span>',
				$url,
				[
					'title' => $title,
					'class' => 'toggle-column',
					'data-method' => 'post',
					'data-pjax' => '0',
				]
			);
		}else{
			return '<span class="glyphicon glyphicon-' . $icon . '"></span>';
		}
    }

    /**
     * Registers the ajax JS
     */
    public function registerJs()
    {
        $js = <<< JS
		$("a.toggle-column").on("click", function(e) {
			e.preventDefault();
			$.get($(this).attr("href"), function(data) {
				$.pjax.reload({container:"#pajax_grid"});
			});
			return false;
		});
JS;
        $this->grid->view->registerJs($js, View::POS_READY, 'pheme-toggle-column');
    }
}
