<?php
namespace app\modules\admin\components;

use yii\base\Widget;
use yii\helpers\Html;

class JQFileUpload extends Widget
{
	public $model;
	public $attribute;
	public $value;
	public $url;
	public $htmlOptions;
	public $options;

    public function run()
    {
        //list($name, $id) = $this->resolveNameID();

        if($this->model) {
            echo Html::activeFileInput($this->model, $this->attribute, $this->htmlOptions);

			//CHtml::resolveNameID($this->model, $this->attribute, $this->htmlOptions);
			// add a hidden field so that if a model only has a file field, we can
			// still use isset($_POST[$modelClass]) to detect if the input is submitted
			/*$hiddenOptions=isset($this->htmlOptions['id']) ? array('id'=>CHtml::ID_PREFIX.$this->htmlOptions['id']) : array('id'=>false);
			echo Html::hiddenInput($this->htmlOptions['name'],  $this->value, $hiddenOptions)
				. Html::fileInput($this->model->getModelName().'['.$this->attribute.']', $this->value, $this->htmlOptions);*/
        } else {
            echo Html::fileInput($this->name, $this->value, $this->htmlOptions);
        }

        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = Html::getInputId($this->model, $this->attribute);
        }

        $this->options['url'] = $this->url;

        $options = \yii\helpers\Json::encode($this->options);

        $js = "jQuery('#{$this->htmlOptions['id']}').fileupload({$options});
		rebuildImagesJQUpload('{$this->model->formName()}','{$this->attribute}');";

        $view=$this->getView();
		$view->registerJs($js, \yii\web\View::POS_READY);
    }
}