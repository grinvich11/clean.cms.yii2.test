<?php
namespace app\modules\admin\components;

use Yii;
use yii\filters\AccessControl;
use app\components\Controller;
use app\modules\admin\models\AdminFilters;

class AdminModuleController extends Controller
{
	public $layout='admin_column';
	public $body_class='hold-transition sidebar-mini skin-blue';
	public $model;
	public $search_model;

	public function init()
	{
		if(!Yii::$app->user->isGuest || end(explode('/', $_SERVER['REQUEST_URI']))=='upload'){

		}else{
			if(Yii::$app->user->isGuest){
				$this->redirect('/admin/login');
			}else{
				$this->redirect('/');
			}
		}

		if(empty($this->model)){
			if($this->getModelClass()){
				$this->model=$this->getModelClass();
				$this->search_model=$this->getModelClass().'Search';
			}else{
				$this->model="\\app\\modules\\{$this->module->id}\\models\\".$this->module->model;
				$this->search_model="\\app\\modules\\{$this->module->id}\\models\\".$this->module->model.'Search';
			}
		}else{
			if($this->getModelClass()){
				$this->search_model=$this->getModelClass().'Search';
			}else{
				$this->model="\\app\\modules\\{$this->module->id}\\models\\".$this->model;
				$this->search_model=$this->model.'Search';
			}
		}

		if(isset($_COOKIE['body_class']))
			$this->body_class=filter_var($_COOKIE['body_class'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

		Yii::$app->params['rememberFilters']=Yii::$app->session->get('rememberFilters');
		Yii::$app->params['admin_pagination']=Yii::$app->session->get('admin_pagination');

		return parent::init();
	}

	public function getModelClass(){

	}

	public function behaviors()
    {
        return [
			'access' => [
                'class' => AccessControl::classname(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

	public function beforeAction($action)
	{
		if (parent::beforeAction($action)) {
			if(Yii::$app->params['rememberFilters']){
				$sGET=serialize($_GET);
				$not_in=['a:1:{s:5:"_pjax";s:12:"#pjax_layout";}', 'a:0:{}'];

				$controller=$action->controller->module->id.'/'.$action->controller->id.'/'.$action->id;
				if($action->id=='index' && !in_array($sGET, $not_in)){
					$filter=AdminFilters::find()->where(['user'=>Yii::$app->user->getId(),'controller'=>$controller])->one();
					if(!$filter){
						$filter=new AdminFilters();
					}
					$filter->controller=$controller;
					$filter->filter=$sGET;
					$filter->uri=Yii::$app->getRequest()->getUrl();
					$filter->save();
				}
				if(Yii::$app->getRequest()->getIsPjax()){
					if($action->id=='index'){
						$filter=AdminFilters::find()->where(['user'=>Yii::$app->user->getId(),'controller'=>$controller])->one();
						if($filter && !in_array($filter->filter, $not_in) && in_array($sGET, $not_in)){
							header('X-PJAX-URL: '.$filter->uri);
							$url=parse_url($filter->uri);
							parse_str($url['query'], $get);
							$_GET=$get;
						}
					}
				}
				if (!Yii::$app->request->isAjax) {
					if($action->id=='index'){
						$filter=AdminFilters::find()->where(['user'=>Yii::$app->user->getId(),'controller'=>$controller])->one();
						if($filter && !in_array($filter->filter, $not_in) && in_array($sGET, $not_in)){
							return $this->redirect($filter->uri);
						}
					}
				}
			}


			return true;
        }

        return false;
	}
/*
	public function actionProcessItems(){
		if($_POST['selectedItems']=='delete'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->delete();
				}
			}
		}

		if($_POST['selectedItems']=='publish'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->published=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unpublish'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->published=0;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='active'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->active=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unactive'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->active=0;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='event'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->state=1;
					$model->save();
				}
			}
		}

		if($_POST['selectedItems']=='unevent'){
			$selectedIds = $_POST['selectedIds'];
			if(count($selectedIds)>0)
			{
				foreach($selectedIds as $ids)
				{
					$model=$this->findModel($ids);
					$model->state=1;
					$model->save();
				}
			}
		}
	}*/
}
