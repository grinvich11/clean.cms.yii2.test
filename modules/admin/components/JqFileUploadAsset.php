<?php
namespace app\modules\admin\components;

use yii\web\AssetBundle;
/**
 * JqFileUploadAsset AssetBundle
 * @since 0.1
 */
class JqFileUploadAsset extends AssetBundle
{
    public $sourcePath = '@vendor/blueimp/jquery-file-upload';
    /*public $css = [
        'dist/css/AdminLTE.min.css',
        'bootstrap/css/bootstrap.min.css',
    ];*/
    public $js = [
        'js/jquery.fileupload.js',
        'js/jquery.fileupload-process.js',
        'js/jquery.fileupload-validate.js',
    ];

	public $depends = [
		'yii\web\JqueryAsset',
		'app\modules\admin\components\FancyBoxAsset',
        /*//'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',*/
    ];
}