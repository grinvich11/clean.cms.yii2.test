<?php
namespace app\modules\admin\components;

use Yii;
use yii\helpers\Console;
use yii\console\Exception;
use yii\helpers\FileHelper;
use yii\console\controllers\MigrateController as MigrateControllerParent;

class MigrateController extends MigrateControllerParent
{
	public $module_id;
	public $with_lang=FALSE;
	public $defaultMigrationPath='@app/migrations';

	public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
			$default_path = Yii::getAlias($this->defaultMigrationPath);
			if(is_array($this->migrationPath))
				$this->migrationPath = $this->migrationPath[0];
			if($this->migrationPath===$default_path){
				$this->migrationPath=Yii::getAlias('@app/modules/'.$this->module_id.'/migrations');
			}

			$path = Yii::getAlias($this->migrationPath);
			if (!is_dir($path)) {
                if ($action->id !== 'create') {
                    throw new Exception("Migration failed. Directory specified in migrationPath doesn't exist: {$this->migrationPath}");
                }
                FileHelper::createDirectory($path);
            }

            return true;
        } else {
            return false;
        }
    }

	/**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return array_merge(
            parent::options($actionID),
            ['templateFile', 'module_id'],
            ['templateFile', 'with_lang']
        );
    }

	/**
     * Creates a new migration.
     *
     * This command creates a new migration using the available migration template.
     * After using this command, developers should modify the created migration
     * skeleton by filling up the actual migration logic.
     *
     * ```
     * yii migrate/create create_user_table
     * ```
     *
     * @param string $name the name of the new migration. This should only contain
     * letters, digits and/or underscores.
     * @throws Exception if the name argument is invalid.
     */
    public function actionCreate($name)
    {
        if (!preg_match('/^\w+$/', $name)) {
            throw new Exception('The migration name should contain letters, digits and/or underscore characters only.');
        }

        $className = 'm' . gmdate('ymd_His') . '_' . $name;
        $file = $this->migrationPath . DIRECTORY_SEPARATOR . $className . '.php';

        if ($this->confirm("Create new migration '$file'?")) {
            if (preg_match('/^create_junction_(.+)_and_(.+)$/', $name, $matches)) {
                $firstTable = mb_strtolower($matches[1], Yii::$app->charset);
                $secondTable = mb_strtolower($matches[2], Yii::$app->charset);

                $content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['create_junction']), [
                    'className' => $className,
                    'table' => $firstTable . '_' . $secondTable,
                    'field_first' => $firstTable,
                    'field_second' => $secondTable,
                ]);
            } elseif (preg_match('/^add_param$/', $name, $matches)) {
				$this->parseConfigParams();
				$content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['add_param']), [
                    'className' => $className,
                    'fields' => $this->fields,
					'module_id' => $this->module_id
                ]);
            } elseif (preg_match('/^add_seopage/', $name, $matches)) {
				$this->parseConfigParams();
				$content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['add_seopage']), [
                    'className' => $className,
					'module_id' => $this->module_id
                ]);
            } elseif (preg_match('/^add_(.+)_to_(.+)$/', $name, $matches)) {
                $content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['add_column']), [
                    'className' => $className,
                    'table' => mb_strtolower($matches[2], Yii::$app->charset),
                    'fields' => $this->fields
                ]);
            } elseif (preg_match('/^drop_(.+)_from_(.+)$/', $name, $matches)) {
                $content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['drop_column']), [
                    'className' => $className,
                    'table' => mb_strtolower($matches[2], Yii::$app->charset),
                    'fields' => $this->fields
                ]);
            } elseif (preg_match('/^create_(.+)$/', $name, $matches)) {
				if($this->with_lang){
					$lang_file = $this->migrationPath . DIRECTORY_SEPARATOR . $className . '_lang.php';

					$lang_content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['create_lang']), [
						'className' => $className.'_lang',
						'table' => mb_strtolower($matches[1], Yii::$app->charset).'_lang',
						'tableRel' => mb_strtolower($matches[1], Yii::$app->charset),
						'fields' => $this->fields
					]);
					file_put_contents($lang_file, $lang_content);
					$this->stdout("New lang migration created successfully.\n", Console::FG_GREEN);
				}

                $content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['create_table']), [
                    'className' => $className,
                    'table' => mb_strtolower($matches[1], Yii::$app->charset),
                    'fields' => $this->fields
                ]);
            } elseif (preg_match('/^drop_(.+)$/', $name, $matches)) {
                $this->addDefaultPrimaryKey();
                $content = $this->renderFile(Yii::getAlias($this->generatorTemplateFiles['drop_table']), [
                    'className' => $className,
                    'table' => mb_strtolower($matches[1], Yii::$app->charset),
                    'fields' => $this->fields
                ]);
            } else {
                $content = $this->renderFile(Yii::getAlias($this->templateFile), ['className' => $className]);
            }

            file_put_contents($file, $content);
            $this->stdout("New migration created successfully.\n", Console::FG_GREEN);
        }
    }

	protected function parseConfigParams()
    {
        foreach ($this->fields as $index => $field) {
            $this->fields[$index] = ['property' => $field['property'], 'type' => str_replace('()', '', $field['decorators'])];
        }
    }
}

