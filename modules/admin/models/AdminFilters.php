<?php

namespace app\modules\admin\models;

use Yii;
use app\models\MainModel;

/**
 * This is the model class for table "admin_filters".
 *
 * @property integer $id
 * @property integer $user
 * @property string $controller
 * @property string $filter
 * @property string $uri
 * @property string $created
 * @property string $modified
 */
class AdminFilters extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_filters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller'], 'required'],
            [['user'], 'integer'],
            [['filter', 'uri'], 'string'],
            [['created', 'modified'], 'safe'],
            [['controller'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'controller' => 'Controller',
            'filter' => 'Filter',
            'uri' => 'Uri',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if($this->isNewRecord)
			{
				$this->user=Yii::$app->user->getId();
			}

			return true;
		}else{
			return false;
		}
	}
}
