<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use app\modules\admin_users\models\AdminUsers;
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends Model
{
	public $username;
	public $password;
	public $rememberMe=true;

	private $_user = false;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'username'=>'Логин или email',
			'password'=>'Пароль',
			'rememberMe'=>'Запомнить',
		);
	}

	/**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

			/*if (!$user) {
                $this->addError('login','Не правильный логин');
            }
			if (!$user->validatePassword($this->password)) {
                $this->addError('password','Не правильный пароль');
            }*/
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password','Не правильный логин или пароль');
            }
        }
    }

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	/*public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}*/

	/**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
			$user=$this->getUser();
			$login=Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
			return $login;
        } else {
            return false;
        }
    }

	/**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = AdminUsers::findByLoginOrEmail($this->username);
        }

        return $this->_user;
    }

}
