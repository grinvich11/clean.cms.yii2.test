<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\AdminModules;

/**
 * AdminModulesSearch represents the model behind the search form about `AdminModules`.
 */
class AdminModulesSearch extends AdminModules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'name', 'version', 'created', 'modified'], 'safe'],
            [['state', 'delete', 'position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminModules::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => false,
			'sort' => ['defaultOrder' => ['position' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'state' => $this->state,
            'delete' => $this->delete,
            'position' => $this->position,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'version', $this->version]);

        return $dataProvider;
    }
}
