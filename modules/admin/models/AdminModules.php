<?php

namespace app\modules\admin\models;

use Yii;
use app\models\MainModel;

/**
 * This is the model class for table "admin_modules".
 *
 * @property string $module
 * @property string $name
 * @property string $version
 * @property integer $state
 * @property integer $delete
 * @property integer $position
 * @property string $created
 * @property string $modified
 */
class AdminModules extends MainModel
{
	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'admin_modules';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['module', 'required'],
			['module', 'unique'],
			['module', 'string', 'max'=>255],
			[['state', 'delete', 'position'], 'integer'],
			[['name', 'version'], 'string', 'max'=>255],
			[['created', 'modified'], 'safe'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'module' => 'Модуль',
			'name' => 'Модуль',
			'state' => 'Состояние',
			'created' => 'Created',
			'modified' => 'Modified',
		];
	}

	public function saveParams(){
		$settings=$_POST['Config'];
		if(!empty($settings)){
			foreach ($settings as $key => $value) {
				$find=\app\models\Config::findOne($key);
				if($find){
					if($find->type=='int')
						$value=(int)$value;

					$find->value=$value;
					$find->save(false);
				}
			}
		}
	}
}
