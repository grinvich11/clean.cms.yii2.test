<?php
namespace app\modules\admin\models;
/**
 * This is the model class for table "admin_images_sizes".
 *
 * The followings are the available columns in table 'admin_images_sizes':
 * @property string $id
 * @property string $modules
 * @property string $size
 * @property integer $width
 * @property integer $heigth
 * @property string $method
 * @property string $created
 * @property string $modified
 */
class AdminImagesSizes extends \app\models\MainModel
{
	public $method_array=[
		'auto'=>'Авто',
		'crop'=>'Кроп',
		'width'=>'По ширине',
		'heigth'=>'По высоте',
		//'precise'=>'precise',
		'adapt'=>'Adapt',
	];
	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return 'admin_images_sizes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			[['modules', 'size', 'width', 'heigth', 'method', 'field'], 'required'],
			[['width', 'heigth'], 'integer'],
			[['modules', 'size', 'method'], 'string', 'max'=>255],
			[['created', 'modified'], 'safe'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'modules' => 'Module',
			'field' => 'Поле',
			'size' => 'Размер',
			'width' => 'Ширина',
			'heigth' => 'Высота',
			'method' => 'Метод',
			'created' => 'Created',
			'modified' => 'Modified',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdminImagesSizes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
