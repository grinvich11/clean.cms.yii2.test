<script>
function initialize() {
  var markers = [];
  var myLatlng = new google.maps.LatLng(<?=($model->location ? $model->location : $defaultLocation);?>);
  $('#<?=$formName;?>-location').val('<?=($model->location ? $model->location : $defaultLocation);?>');
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
	zoom: <?=($model->zoom ? $model->zoom : '14');?>,
    center: myLatlng,
	scrollwheel: false
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('<?=$formName;?>-search');

  var searchBox = new google.maps.places.SearchBox((input));

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Переместите метку',
	  draggable: true
  });

  google.maps.event.addListener(marker, 'dragend', function(event) {
	  $('#<?=$formName;?>-location').val(event.latLng.lat()+','+event.latLng.lng());
  });

  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location,
		draggable: true
      });

      markers.push(marker);

	  $('#<?=$formName;?>-location').val(place.geometry.location.lat()+','+place.geometry.location.lng());

      bounds.extend(place.geometry.location);

	  google.maps.event.addListener(marker, 'dragend', function(event) {
		  $('#<?=$formName;?>-location').val(event.latLng.lat()+','+event.latLng.lng());
	  });

	  $('#<?=$formName;?>-zoom').val(map.getZoom());
    }

    map.fitBounds(bounds);
  });
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
  google.maps.event.addListener(map, 'zoom_changed', function () {
    $('#<?=$formName;?>-zoom').val(map.getZoom());
  });
}
<?php
if(Yii::$app->getRequest()->getHeaders()->get('X-Pjax')){
	echo 'initialize();';
}else{
	echo "google.maps.event.addDomListener(window, 'load', initialize);";
}
?>
</script>
<?php echo $form->field($model,'location', ['template'=>'{input}'])->hiddenInput();?>
<?php
$model->zoom=!empty($model->zoom) ? $model->zoom : '14';
echo $form->field($model,'zoom', ['template'=>'{input}'])->hiddenInput();?>
<?php echo $form->field($model,'search',['options'=>['maxlength'=>255,'onkeypress'=>'return event.keyCode!=13']]); ?>
<br>
<div id="map-canvas"></div>
<br>