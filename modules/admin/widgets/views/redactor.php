<?php echo $form->field($model, $attribute,['wrapperOptions'=>['class'=>'col-sm-9']])->widget(\vova07\imperavi\Widget::className(), [
	'settings' => [
        'lang' => 'ru',
        'minHeight' => 150,
		'formattingTags'=>['p','h1', 'h2'],
		'buttons'=>$easy ? ['html', '|', 'bold', 'italic', 'deleted'] : ['html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|',
		'unorderedlist', 'orderedlist', '|',
		'image', 'video', 'file', 'table', 'link', '|',
		'fontcolor', 'backcolor', '|', 'alignment'],
		'imageUpload' => \yii\helpers\Url::to(['/'.\Yii::$app->controller->module->id.'/'.\Yii::$app->controller->id.'/image-upload']),
		'imageManagerJson' => \yii\helpers\Url::to(['/'.\Yii::$app->controller->module->id.'/'.\Yii::$app->controller->id.'/images-get']),
        'plugins' => $easy ? ['fontsize'] : [
			'imagemanager',
            'fontsize',
			'fontfamily',
			'fontcolor',
			'table'
        ]
    ]
]); ?>


		<?/* $this->widget('vendor.yiiext.imperavi-redactor-widget.ImperaviRedactorWidget',array(
			'model'=>$model,
			'attribute'=>$attribute,
			'options'=>array(
				'formattingTags'=>array('p','h1', 'h2'),
				'buttons'=>array('html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|',
				'unorderedlist', 'orderedlist', '|',
				'image', 'video', 'file', 'table', 'link', '|',
				'fontcolor', 'backcolor', '|', 'alignment'),
				'minHeight'=> 150,
				'lang'=>'ru',
				'fileUpload'=>Yii::app()->createUrl('admin/'.Yii::app()->controller->module->getName().'/fileUpload').'?attr='.$attribute,
				'fileUploadErrorCallback'=>new CJavaScriptExpression(
					'function(obj,json) { alert(json.error); }'
				),
				'imageUpload'=>Yii::app()->createUrl('admin/'.Yii::app()->controller->module->getName().'/imageUpload').'?attr='.$attribute,
				'imageGetJson'=>Yii::app()->createUrl('admin/'.Yii::app()->controller->module->getName().'/imageList').'?attr='.$attribute,
				'imageUploadErrorCallback'=>new CJavaScriptExpression(
					'function(obj,json) { alert(json.error); }'
				),
			),
			'plugins'=> array(
				'fontsize' => array(
					'js' => array('fontsize.js',),
				),
				'fontfamily' => array(
					'js' => array('fontfamily.js',),
				),
				'fontcolor' => array(
					'js' => array('fontcolor.js',),
				),
			),
		)); */?>