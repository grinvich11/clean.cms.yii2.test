<?
use yii\helpers\Html;
use yii\web\JsExpression;
use app\helpers\SiteHelper;
use app\modules\admin\components\JQFileUpload;
use app\modules\admin\components\JqFileUploadAsset;

JqFileUploadAsset::register($this);
?>
<?php echo Html::activeHiddenInput($model, 'id'); ?>

<div class="form-group">
	<? echo Html::activeLabel($model, $attribute, ['class'=>'col-sm-3 control-label']); ?>
	<input type="hidden" name="<?=$model->formName();?>[<?=$attribute;?>_][path]" value="<?=$path;?>">
	<div class="col-sm-9">
		<div class="row-i row-file">
			<div class="preview_container logo <?=$model->formName().'_'.$attribute;?>">
				<div class="progress">
					<div class="progress_bar">
						<div class="bar"></div>
					</div>
				</div>
				<div class="img-jqupload-body ijb-<?=$attribute;?>" data-field="<?=$attribute;?>" data-model="<?=$model->formName();?>">
				<?php
					if ($model->$attribute) {
						$images=SiteHelper::returnImagesArray($model->$attribute, true);
						if($images){
							foreach ($images as $value) {
								echo '<div class=preview>';
									echo '<div class=panel>';
										echo Html::a('','#', [
											'class' => 'delete delete-jqupload', 'data-id'=>$value
										]);
									echo '</div>';

									echo '<img src="/images/'.Yii::$app->controller->module->path_images.'/'.$path.$value.'?m='.$model->modified.'" big-src="/images/'.Yii::$app->controller->module->path_images.'/'.$attribute.'/'.$value.'?m='.$model->modified.'">';
								echo '</div>';
							}
						}
					}
				?>
				</div>
			</div>
		</div>
		<div class="file_upload">
			<a href="javascript:void(0);" class="btn-light-blue btn btn-success">Загрузить</a>
			<?php
				if($many)
					$htmlOptions=['multiple'=>'multiple'];
				else
					$htmlOptions=[];

				echo JQFileUpload::widget([
					'model' => $model,
					'attribute' => $attribute,
					'url' => '/'.Yii::$app->controller->module->id.'/'.Yii::$app->controller->id.'/jupload',
					'htmlOptions'=>$htmlOptions,
					'options' => [
						'dataType' => 'json',
						'maxFileSize'=>12000000,
						'acceptFileTypes' => new JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
						'progress' => new JsExpression('function(e, data) {
							var progress = parseInt(data.loaded / data.total * 100, 10);
							$(".preview_container.'.$model->formName().'_'.$attribute.' > .progress").show()
								.find(".progress_bar > .bar")
								.css("width", progress + "%");
						}'),
						'processalways' => new JsExpression('function(e, data) {
							var index = data.index,
								file = data.files[index];

							if (file.error) {
								alert(file.error);
							}
						}'),
						'done' => new JsExpression('function(e, data) {
							if (data.result.error === false) {
								'.($many ? '$(".preview_container.'.$model->formName().'_'.$attribute.' .img-jqupload-body").append(
										$("<div/>").attr("class", "preview").append(
											$("<div/>").attr("class", "panel").append(
												$("<a/>").attr({
													"href": "#",
													"data-id": data.result.image.id,
													"class": "delete delete-jqupload",
												})
											),
											$("<img/>").attr({"src": data.result.image.src, "big-src": data.result.image.big_src})
										)
									);
									rebuildImagesJQUpload("'.$model->formName().'","'.$attribute.'"); ' : ''
								. '
									var $preview=$(".preview_container.'.$model->formName().'_'.$attribute.' .preview"), $parent=$preview.closest(".img-jqupload-body");
									$preview.after("<input type=hidden name=\'"+$parent.attr(\'data-model\')+"[updatedimage]["+$parent.attr(\'data-field\')+"]["+$preview.find(\'a.delete\').attr(\'data-id\')+"]\' value=1>");
									$(".preview_container.'.$model->formName().'_'.$attribute.' .preview").remove();
									$(".preview_container.'.$model->formName().'_'.$attribute.' .img-jqupload-body").append(
										$("<div/>").attr("class", "preview").append(
											$("<div/>").attr("class", "panel").append(
												$("<a/>").attr({
													"href": "#",
													"data-id": data.result.image.id,
													"class": "delete delete-jqupload",
												})
											),
											$("<img/>").attr({"src": data.result.image.src, "big-src": data.result.image.big_src})
										)
									);
									rebuildImagesJQUpload("'.$model->formName().'","'.$attribute.'");').'
							} else {
								alert(data.result.message);
							}
						}'),
						'stop' => new JsExpression('function(e) {
							$(".preview_container.'.$model->formName().'_'.$attribute.' > .progress").hide();
						}')
					],
				]);
			?>
		</div>
	</div>
</div>