<?php
namespace app\modules\admin\widgets;

use yii\base\Widget;

class jqUploadWidget extends Widget
{
	public $form;
	public $model;
	public $attribute;
	public $many;
	public $path;

    public function run()
    {
		/*$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery.ui');*/

		if(!isset($this->path))
			$this->path=$this->attribute.'/admin/';

        return $this->render('jqfileupload', ['form'=>$this->form, 'model'=>$this->model, 'attribute'=>$this->attribute, 'many'=>$this->many, 'path'=>$this->path]);
    }
}