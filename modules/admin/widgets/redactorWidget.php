<?php
namespace app\modules\admin\widgets;

use yii\base\Widget;

class redactorWidget extends Widget
{
	public $form;
	public $model;
	public $attribute;
	public $label;
	public $easy=false;

    public function run()
    {
		if(!$this->label)
			$this->label=$this->model->attributeLabels()[$this->attribute];

        return $this->render('redactor', array('form'=>$this->form, 'model'=>$this->model, 'attribute'=>$this->attribute, 'label'=>$this->label, 'easy'=>$this->easy));
    }
}