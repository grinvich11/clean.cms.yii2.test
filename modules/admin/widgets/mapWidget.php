<?php
namespace app\modules\admin\widgets;

use yii\web\View;
use yii\base\Widget;
use yii\base\UnknownPropertyException;

class mapWidget extends Widget
{
	public $form;
	public $model;
	public $defaultLocation='50.44135476137398,30.5597922119141';

    public function run()
    {
		try {
			$this->model->location;
			$this->model->zoom;
			$this->model->search;
		} catch (UnknownPropertyException $exc) {
			echo '<div class="form-group"><div class="col-sm-9 col-sm-offset-3"><div class="alert alert-danger" role="alert">Not enough fields location or zoom or search for map</div></div></div>';
		}

		$formName=strtolower($this->model->formName());

		$this->view->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places',  ['position' => View::POS_BEGIN]);

		return $this->render('map_google', [
			'form'=>$this->form,
			'model'=>$this->model,
			'defaultLocation'=>$this->defaultLocation,
			'formName'=>$formName,
		]);
    }
}