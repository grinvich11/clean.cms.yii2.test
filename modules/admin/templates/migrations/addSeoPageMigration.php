<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use yii\db\Migration;

class <?= $className ?> extends Migration
{
    public $tableName='{{seo_pages}}';
    public $moduleId='<?=$module_id;?>';

    public function up()
    {
        $this->insert(
            $this->tableName,
            [
                'module' => $this->moduleId,
                'alias' => $this->moduleId.'_'.'page',
                'name' => ''
            ]
        );
    }

    public function down()
    {
        $this->delete($this->tableName, 'module=:module AND alias=:alias', [':module'=>$this->moduleId, ':alias'=>$this->moduleId.'_'.'page']);
    }
}
