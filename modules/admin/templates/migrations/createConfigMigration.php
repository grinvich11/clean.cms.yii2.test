<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use yii\db\Migration;

class <?= $className ?> extends Migration
{
    public $tableName='{{config}}';
    public $moduleId='<?=$module_id;?>';

    public function up()
    {
<?php foreach ($fields as $field): ?>
        $this->insert(
            $this->tableName,
            [
                'module' => $this->moduleId,
                'param' => $this->moduleId.'_'.'<?=$field['property'];?>',
                'value' => '',
                'default' => '',
                'label' => '',
                'type' => '<?=$field['type'];?>'
            ]
        );<?php if ($field <> end($fields))
			echo "\n\n";
		else
			echo "\n";
		?>
<?php endforeach; ?>
    }

    public function down()
    {
        $this->delete($this->tableName, 'module=:module AND param IN(<?php foreach ($fields as $field){
			echo "\"{$module_id}_{$field['property']}\"";
			if ($field <> end($fields)){
				echo ', ';
			}
		} ?>)', [':module'=>$this->moduleId]);
    }
}
