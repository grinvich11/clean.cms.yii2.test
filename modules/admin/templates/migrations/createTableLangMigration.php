<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use yii\db\Migration;

class <?= $className ?> extends Migration
{
    public $tableName='{{<?= $table ?>}}';

    public function up()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',

<?php foreach ($fields as $field): ?>
                '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . ",\n"?>
<?php endforeach; ?>

                'model_id' => 'INT(11) UNSIGNED NOT NULL',
                'language_id' => 'VARCHAR(7) NOT NULL DEFAULT "ru-RU"',

                'created' => 'DATETIME DEFAULT NULL',
                'modified' => 'DATETIME DEFAULT NULL',

                'INDEX index_language_id (language_id)',
                'INDEX index_model_id (model_id)',
                'INDEX index_model_id_language_id (model_id, language_id)',

                'CONSTRAINT fk_<?=$tableRel;?>_id_to_language_id FOREIGN KEY (language_id) REFERENCES {{language}} (locale) ON DELETE CASCADE ON UPDATE CASCADE',
                'CONSTRAINT fk_<?=$tableRel;?>_id_to_model_id FOREIGN KEY (model_id) REFERENCES {{<?=$tableRel;?>}} (id) ON DELETE CASCADE ON UPDATE CASCADE',
            ],
            'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
        );
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
