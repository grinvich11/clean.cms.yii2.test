<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
/* @var $table string the name table */
/* @var $fields array the fields */

echo "<?php\n";
?>

use yii\db\Migration;

class <?= $className ?> extends Migration
{
    public $tableName='{{<?= $table ?>}}';

    public function up()
    {
        $this->createTable($this->tableName, [
            'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
<?php foreach ($fields as $field): ?>
            '<?= $field['property'] ?>' => $this-><?= $field['decorators'] . ",\n"?>
<?php endforeach; ?>

            'created' => 'DATETIME DEFAULT NULL',
            'modified' => 'DATETIME DEFAULT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
