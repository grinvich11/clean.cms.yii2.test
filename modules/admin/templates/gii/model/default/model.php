<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\model\Generator */
/* @var $tableName string full table name */
/* @var $className string class name */
/* @var $queryClassName string query class name */
/* @var $tableSchema yii\db\TableSchema */
/* @var $labels string[] list of attribute labels (name => label) */
/* @var $rules string[] list of validation rules */
/* @var $relations array list of relations (name => relation declaration) */

$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use Yii;
use app\models\MainModel;
<?php if ($generator->hasUrl): ?>
use app\helpers\SiteHelper;
<?php if ($generator->langModel): ?>
use app\helpers\LangHelper;
<?php endif; ?>
<?php endif; ?>
<?php if ($generator->addSeo || $generator->langModel): ?>
use yii\helpers\ArrayHelper;
<?php if ($generator->addSeo): ?>
use app\modules\seo\components\MetaTagBehavior;
<?php endif; ?>
<?php if ($generator->langModel): ?>
use app\modules\language\components\MultilingualBehavior;
<?php endif; ?>
<?php endif; ?>
<?php if (!empty($relations)): ?>
<?php foreach ($relations as $name => $relation): ?>
use <?= $relation[1] . ";\n" ?>
<?php endforeach; ?>
<?php endif; ?>

/**
 * This is the model class for table "<?= $generator->generateTableName($tableName) ?>".
 *
<?php foreach ($tableSchema->columns as $column): ?>
 * @property <?= "{$column->phpType} \${$column->name}\n" ?>
<?php endforeach; ?>
<?php if (!empty($relations)): ?>
 *
<?php foreach ($relations as $name => $relation): ?>
 * @property <?= $relation[1] . ($relation[2] ? '[]' : '') . ' $' . lcfirst($name) . "\n" ?>
<?php endforeach; ?>
<?php endif; ?>
 */
class <?= $className ?> extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '<?= $generator->generateTableName($tableName) ?>';
    }
<?php if ($generator->db !== 'db'): ?>

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('<?= $generator->db ?>');
    }
<?php endif; ?>

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [<?= "\n            " . implode(",\n            ", $rules) . "\n        " ?>];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
<?php foreach ($labels as $name => $label): ?>
            <?= "'$name' => " . $generator->generateString($label) . ",\n" ?>
<?php endforeach; ?>
        ];
    }
<?php foreach ($relations as $name => $relation): ?>

    /**
     * @return \yii\db\ActiveQuery
     */
    public function get<?= $name ?>()
    {
        <?= $relation[0] . "\n" ?>
    }
<?php endforeach; ?>
<?php if ($queryClassName): ?>
<?php
    $queryClassFullName = ($generator->ns === $generator->queryNs) ? $queryClassName : '\\' . $generator->queryNs . '\\' . $queryClassName;
    echo "\n";
?>
    /**
     * @inheritdoc
     * @return <?= $queryClassFullName ?> the active query used by this AR class.
     */
    public static function find()
    {
        return new <?= $queryClassFullName ?>(get_called_class());
    }
<?php endif; ?>
<?php if ($generator->addSeo || $generator->langModel): ?>

    public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
<?php if ($generator->addSeo): ?>
				'MetaTag' => [
					'class' => MetaTagBehavior::className(),
				],
<?php endif; ?>
<?php if ($generator->langModel):
$fields=$generator->getMultiLangFields();
?>
				'ml' => [
					'class' => MultilingualBehavior::className(),
					'attributes' => [<? if($fields): ?>'<?=implode("', '", $fields);?>'<? endif; ?>]
				],
<?php endif; ?>
			]
		);
	}
<?php endif; ?>

<?php if ($generator->hasUrl): ?>
	public function getUrl(){
		return Url::to(['/<?=$tableName;?>/frontend/main/view', 'id' => $this->id, 'alias'=>SiteHelper::str2url($this-><?=$nameAttribute;?>)]);
	}
<?php endif; ?>
}
