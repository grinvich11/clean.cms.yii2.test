<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\crud\Generator */

echo $form->field($generator, 'modelClass');
echo $form->field($generator, 'moduleID');
echo $form->field($generator, 'controllerID');
echo $form->field($generator, 'generateDropDownRelation')->checkbox();
echo $form->field($generator, 'generateFront')->checkbox();
echo $form->field($generator, 'onlyFront')->checkbox();
echo $form->field($generator, 'onlyForm')->checkbox();
/*echo $form->field($generator, 'indexWidgetType')->dropDownList([
    'grid' => 'GridView',
    'list' => 'ListView',
]);*/
