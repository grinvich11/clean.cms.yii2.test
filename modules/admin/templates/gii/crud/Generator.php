<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\templates\gii\crud;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generates CRUD
 *
 * @property array $columnNames Model column names. This property is read-only.
 * @property string $controllerID The controller ID (without the module ID prefix). This property is
 * read-only.
 * @property array $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    public $modelClass;
    public $controllerClass;
    public $controllerFrontClass;
    public $viewPath;
    public $baseControllerClass = 'yii\web\Controller';
    public $indexWidgetType = 'grid';
    public $searchModelClass = '';
	public $moduleID;
	public $controllerID = 'main';
	public $generateFront = false;
	public $tableName;
	public $relations;
	public $generateDropDownRelation = false;	
	public $onlyForm = false;
	public $onlyFront = false;


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Clean CRUD Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates a controller and views that implement CRUD (Create, Read, Update, Delete)
            operations for the specified data model.';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['controllerClass', 'modelClass', 'searchModelClass', 'baseControllerClass', 'moduleID', 'controllerID'], 'filter', 'filter' => 'trim'],
            [['modelClass', 'indexWidgetType', 'moduleID', 'controllerID'], 'required'],
            [['searchModelClass'], 'compare', 'compareAttribute' => 'modelClass', 'operator' => '!==', 'message' => 'Search Model Class must not be equal to Model Class.'],
            [['modelClass', 'controllerClass', 'baseControllerClass', 'searchModelClass', 'moduleID', 'controllerID'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['modelClass'], 'validateClass', 'params' => ['extends' => BaseActiveRecord::className()]],
            [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            [['controllerClass'], 'match', 'pattern' => '/Controller$/', 'message' => 'Controller class name must be suffixed with "Controller".'],
            [['controllerClass'], 'match', 'pattern' => '/(^|\\\\)[A-Z][^\\\\]+Controller$/', 'message' => 'Controller class name must start with an uppercase letter.'],
            [['controllerClass', 'searchModelClass'], 'validateNewClass'],
            [['indexWidgetType'], 'in', 'range' => ['grid', 'list']],
            [['modelClass'], 'validateModelClass'],
            [['enableI18N', 'generateFront', 'generateDropDownRelation', 'onlyForm', 'onlyFront'], 'boolean'],
            ['viewPath', 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'modelClass' => 'Model Class',
            'controllerClass' => 'Controller Class',
            'viewPath' => 'View Path',
            'baseControllerClass' => 'Base Controller Class',
            'indexWidgetType' => 'Widget Used in Index Page',
            'searchModelClass' => 'Search Model Class',
            'generateFront' => 'Генерировать фронт',            
            'onlyForm' => 'Одна страница для редактирования',
            'onlyFront' => 'Только фронт',
            'generateDropDownRelation' => 'Генерировать select по связям(если есть)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'modelClass' => 'Например, <code>app\modules\news\models\News</code>.',
            'controllerClass' => 'This is the name of the controller class to be generated. You should
                provide a fully qualified namespaced class (e.g. <code>app\controllers\PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
            'viewPath' => 'Specify the directory for storing the view scripts for the controller. You may use path alias here, e.g.,
                <code>/var/www/basic/controllers/views/post</code>, <code>@app/views/post</code>. If not set, it will default
                to <code>@app/views/ControllerID</code>',
            'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',
            'indexWidgetType' => 'This is the widget type to be used in the index page to display list of the models.
                You may choose either <code>GridView</code> or <code>ListView</code>',
            'searchModelClass' => 'This is the name of the search model class to be generated. You should provide a fully
                qualified namespaced class name, e.g., <code>app\models\PostSearch</code>.',			
			'onlyForm' => 'Если модуль будет иметь только одну страницу, к примеру контакты, необходимо указать. Тогда в админке будет сразу открываться форма редактирования',
			'onlyFront' => 'Если необходимо сгенерировать только фронт, не трогая backend',
			'generateFront' => 'Если необходимо сгенерировать crud для фронта',
			'moduleID' => 'Например news',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['backend/controller.php'];
    }

    /**
     * @inheritdoc
     */
    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), ['baseControllerClass', 'indexWidgetType']);
    }

    /**
     * Checks if model class is valid
     */
    public function validateModelClass()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();
        if (empty($pk)) {
            $this->addError('modelClass', "The table associated with $class must have primary key(s).");
        }
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
		$this->controllerClass='app\modules\\'.$this->moduleID.'\controllers\backend\\'.ucfirst($this->controllerID).'Controller';
		$this->controllerFrontClass='app\modules\\'.$this->moduleID.'\controllers\frontend\\'.ucfirst($this->controllerID).'Controller';
		$this->searchModelClass=$this->modelClass.'Search';
		$this->viewPath='@app/modules/'.$this->moduleID.'/views/backend/'.$this->controllerID;

		$this->relations = $this->generateRelations();


        $controllerBackFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerClass, '\\')) . '.php');
        $controllerFrontFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->controllerFrontClass, '\\')) . '.php');

		if(!$this->onlyFront){
			$files = [
				new CodeFile($controllerBackFile, $this->render('backend/controller.php'))
			];
		}
		if($this->generateFront){
			$files[]=new CodeFile($controllerFrontFile, $this->render('frontend/controller.php'));
		}

        $viewPath = $this->getViewPath();
        $templatePath = $this->getTemplatePath() . '/backend/views';
		if(!$this->onlyFront){
			foreach (scandir($templatePath) as $file) {
				if (empty($this->searchModelClass) && $file === '_search.php') {
					continue;
				}

				if($this->onlyForm && $file === 'index.php'){
					continue;
				}

				if($file === 'view.php' || $file === '_search.php')
					continue;
				if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
					$files[] = new CodeFile("$viewPath/$file", $this->render("backend/views/$file"));
				}
			}
		}

		if($this->generateFront){
			$this->viewPath='@app/modules/'.$this->moduleID.'/views/frontend/'.$this->controllerID;
			$viewPath = $this->getViewPath();

			$templatePath = $this->getTemplatePath() . '/frontend/views';
			foreach (scandir($templatePath) as $file) {
				if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
					if($this->onlyForm && in_array($file, ['_view.php', 'view.php'])){
						continue;
					}
					$files[] = new CodeFile("$viewPath/$file", $this->render("frontend/views/$file"));

				}
			}
		}

        return $files;
    }

    /**
     * @return string the controller ID (without the module ID prefix)
     */
    public function getControllerID()
    {
        $pos = strrpos($this->controllerClass, '\\');
        $class = substr(substr($this->controllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string the controller view path
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pk = $class::primaryKey();

        return $pk[0];
    }

    /**
     * Generates code for active field
     * @param string $attribute
     * @return string
     */
    public function generateActiveField($attribute, $multilang=false)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model, '$attribute')->passwordInput()";
            } else {
				return "\$form->field(\$model, '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];

		if(in_array($column->name, ['image', 'images']) || (stripos($column->name, 'image') !== false)){
			return 'app\modules\admin\widgets\jqUploadWidget::widget([\'form\'=>$form, \'model\'=>$model, \'attribute\'=>\''.$attribute.'\']);';
		}
        if ($column->phpType === 'boolean' || ($column->phpType === 'integer' && $column->size == '1')) {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        } elseif ($column->type === 'text') {
			if(in_array($attribute, ['preview'])){
				if(!$multilang)
					return "\$form->field(\$model, '$attribute')->textarea(['rows' => 3])";
				else
					return "\$form->field(\$model,'".$attribute."_'.\$lang->locale,['labelOptions'=>['label'=>\$model->getAttributeLabel('$attribute').'_'.\$lang->locale]])->textarea(['rows' => 3]);";
			}else{
				if(!$multilang)
					return "app\modules\admin\widgets\\redactorWidget::widget(['form'=>\$form, 'model'=>\$model, 'attribute'=>'$attribute']);";
				else
					return "app\modules\admin\widgets\\redactorWidget::widget(['form'=>\$form, 'model'=>\$model, 'attribute'=>'$attribute'.'_'.\$lang->locale, 'label'=>\$model->getAttributeLabel('$attribute').'_'.\$lang->locale]);";
			}
        } elseif ($column->type === 'datetime' || $column->type === 'date') {
            //return "\$form->field(\$model, '$attribute')->textarea(['rows' => 6])";
			return '$form->field($model, \''.$column->name.'\',[\'wrapperOptions\'=>[\'class\'=>\'col-sm-3\']])->widget(
		dosamigos\datetimepicker\DateTimePicker::className(), [
		   \'language\' => \'ru\',
		   \'clientOptions\' => [
				\'autoclose\' => true,
				\'format\' => \'yyyy-mm-dd'.($column->type === 'datetime' ? ' HH:ii' : '').'\',
				\'todayBtn\' => true,'.($column->type === 'date' ? "\n\t\t\t\t".'\'minView\' => 2,' : '').'
		   ]
	]);';
			//.($column->type === 'datetime' ? ' HH:ii' : '')
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "\$form->field(\$model, '$attribute')->dropDownList("
                    . preg_replace("/\n\s*/", ' ', VarDumper::export($dropDownOptions)).", ['prompt' => ''])";
            } elseif ($this->generateDropDownRelation && isset($this->relations[$attribute])) {
				return "\$form->field(\$model, '$attribute', ['options'=>['class'=>'form-group']])->dropDownList(\yii\helpers\ArrayHelper::map("
                    . $this->relations[$attribute]."::find()->orderBy(['position'=>SORT_ASC])->all(), 'id', 'name'))";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
				if(!$multilang)
					return "\$form->field(\$model, '$attribute')->$input()";
				else
					return "\$form->field(\$model,'".$attribute."_'.\$lang->locale,['labelOptions'=>['label'=>\$model->getAttributeLabel('$attribute').'_'.\$lang->locale]])->$input();";
            } else {
				if(!$multilang)
					return "\$form->field(\$model, '$attribute')->$input(['maxlength' => true])";
				else
					return "\$form->field(\$model,'".$attribute."_'.\$lang->locale,['labelOptions'=>['label'=>\$model->getAttributeLabel('$attribute').'_'.\$lang->locale]])->$input(['maxlength' => true]);";
            }
        }
    }

    /**
     * Generates code for active search field
     * @param string $attribute
     * @return string
     */
    public function generateActiveSearchField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false) {
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        } else {
            return "\$form->field(\$model, '$attribute')";
        }
    }

    /**
     * Generates column format
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
		if ($column->phpType === 'boolean' || ($column->phpType === 'integer' && $column->size == '1')) {
            return 'boolean';
		} elseif (stripos($column->name, 'image') !== false) {
            return 'images';
        } elseif ($column->type === 'text') {
            return 'ntext';
        } elseif (stripos($column->name, 'time') !== false && $column->phpType === 'integer') {
            return 'datetime';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'text';
        }
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][] = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        return $rules;
    }

    /**
     * @return array searchable attributes
     */
    public function getSearchAttributes()
    {
        return $this->getColumnNames();
    }

    /**
     * Generates the attribute labels for the search model.
     * @return array the generated attribute labels (name => label)
     */
    public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        $model = new $this->modelClass();
        $attributeLabels = $model->attributeLabels();
        $labels = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'ID';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                        $label = substr($label, 0, -3) . ' ID';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['like', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode("\n" . str_repeat(' ', 12), $hashConditions)
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode("\n" . str_repeat(' ', 12), $likeConditions) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates URL parameters
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                return "'id' => (string)\$model->{$pks[0]}";
            } else {
                return "'id' => \$model->{$pks[0]}";
            }
        } else {
            $params = [];
            foreach ($pks as $pk) {
                if (is_subclass_of($class, 'yii\mongodb\ActiveRecord')) {
                    $params[] = "'$pk' => (string)\$model->$pk";
                } else {
                    $params[] = "'$pk' => \$model->$pk";
                }
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks);
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema();
        } else {
            return false;
        }
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $class ActiveRecord */
        $class = $this->modelClass;
        if (is_subclass_of($class, 'yii\db\ActiveRecord')) {
            return $class::getTableSchema()->getColumnNames();
        } else {
            /* @var $model \yii\base\Model */
            $model = new $class();

            return $model->attributes();
        }
    }

	public function generateAttribute($column, $format, $comment=false){
		if($format=='images')
			return "            ".($comment ? "/*" : "")."[
				'class' => '\app\modules\admin\components\widgets\ImageColumn',
				'attribute' => '".$column->name."',
			],".($comment ? "*/" : "")."\n";
		elseif($format=='boolean'){
			return "            ".($comment ? "/*" : "")."[
				'class' => '\app\modules\admin\components\widgets\ToggleColumn',
				'attribute' => '".$column->name."',
			],".($comment ? "*/" : "")."\n";
		}elseif($column->name=='id'){
			return "            ".($comment ? "/*" : "")."[
				'class' => '\app\modules\admin\components\widgets\IdColumn',
				'attribute' => '".$column->name."',
			],".($comment ? "*/" : "")."\n";
		}else
			return "            ".($comment ? "// " : "")."'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
	}




	/**
     * @return array the generated relation declarations
     */
    protected function generateRelations()
    {
		$class = $this->modelClass;
		$this->tableName=$class::tableName();

        $db = $this->getDbConnection();

        $schema = $db->getSchema();
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }

        $relations = [];
        foreach ($schemaNames as $schemaName) {
            foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                foreach ($table->foreignKeys as $refs) {
                    $refTable = $refs[0];
                    unset($refs[0]);
                    $refClassName = $this->generateClassName($refTable);

                    // Add relation for this table
                    $link = $this->generateRelationLink($refs);
                    $relations[$link] = $refClassName;
                }
            }
        }

        return $relations;
    }

	/**
     * @return Connection the DB connection as specified by [[db]].
     */
    protected function getDbConnection()
    {
        return Yii::$app->get('db', false);
    }

	/**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {
       /* if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }*/

        $schemaName = '';
        $fullTableName = $tableName;
        if (($pos = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db = $this->getDbConnection();
        $patterns = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        if (strpos($this->tableName, '*') !== false) {
            $pattern = $this->tableName;
            if (($pos = strrpos($pattern, '.')) !== false) {
                $pattern = substr($pattern, $pos + 1);
            }
            $patterns[] = '/^' . str_replace('*', '(\w+)', $pattern) . '$/';
        }
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return Inflector::id2camel($schemaName.$className, '_');
    }

	/**
     * Generates the link parameter to be used in generating the relation declaration.
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = $a;
        }

        return implode(', ', $pairs);
    }


	public function getMultiLangFields(){
		$fields=Yii::$app->db->createCommand("show columns from ".$this->tableName.'_lang')->queryColumn();
		if($fields){
			foreach ($fields as $key=>$value) {
				if(in_array($value, ['id', 'model_id', 'language_id', 'created', 'modified'])){
					unset($fields[$key]);
				}
			}
			return $fields;
		}
	}
}
