<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$behaviors=$model->behaviors();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
<? if(isset($behaviors['MetaTag'])) echo "use app\modules\seo\widgets\MetaTags;\n"; ?>
<? if(isset($behaviors['ml'])) echo "use app\helpers\LangHelper;\n"; ?>

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
	],
]); ?>
	<?= "<?" ?> $this->beginBlock('base'); ?>

<?php
if(isset($behaviors['ml'])):
	$fields=$generator->getMultiLangFields();

	echo "\t<?";
	echo " \$languages = LangHelper::getLangModels();\n";
	echo "\tif(\$languages){\n";
		echo "\t\tforeach (\$languages as \$key=>\$lang) {\n";
			echo "\t\t\t\$this->beginBlock('lang_'.\$lang->alias);\n";
				if($fields){
					foreach ($fields as $field) {
						if (in_array($field, $safeAttributes) && !in_array($field, ['created', 'modified', 'position'])) {
							echo "\t\t\t\techo " . $generator->generateActiveField($field, true) . "\n\n";
						}
					}
				}
				//echo "\t\t\t\techo \$form->field(\$model,'name_'.\$lang->locale,['labelOptions'=>['label'=>\$model->getAttributeLabel('name').'_'.\$lang->locale]]);\n";
				//echo "\t\t\t\techo app\modules\admin\widgets\redactorWidget::widget(['form'=>\$form, 'model'=>\$model, 'attribute'=>'text_'.\$lang->locale, 'label'=>\$model->getAttributeLabel('text').'_'.\$lang->locale]);\n";

			echo "\t\t\t\$this->endBlock();\n";

			echo "\t\t\t\$lang_tab[]=['label'=>\$lang->name, 'content'=>\$this->blocks['lang_'.\$lang->alias], 'active'=>\$key==0];\n";
		echo "\t\t}\n\n";

		echo "\t\techo '<div class=\"nav-tabs-custom\">';\n";
			echo "\t\t\techo Tabs::widget([\n";
				echo "\t\t\t\t'items'=>\$lang_tab,\n";
			echo "\t\t\t]);\n";
		echo "\t\techo '</div>';\n";
	echo "\t} ?>\n";
endif; ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
	if(isset($fields) && array_search($attribute, $fields)!==false)
		continue;

    if (in_array($attribute, $safeAttributes) && !in_array($attribute, ['created', 'modified', 'position'])) {
		echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>
    <div class="box-footer">
        <?= "<?= " ?>Html::submitButton($model->isNewRecord ? <?= $generator->generateString('Добавить') ?> : <?= $generator->generateString('Сохранить') ?>, ['class' => 'btn btn-primary']) ?>
<? if(!$generator->onlyForm): ?>
		<a class="btn btn-default btn-link" href="/<?= "<?=" ?>Yii::$app->controller->module->id;?>/<?= "<?=" ?>Yii::$app->controller->id;?>">К списку</a>
<? endif; ?>
    </div>

	<?= "<?" ?> $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?= "<?= " ?>Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true],
<? if(isset($behaviors['MetaTag'])) echo "\t\t\t\t".'[\'label\'=>\'SEO\', \'content\'=>  MetaTags::widget([\'model\' => $model,\'form\' => $form])],'."\n"; ?>
			]
		]);
		?>
		</div>
	</div>

<?= "<?php " ?>ActiveForm::end(); ?>
