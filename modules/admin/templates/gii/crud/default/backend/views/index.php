<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>
use yii\helpers\Html;
use app\modules\admin\components\widgets\PjaxGrid;
use <?= $generator->indexWidgetType === 'grid' ? "app\\modules\\admin\\components\\widgets\\GridView" : "yii\\widgets\\ListView" ?>;

PjaxGrid::begin();
<?php if(!empty($generator->searchModelClass) && 0): ?>
<?= ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]);
<?php endif; ?>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
			[
				'class' => 'yii\grid\CheckboxColumn',
			],
<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            // '" . $name . "',\n";
        }
    }
} else {
	$count_boolean = 0;
	$count_images = 0;
	$boolean='';
	$images='';
	foreach ($tableSchema->columns as $column) {
		$format = $generator->generateColumnFormat($column);
		if ($column->phpType === 'boolean' || ($column->phpType === 'integer' && $column->size == '1')) {
			if (++$count_boolean < 3) {
				$boolean.=$generator->generateAttribute($column, 'boolean');
				unset($tableSchema->columns[$column->name]);
				++$count;
			}
		}

		if ($format == 'images') {
			if (++$count_images < 3) {
				$images.=$generator->generateAttribute($column, 'images');
				unset($tableSchema->columns[$column->name]);
				++$count;
			}
		}
	}
    foreach ($tableSchema->columns as $column) {
		if(in_array($column->name, ['position', 'modified']))
			continue;
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo $generator->generateAttribute($column, $format);
        } else {
            echo $generator->generateAttribute($column, $format, true);
        }
    }

	echo $images;
	echo $boolean;
}
?>
            [
				'class' => 'app\modules\admin\components\widgets\GridActionColumn',
<? if(isset($tableSchema->columns['position'])){
	echo "				'template' => '{move} {update} {delete}'\n";
} ?>
			],
        ],
    ]);
<?php else: ?>
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ])
<?php endif; ?>

PjaxGrid::end();
