<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use app\modules\admin\components\CrudAdminModuleController;

class <?= $controllerClass ?> extends CrudAdminModuleController
{
<?
	if($generator->onlyForm)
		echo '	public $onlyForm=true;'."\n\n";
?>
	public function getModelClass(){
		return $this->model='<?= ltrim($generator->modelClass, '\\') ?>';
	}
}
