<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

if(!$generator->onlyForm):
echo "<?php\n";
?>
use yii\helpers\Html;
use app\components\ListView;
?>

<h1><?=Yii::$app->getModule($generator->moduleID)->name;?></h1>
<br />

<?php echo "<?php\n"; ?>
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    ]);
?>
<? else:


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['breadcrumbs']=[
	'<?=Yii::$app->getModule($generator->moduleID)->name;?>',
];
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">

    <h1><?= "<?= " ?>Html::encode($model-><?=$generator->getNameAttribute();?>) ?></h1>

    <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "            '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
    }
}
?>
        ],
    ]) ?>

</div>
<? endif; ?>