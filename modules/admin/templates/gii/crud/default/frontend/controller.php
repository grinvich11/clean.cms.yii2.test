<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\helpers\StringHelper;
use yii\helpers\Inflector;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerFrontClass);
$tableSchema = $generator->getTableSchema();
$columns = $tableSchema->columns;

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerFrontClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
use app\components\FrontController;
<? if(!$generator->onlyForm): ?>
use yii\data\ActiveDataProvider;
<? endif; ?>

class <?= $controllerClass ?> extends FrontController
{
	public function actionIndex()
	{
<? if(!$generator->onlyForm): ?>
		//\app\helpers\SeoHelper::registerSeoPage('<?=$generator->moduleID;?>_page');

		$dataProvider = new ActiveDataProvider([
            'query' => <?= StringHelper::basename($generator->modelClass) ?>::find()<? if(isset($tableSchema->columns['published'])) echo '->where([\'published\'=>1])'; ?>,
            'pagination' => [
                'pageSize' => Yii::$app->params['<?=$generator->moduleID;?>_pagination'],
				'pageParam'=>'p',
				'pageSizeParam'=>'pp'
            ],
<? if(isset($columns['position'])):?>
			'sort' => ['defaultOrder' => ['position' => SORT_ASC]]
<? elseif(isset($columns['id'])): ?>
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
<? endif; ?>
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
<? else: ?>
		$model=$this->findModel();
		Yii::$app->metaTags->register($model);

        return $this->render('index', [
            'model' => $model
        ]);
<? endif; ?>
	}

<? if(!$generator->onlyForm): ?>
	public function actionView()
	{
		$model=$this->findModel($_GET['id']);
		Yii::$app->metaTags->register($model);

		return $this->render('view', [
			'model'=>$model,
		]);
	}

<? endif; ?>
<? if($generator->onlyForm): ?>
	protected function findModel()
	{
		$model=<?= StringHelper::basename($generator->modelClass) ?>::find()->one();
		if($model===null)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
<? else: ?>
	protected function findModel($id)
	{
		$model=<?= StringHelper::basename($generator->modelClass) ?>::findOne($id);
		if($model===null<? if(isset($columns['published'])) echo ' || $model->published==0'; ?>)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
<? endif; ?>
}
