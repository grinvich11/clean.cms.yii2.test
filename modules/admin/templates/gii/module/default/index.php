<?php
echo "<?php\n";
?>
use yii\widgets\Pjax;
use app\modules\admin\components\widgets\GridView;
use yii\helpers\Html;

Pjax::begin(['id' => 'pajax_grid']);
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		'id',
		[
            'class' => 'app\modules\admin\components\widgets\GridActionColumn',
        ],
	]
]);
Pjax::end();