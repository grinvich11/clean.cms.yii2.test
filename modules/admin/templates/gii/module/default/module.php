<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\module\Generator */

$className = $generator->moduleClass;
$pos = strrpos($className, '\\');
$ns = ltrim(substr($className, 0, $pos), '\\');
$className = substr($className, $pos + 1);

echo "<?php\n";
?>

namespace <?= $ns ?>;

use Yii;
use app\components\Module as ParentModule;

class <?= $className ?> extends ParentModule
{
	public $version='1';
	public $name='<?= $generator->moduleName ?>';
<? if($generator->mainModel): ?>
	public $model='<?= $generator->mainModel ?>';
<? endif; ?>
<? if($generator->uploadsDir || $generator->hasImages): ?>

	public $install=[
<? if($generator->uploadsDir): ?>
		'uploads_dir'=>true,
<? endif; ?>
<? if($generator->hasImages): ?>
		'images'=>[
			'images'=>[
				'admin'=>[
					'width'=>88,
					'heigth'=>88,
					'method'=>'auto'
				],
			]
		]
<? endif; ?>
    ];
<? endif; ?>
<? if($generator->hasMenuItems): ?>

    public function getMenuItems(){
        return [
			[
				'label' => '<?= $generator->moduleName ?>',
				'icon' => 'glyphicon glyphicon-list',
				'url' => '/<?= $generator->moduleID ?>/backend/main',
				'active' => Yii::$app->controller->module->id=='<?= $generator->moduleID ?>',
			]
        ];
    }
<? endif; ?>
<? if($generator->hasRules): ?>

    public static function rules(){
        return [
			'<?= $generator->moduleID ?>'=>'<?= $generator->moduleID ?>/frontend/main/index',
        ];
    }
<? endif; ?>

}
