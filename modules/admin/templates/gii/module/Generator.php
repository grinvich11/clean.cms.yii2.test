<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\templates\gii\module;

use yii\gii\CodeFile;
use yii\helpers\Html;
use Yii;
use yii\helpers\StringHelper;

/**
 * This generator will generate the skeleton code needed by a module.
 *
 * @property string $controllerNamespace The controller namespace of the module. This property is read-only.
 * @property boolean $modulePath The directory that contains the module class. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{
    public $moduleClass;
    public $moduleID;
    public $moduleName;
	public $hasRules = true;
	public $hasMenuItems = true;
	public $uploadsDir = false;
	public $hasImages = false;
	public $mainModel;


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return Yii::t('gii', 'Clean Module Generator');
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return Yii::t('gii', 'This generator helps you to generate the skeleton code needed by a Yii clean module.');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['moduleID', 'moduleName', 'mainModel'], 'filter', 'filter' => 'trim'],
            [['moduleID', 'moduleName'], 'required'],
            [['moduleID', 'mainModel'], 'match', 'pattern' => '/^[\w\\-]+$/', 'message' => 'Only word characters and dashes are allowed.'],
			[['hasRules', 'hasMenuItems', 'uploadsDir', 'hasImages'], 'boolean'],
            //[['moduleClass'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            //[['moduleClass'], 'validateModuleClass'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'moduleID' => Yii::t('gii', 'Module ID'),
            'moduleClass' => 'Module Class',
            'moduleName' => Yii::t('gii', 'Module Name'),
            'hasRules' => Yii::t('gii', 'Whether the rules in the module'),
            'hasMenuItems' => Yii::t('gii', 'Has menu admin items?'),
            'uploadsDir' => Yii::t('gii', 'Has upload dir?'),
            'hasImages' => Yii::t('gii', 'Has images?'),
            'mainModel' => Yii::t('gii', 'Main model module'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function hints()
    {
        return [
            'moduleID' => 'Например news',
            'moduleName' => 'Просто название для админки. Например "Новости"',
            'moduleClass' => 'This is the fully qualified class name of the module, e.g., <code>app\modules\admin\Module</code>.',
            'hasMenuItems' => 'Если в админке будет роутинг, добавит нужный метод.',
            'hasRules' => 'Если во фронте будет роутинг, добавит нужный метод.'."\n".'public static function rules()
    {
        return [

        ];
    }',
			'uploadsDir' => Yii::t('gii', 'If model has text field with images upload'),
			'hasImages' => 'Если у модели будут картинки, то добавит массив для конфигурации картинок, который необходимо заполнить перед установкой',
        ];
    }

    /**
     * @inheritdoc
     */
    public function successMessage()
    {
        if (Yii::$app->hasModule($this->moduleID)) {
            $link = Html::a('try it now', Yii::$app->getUrlManager()->createUrl($this->moduleID), ['target' => '_blank']);
			// You may $link.
            return Yii::t('gii', 'The module has been generated successfully.');
        }

        $output = <<<EOD
<p>The module has been generated successfully.</p>
EOD;
		$code = <<<EOD
./yii migrate/create create_{$this->moduleID} --module_id={$this->moduleID}

EOD;

        return $output . '<pre>' . highlight_string($code, true) . '</pre>';
    }

    /**
     * @inheritdoc
     */
    public function requiredTemplates()
    {
        return ['module.php', 'controller.php', 'index.php'];
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
		$this->moduleClass='app\modules\\'.$this->moduleID.'\Module';

        $files = [];
        $modulePath = $this->getModulePath();
        $files[] = new CodeFile(
            $modulePath . '/' . StringHelper::basename($this->moduleClass) . '.php',
            $this->render("module.php")
        );
        /*$files[] = new CodeFile(
            $modulePath . '/controllers/backend/MainController.php',
            $this->render("controller.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/views/backend/main/index.php',
            $this->render("index.php")
        );*/

        return $files;
    }

    /**
     * Validates [[moduleClass]] to make sure it is a fully qualified class name.
     */
    public function validateModuleClass()
    {
        if (strpos($this->moduleClass, '\\') === false || Yii::getAlias('@' . str_replace('\\', '/', $this->moduleClass), false) === false) {
            $this->addError('moduleClass', 'Module class must be properly namespaced.');
        }
        if (empty($this->moduleClass) || substr_compare($this->moduleClass, '\\', -1, 1) === 0) {
            $this->addError('moduleClass', 'Module class name must not be empty. Please enter a fully qualified class name. e.g. "app\\modules\\admin\\Module".');
        }
    }

    /**
     * @return boolean the directory that contains the module class
     */
    public function getModulePath()
    {
        return Yii::getAlias('@' . str_replace('\\', '/', substr($this->moduleClass, 0, strrpos($this->moduleClass, '\\'))));
    }

    /**
     * @return string the controller namespace of the module.
     */
    public function getControllerNamespace()
    {
        return substr($this->moduleClass, 0, strrpos($this->moduleClass, '\\')) . '\controllers\backend';
    }
}
