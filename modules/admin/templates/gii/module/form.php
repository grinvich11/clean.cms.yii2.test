<?php
/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\module\Generator */

?>
<div class="module-form">
<?php
    echo $form->field($generator, 'moduleID');
    echo $form->field($generator, 'moduleName');
    //echo $form->field($generator, 'mainModel');
    echo $form->field($generator, 'hasRules')->checkbox();
    echo $form->field($generator, 'hasMenuItems')->checkbox();
    echo $form->field($generator, 'uploadsDir')->checkbox();
    echo $form->field($generator, 'hasImages')->checkbox();
?>
</div>
