<?php
use yii\helpers\Html;

$this->params['breadcrumbs']=[
	'Контакты',
];
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?v=3.exp',  ['position' => $this::POS_BEGIN]);
?>
<div class="container">

    <h1><?= Html::encode($model->id) ?></h1>

    <div class="row">
		<div class="col-xs-8">
			<div class="b-contact-data">
				<h2>Контактная информация</h2>
				<p>Адрес: <?=$model->address;?></p>
				<p>Телефон: <?=$model->phone;?></p>
				<p>E-mail: <?=$model->email;?></p>
			</div>
		</div>
		<div class="col-xs-4">
			<h2>На карте</h2>
			<script>
			function initialize_map() {
			  var myLatlng = new google.maps.LatLng(<?=$model->location;?>);
			  var mapOptions = {
				zoom: <?=$model->zoom;?>,
				scrollwheel: false,
				center: myLatlng,
				disableDefaultUI: true,
				zoomControl: true
			  }
			  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

			  var marker = new google.maps.Marker({
				  position: myLatlng,
				  map: map,
				  title: '<?=Html::encode($model->address);?>',
				  draggable: false
			  });

			}
			google.maps.event.addDomListener(window, 'load', initialize_map);
			</script>
			<div class="b-map" style="width: 317px; height: 368px;" id="map-canvas"></div>
		</div>
	</div>

</div>
