<?php
use yii\helpers\Html;
use app\modules\admin\components\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use app\modules\seo\widgets\MetaTags;

$form = ActiveForm::begin([
	'id'=>'form',
	'enableClientValidation'=>true,
	'enableAjaxValidation'=>false,
	'validateOnSubmit'=>true,
	'layout'=>'horizontal',
	'options'=>[
		'enctype'=>'multipart/form-data',
		'autocomplete'=>'off',
	],
]); ?>
	<? $this->beginBlock('base'); ?>


    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= app\modules\admin\widgets\mapWidget::widget(['form'=>$form, 'model'=>$model]); ?>

    <div class="box-footer">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

	<? $this->endBlock(); ?>

	<div class="box-body">
		<div class="nav-tabs-custom">
		<?= Tabs::widget([
			'items'=>[
				['label'=>'Основные', 'content'=>$this->blocks['base'], 'active'=>true],
				['label'=>'SEO', 'content'=>  MetaTags::widget(['model' => $model,'form' => $form])],
			]
		]);
		?>
		</div>
	</div>

<?php ActiveForm::end(); ?>
