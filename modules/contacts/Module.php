<?php
namespace app\modules\contacts;

use Yii;

class Module extends \app\components\Module
{
	public $version='1';
	public $name='Контакты';

	public function getMenuItems(){
		return [
			[
				'label' => 'Контакты',
				'icon' => 'fa fa-map-marker',
				'url' => '/admin/contacts',
				'active' => Yii::$app->controller->module->id=='contacts',
			]
        ];
	}


	public static function rules()
    {
        return [
            'contacts'=>'contacts/frontend/main/index',
        ];
    }
}
