<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_131609_create_table_contacts extends Migration
{
    public $tableName='{{contacts}}';

	public function safeUp()
	{
		$this->createTable(
			$this->tableName,
			[
				'id' => 'TINYINT(3) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',

				'phone' => 'VARCHAR(255) NULL COMMENT "Телефон"',
				'email' => 'VARCHAR(255) NOT NULL COMMENT "Email"',
				'address' => 'VARCHAR(255) NULL COMMENT "Адрес"',
				'search' => 'VARCHAR(255) NULL COMMENT "Поиск"',

				'location' => 'VARCHAR(255) NULL COMMENT "Место на карте"',
				'zoom' => 'VARCHAR(255) NOT NULL DEFAULT "14" COMMENT "Зум"',

				'created' => 'DATETIME DEFAULT NULL',
				'modified' => 'DATETIME DEFAULT NULL',
			],
			'ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci'
		);
	}

	public function safeDown()
	{
		$this->dropTable($this->tableName);
	}
}
