<?php

namespace app\modules\contacts\models;

use Yii;
use app\models\MainModel;
use yii\helpers\ArrayHelper;
use app\modules\seo\components\MetaTagBehavior;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $search
 * @property string $location
 * @property string $zoom
 * @property string $created
 * @property string $modified
 */
class Contacts extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['created', 'modified'], 'safe'],
            [['phone', 'email', 'address', 'search', 'location', 'zoom'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'email' => 'Email',
            'address' => 'Адрес',
            'search' => 'Поиск',
            'location' => 'Место на карте',
            'zoom' => 'Зум',
            'created' => 'Created',
            'modified' => 'Modified',
        ];
    }

    public function behaviors()
	{
		return ArrayHelper::merge(
			parent::behaviors(),
			[
				'MetaTag' => [
					'class' => MetaTagBehavior::className(),
				],
			]
		);
	}

}
