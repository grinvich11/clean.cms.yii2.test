<?php
namespace app\modules\contacts\widgets;

use yii\base\Widget;
use app\modules\contacts\models\Contacts;

class contactsWidget extends Widget {

    public function run() {
        $model = Contacts::find()->one();

		if($model)
			return $this->render('contacts', ['model' => $model]);
    }

}
