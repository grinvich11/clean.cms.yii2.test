<?php

namespace app\modules\contacts\controllers\frontend;

use Yii;
use app\modules\contacts\models\Contacts;
use app\components\FrontController;
use yii\data\ActiveDataProvider;

class MainController extends FrontController
{
	public function actionIndex()
	{
		//\app\helpers\SeoHelper::registerSeoPage('contacts_page');

		$dataProvider = new ActiveDataProvider([
            'query' => Contacts::find(),
            'pagination' => [
                'pageSize' => Yii::$app->params['contacts_pagination'],
				'pageParam'=>'p',
				'pageSizeParam'=>'pp'
            ],
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
	}

	public function actionView()
	{
		$model=$this->findModel($_GET['id']);
		Yii::$app->metaTags->register($model);

		return $this->render('view', [
			'model'=>$model,
		]);
	}

	protected function findModel($id)
	{
		$model=Contacts::findOne($id);
		if($model===null)
			throw new NotFoundHttpException('The requested page does not exist.');
		return $model;
	}
}
