<?php

namespace app\modules\contacts\controllers\backend;

use app\modules\admin\components\CrudAdminModuleController;

class MainController extends CrudAdminModuleController
{
	public $onlyForm=true;

	public function getModelClass(){
		return $this->model='app\modules\contacts\models\Contacts';
	}
}
