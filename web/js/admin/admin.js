(function($){$.fn.extend({confirmModal:function(options){var html='<div class="modal" id="confirmContainer"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><a class="close" data-dismiss="modal">×</a>'+'<h3>#Heading#</h3></div><div class="modal-body">'+'#Body#</div><div class="modal-footer">'+'<a href="javascript: void(0);" class="btn btn-primary" id="confirmYesBtn">#Confirm#</a>'+'<a href="javascript: void(0);" class="btn btn-default" data-dismiss="modal">#Close#</a></div></div></div></div>';var defaults={heading:'Please confirm',body:'Body contents',confirmButton:'Да',closeButton:'Нет',callback:null};var options=$.extend(defaults,options);html=html.replace('#Heading#',options.heading).replace('#Body#',options.body).replace('#Confirm#',options.confirmButton).replace('#Close#',options.closeButton);$(this).html(html);$(this).modal('show');var context=$(this);$('#confirmYesBtn',this).click(function(){if(options.callback!=null)
options.callback();$(context).modal('hide');});}});})(jQuery);

jQuery(function($) {
	$('body').on('click','#cbcwr_clear',function(){
		window.location.href=$('#cleared_url').val();
	});

	$('body').on('click','.not-menu',function(){
		saveMenuState('hide');
		$('#main').animate({'width':$('.col-lg-12').css('width')},{duration: 400, complete: function() {
			$('#main').removeClass('col-lg-9').addClass('col-lg-12');
			$('.not-menu-active').css({'display':'block'});
		}});
		$('.sidebar-block').animate({'width':0},{duration: 300, complete: function() {
			$('.sidebar-block').css({'display':'none'});
		}});
		return false;
	});

	$('body').on('click','.not-menu-active',function(){
		saveMenuState('show');
		$('#main').animate({'width':$('.col-lg-9').css('width')},{duration: 400, complete: function() {
			$('#main').removeClass('col-lg-12').addClass('col-lg-9');
			$('.not-menu-active').hide();
		}});
		$('.sidebar-block').animate({'width':$('.col-lg-3').css('width')},{duration: 300, complete: function() {
			$('.sidebar-block').css({'display':'block'});
		}});
		return false;
	});

	$('body').on('click','.delete-jqupload', function(){
		var $this=$(this),$parent=$(this).closest('.img-jqupload-body');
		if(confirm('Удалить?')) {
			$this.closest('.preview').after('<input type=hidden name='+$parent.attr('data-model')+'[updatedimage]['+$parent.attr('data-field')+']['+$this.attr('data-id')+']  value=1>');
			$this.closest('.preview').remove();
			rebuildImagesJQUpload($parent.attr('data-model'),$parent.attr('data-field'));
		}
		return false;
	});

	$('body').on('click','.no-print ul.list-unstyled li a', function(){
		$('.no-print ul.list-unstyled li a').addClass('full-opacity-hover');
		$(this).toggleClass('full-opacity-hover');
	});

	if($(".img-jqupload-body").length){
		$(".img-jqupload-body").sortable({
			placeholder: "ui-state-highlight",
			scrollSpeed: 0,
			cursor: "move",
			delay: 0,
			cancel: "a,label,.item-td,button",
			//handle: ".icon-move",
			stop : function(){
				rebuildImagesJQUpload($(this).attr('data-model'),$(this).attr('data-field'));
			}
		});

		$('body').on('click','.preview img[big-src]',function(){
			$this=$(this);
			$.fancybox($this.attr('big-src'));
		});
	}

	if($(".treeview").length){
		$(".treeview").each(function(){
			if($(this).find('.active').length)
				$(this).addClass('active');
		});
	}

	if($(".sortableItem").length && $(".crud-grid").length){
		setSortable();
	}

	if($('#pajax_grid').length){
		$('#pajax_grid').on('pjax:success', function() {
			setSortable();
		});
	}

	//Make the dashboard widgets sortable Using jquery UI
	$(".connectedSortable").sortable({
	  placeholder: "sort-highlight",
	  connectWith: ".connectedSortable",
	  handle: ".box-header, .nav-tabs",
	  forcePlaceholderSize: true,
	  zIndex: 999999
	});
	$(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

	$("body").on("afterValidateAttribute", "#form", function (event, messages) {
		setTimeout(function(){
            errorTab();
        }, 500);
	});

	$("body").on("afterValidate", "#form", function (event, messages) {
		errorTab();

		if($('.tab-pane').find('.has-error').eq(0)){
			$('ul li > a[href="#'+$('.tab-pane').find('.has-error').eq(0).closest('.tab-pane').attr('id')+'"]').click();
		}
	});

	$('#alignmentbox input').css('visibility', 'hidden');
	$('#alignmentbox input:checked').each(function(){
		$(this).parent().addClass('current');
	});

	$('#alignmentbox :radio').click(function(){
		if($(this).is(":checked")){
			$('#alignmentbox label').removeClass('current');
			$(this).parent().addClass('current');

			var arr=$(this).val().split(' ');

			$('#watermark-alignment_1').val(arr[0]);
			$('#watermark-alignment_2').val(arr[1]);
		}
	});

	$('#pjax_layout').on('pjax:send', function() {
		Pace.start();
	});
	$('#pjax_layout').on('pjax:beforeReplace', function(content, options) {
		for (k in options){
			if(typeof options[k] !== "undefined" && typeof options[k].outerHTML !== "undefined"){
				if(options[k].outerHTML.indexOf('<link ')!==-1){
					if(!$('link[href="'+options[k].href+'"]').length){
						var link = document.createElement('link')
						link.rel = "stylesheet";
						link.href=options[k].href;
						document.head.appendChild(link);
					}
				}
				if(options[k].id=="pajax-application"){
					break;
				}
			}
		}
		Pace.stop();
	});

	$('#pjax_layout').on('pjax:end', function() {
		//Pace.stop();
		$('title').text($('#pajax-application').data('title'));
		$.AdminLTE.layout.fix();
		$.AdminLTE.tree('.sidebar');
	});
});

function errorTab(){
	$('#form a').removeClass('error-tab');

	$('#form .tab-pane').each(function(index){
		var $this=$(this);
		if($this.find('.has-error').length){
			$('ul li > a[href="#'+$this.attr('id')+'"]').addClass('error-tab');
		}
	});
}

function setSortable(){
	$(".crud-grid tbody").sortable({
		placeholder: "ui-state-highlight",
		scrollSpeed: 0,
		cursor: "move",
		delay: 0,
		cancel: "button",
		handle: ".glyphicon.glyphicon-move",
		stop : function(){
			rebuildItems();
		}
	});
}

function rebuildItems(){
	var s=[];
	$('.sortableItem').each(function(indx, element){
		s.push($(element).attr('data-id'));
	});
	$.ajax({
		url:'/'+$('body').attr('data-module')+'/'+$('body').attr('data-controller')+'/setsortvalue',
		type:'GET',
		data: {
			ids: s.join(',')
		}
	});
}

function rebuildImagesJQUpload(model,field){
	var s=[];
	$(".ijb-"+field+" .preview .panel .delete-jqupload").each(function(indx, element){
		s.push($(element).attr("data-id"));
	});
	$('[type=hidden][name="'+model+'['+field+']"]').val(s.join(";"));
}