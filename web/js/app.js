jQuery(function($) {
	$('body').on('submit','.feedback_form', function(){
		var $this=$(this);

		$.ajax({
			type: 'post',
			url: $this.attr('action'),
			data: $this.serialize(),
			success: function (data) {
				if(data=='1'){
					$this[0].reset();
					$this.find('.om-btn').html('Спасибо');
				}
			},
		});

		return false;
	});


	// Show/Hide teacher full card
    if ($('.om-teachers__full-card-close').length) {
        // show by click on small card
        $('.om-teachers__teacher').click(function(){
            scrollBack = $(this);
			var $this=$(this);
			$('.om-teachers__full-card').addClass('hidden');
            $('.om-teachers__full-card[data-id='+$this.attr('data-id')+']').removeClass('hidden');
            $('html, body').animate({
                scrollTop: $('.om-teachers__full-card[data-id='+$this.attr('data-id')+']').offset().top
            }, 1000);
        });

        // hide by click on cross
        $('.om-teachers__full-card-close').click(function(){
            $('.om-teachers__full-card').addClass('hidden');
            $('html, body').animate({
                scrollTop: scrollBack.offset().top
            }, 1000);
        });
    }
});