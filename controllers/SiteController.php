<?php

namespace app\controllers;

use Yii;
use app\components\Controller;
use yii\web\HttpException;

class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionError()
	{
		$exception = Yii::$app->errorHandler->exception;
		if ($exception !== null) {
			$code=$exception->statusCode;
			$message=$exception->getMessage();

			$this->view->title=$message;

			if($exception instanceof HttpException && ($code=='503' || ($code=='404' && Yii::$app->params['siteIsConstruction']))){
				$this->layout='error';
				return $this->render('error_503', ['exception' => $exception, 'message' => $message]);
			}

			return $this->render('error', ['exception' => $exception, 'name' => $name, 'message' => $message]);
		}
	}
}
