Установка
-------------------
Пока по такой схеме

1. Клонировать репозиторий
2. composer install
3. Отредактировать db файл, и перенести дамп бд(в случае переноса приложения)

Создание модуля
-------------------
1. Clean Module Generator создаем модуль с нужными параметрами.
2. Создаем миграцию, если у нас будет модель, для указания папки модуля, добавить параметр --module_id=test

```
#!php

./yii migrate/create create_test --module_id=test
```

3. В gii выбираем Clean Model Generator, и генерим модель.
4. Clean CRUD Generator. В поле модель необходимо указать полный путь к модели app\modules\test\models\Test. Также есть checkbox для генерации фронта.

Clean CRUD Generator - Особенности
-------------------
* столбец типа boolean или tinyint(1) - будет считаться checkbox
* столбец с именем содержащем images(потом сделаю автомат, буду смотреть по базе) - будет считать картинкой, и сгенерятся соответствующие виджеты
* для столбца с типом text - сгенерится виджет для формы с редактором
* для столбца с типом datetime/date - сгенерится виджет для формы с datepicker
* при генерации связей в модели, необходимо потом в use добавить namespace для моделей

Мультиязычность
-------------------
Для создания мультиязычности, нужно сделать миграцию основной таблицы с параметром with_lang=1, тогда сгенерируется две миграции, основная и вспомогательная языковая
```
#!php
./yii migrate/create create_test --module_id=test --with_lang=1

```
Генерировать нужно только основную модель(news), языковую(news_lang) не нужно. При генерации модели необходимо указать checkbox Мультиязычная, при генерации crud аналогично.

Виды миграций
-------------------
### Не стандартные миграции, необходимо указывать --module_id ###
Флаг with_lang для генерации основной таблицы и мультиязычной
```
#!php
./yii migrate/create create_test --module_id=test --with_lang=1
```
============================

Добавление параметров модуля в конфиг, миграцию необходимо назвать add_param, указав поля создадутся необходимые запросы на вставку с именем и типом. Доступные типы ('string','bool','text','html','email','int')
```
#!php
./yii migrate/create add_param --fields=emails:string,text:text --module_id=news
```
============================

Добавление seo страницы, связанной с модулем, название миграции - add_seopage
```
#!php
./yii migrate/create add_seopage --module_id=news
```
============================

Новое в yii [Добавление промежуточной таблицы](https://github.com/yiisoft/yii2/blob/master/docs/guide-ru/db-migrations.md#%D0%94%D0%BE%D0%B1%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%BC%D0%B5%D0%B6%D1%83%D1%82%D0%BE%D1%87%D0%BD%D0%BE%D0%B9-%D1%82%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D1%8B)
```
#!php
./yii create/migration create_junction_post_and_tag --module_id=test
```
============================

[Генерация миграций](https://github.com/yiisoft/yii2/blob/master/docs/guide-ru/db-migrations.md#%D0%93%D0%B5%D0%BD%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D1%8F-%D0%BC%D0%B8%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D0%B9-) - с версии 2.0.7 можно генерировать разные типы миграции, генерировать со столбцами

============================

[Yii::t()] -
Если необходимо, чтобы текстовая метка была полем textarea, необходимо использовать в название слово text


Модуль меню
-------------------
Создать в корне меню.
У корневых меню, есть метка, по которой меню можно вызвать.
Пример, для сайта с несколькими языками.
Делаем метку **header**, указываем ее для нескольких меню, с разными языками.
При переключении языка, подтягивается нужное меню.
```
#!php
<?=\yii\widgets\Menu::widget(['items'=>\app\helpers\MenuHelper::getMenu('header'), 'options'=>['tag'=>false], 'itemOptions'=>['tag'=>false]]);?>
```

В хэлпере формируется массив, который используется в стандартном виджете меню

Yii 2 Basic Project Template
============================

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.0.0"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTE:** Yii won't create the database for you, this has to be done manually before you can access it.

Also check and edit the other files in the `config/` directory to customize your application.