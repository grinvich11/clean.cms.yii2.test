<?php
namespace app\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\modules\admin\components\PositionBehavior;
use omgdef\multilingual\MultilingualQuery;
use app\helpers\AdminHelper;

class MainModel extends ActiveRecord {

	public function init()
    {
        parent::init();
		if(!method_exists($this, 'search'))
			$this->loadDefaultValues();
    }

	public function behaviors(){
		return [
			[
				'class' => TimestampBehavior::className(),
				'createdAtAttribute' => $this->hasAttribute('created') ? 'created' : null,
				'updatedAtAttribute' => $this->hasAttribute('modified') ? 'modified' : null,
				'value' => new Expression('NOW()'),
			],
			[
				'class' => PositionBehavior::className(),
				'attribute' => 'position'
			]
		];
	}

	public function getModelName()
	{
		return get_called_class();
	}

	public function getModuleId()
	{
		return str_replace(['app\modules\\', 'models\\', '\\'.$this->formName()], '', get_called_class());
	}

	public function getModule(){
		return Yii::$app->getModule($this->getModuleId());
	}

	public static function find()
    {
		$model_class='\\'.get_called_class();
		$model=new $model_class;
		if(isset($model->behaviors()['ml'])){
			return new MultilingualQuery(get_called_class());
		}else{
			return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
		}
    }

	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert))
		{
			if(isset($_POST[$this->formName()]['updatedimage'])){
				$module=$this->getModuleId();

				foreach ($_POST[$this->formName()]['updatedimage'] as $field=>$deleted) {
					if(is_array($deleted))
						foreach ($deleted as $file=>$value) {
							AdminHelper::deleteImages($file, $field, $module);
						}
				}
			}

			return true;
		}
		else
			return false;
	}

	public function afterDelete()
    {
		$this->deleteImages();

		parent::afterDelete();
	}

	/**
	 * Удаление изображений, связанных с моделью
	 */
	private function deleteImages(){
		$module=$this->getModuleId();

		foreach (self::getTableSchema()->columns as $column=>$value) {
			if(substr_count($column, 'image')>0){
				$images=explode(';',$this->$column);
				if($images){
					foreach ($images as $value) {
						AdminHelper::deleteImages($value, $column, $module);
					}
				}
			}
		}
	}
}
