<?php
namespace app\models;

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property string $id
 * @property string $section
 * @property string $param
 * @property string $value
 * @property string $default
 * @property string $label
 * @property string $type
 * @property string $modified
 * @property integer $notDelete
 */
class Config extends \yii\db\ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public static function tableName()
	{
		return '{{config}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value', 'safe'),
			array('notDelete', 'integer'),
			array(['section', 'label'], 'string', 'max'=>255),
			array('param', 'string', 'max'=>128),
			array('type', 'string', 'max'=>6),
			array(['section', 'label', 'param','default','modified', 'type','help'], 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, section, param, value, default, label, type, modified, notDelete,module', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'section' => 'Категория',
			'param' => 'Параметр',
			'value' => 'Значение',
			'default' => 'По умолчанию',
			'label' => 'Название',
			'type' => 'Тип',
			'modified' => 'Изменен',
			'notDelete' => 'Not Delete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	/*public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition='t.section<>""';

		$criteria->compare('id',$this->id,true);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('param',$this->param,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('default',$this->default,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('modified',$this->modified,true);
		$criteria->compare('notDelete',$this->notDelete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}*/

	public function beforeSave($insert)
	{
		if(parent::beforeSave($insert))
		{
			$this->modified=date('Y-m-j H-i-s');

			\app\components\MyConfig::clearCache();

			return true;
		}
		else
			return false;
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasOne(\app\modules\admin\models\AdminModules::className(), ['module' => 'module']);
    }
}